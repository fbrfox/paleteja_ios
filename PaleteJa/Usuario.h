//
//  Usuario.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

/*
 
 "UsuarioId": 1,
 "Nome": "Compra Paletes LTDA",
 "PrecoMedio": "R$ 6,99",
 "Paletes": [
 {
 "PaleteId": 1,
 "TipoProduto": 1,
 "TipoProdutoDesc": "Palete Novo",
 "PrecoAlvo": 10,
 "PrecoPromocao": 6.99,
 "NotaFiscal": false,
 "PrecoMedio": 13.49,
 "Saldo": 100,
 "Descricao": "Paletes praticamente novos",
 "TabelaA": 15,
 "TabelaB": 10,
 "TabelaC": 5,
 "QuantidadeA": 50,
 "QuantidadeB": 25,
 "QuantidadeC": 15
 }
 ],
 "Endereco": null,
 "Telefone": null,
 "Celular": null
 
 */


#import "ModelBase.h"

@protocol Palete;
@class UsuarioConfiguracao;


@protocol Usuario <NSObject>



@end

@interface Usuario : ModelBase

@property(nonatomic,strong) NSNumber *UsuarioId;
@property(nonatomic,strong) NSString *Nome;
@property(nonatomic,strong) NSString *PrecoMedio;
@property(nonatomic,strong) NSArray<Palete> *Paletes;
@property(nonatomic,strong) NSString *Endereco;
@property(nonatomic,strong) NSString *Telefone;
@property(nonatomic,strong) NSString *Celular;
@property(nonatomic, strong) NSString *Email;

@property(nonatomic,strong) NSString *Documento;
@property(nonatomic,strong) NSString *Foto;
@property(nonatomic,strong) NSString *TelefoneFixo;
@property(nonatomic) BOOL CelularWhatsapp;
@property(nonatomic,strong) NSString *Site;
@property(nonatomic,strong) NSNumber *Latitude;
@property(nonatomic,strong) NSNumber *Longitude;
@property(nonatomic,strong) NSString *Cidade;
@property(nonatomic,strong) NSString *Estado;
@property(nonatomic, strong) UsuarioConfiguracao *UsuarioConfiguracao ;
@property(nonatomic, strong)NSNumber *TipoUsuario;

@end
