//
//  SalvarPaleteRequest.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

/*
 
 {
 "PaleteId": 1,
 "TipoProduto": 1 - novo 2 -usado,
 "PrecoAlvo": 15.00,
 "PrecoPromocao": 9.99,
 "NotaFiscal": false,
 "PrecoMedio": 12.00,
 "Saldo": 100,
 "Descricao": "Paletes praticamente novos",
 "TabelaA": 15,
 "TabelaB": 10,
 "TabelaC": 5,
 "QuantidadeA": 50,
 "QuantidadeB": 25,
 "QuantidadeC": 15
 }
 
 */


#import "ModelBase.h"

@interface SalvarPaleteRequest : ModelBase

@property(nonatomic,strong) NSNumber *PaleteId;
@property(nonatomic,strong) NSNumber *TipoProduto;
@property(nonatomic,strong) NSNumber *PrecoAlvo;
@property(nonatomic,strong) NSNumber *PrecoPromocao;
@property(nonatomic,strong) NSNumber *NotaFiscal;
@property(nonatomic,strong) NSNumber *PrecoMedio;
@property(nonatomic,strong) NSNumber *Saldo;
@property(nonatomic,strong) NSString *Descricao;
@property(nonatomic,strong) NSNumber *TabelaA;
@property(nonatomic,strong) NSNumber *TabelaB;
@property(nonatomic,strong) NSNumber *TabelaC;
@property(nonatomic,strong) NSNumber *QuantidadeA;
@property(nonatomic,strong) NSNumber *QuantidadeB;
@property(nonatomic,strong) NSNumber *QuantidadeC;


@end
