//
//  AutenticarRequest.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "AutenticarRequest.h"

@implementation AutenticarRequest

-(id)initWithEmail:(NSString *)email andSenha:(NSString *)senha{
    
    self = [super init];
    
    if(self){
        
        self.Email = email;
        self.Senha = senha;
    }
    
    return self;
}

@end
