//
//  CadastroFotoViewController.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 19/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "CadastroFotoViewController.h"
#import "CadastroFotoSegue.h"

#define kCadastroDadosSegue @"cadastroDadosSegue"

@interface CadastroFotoViewController ()

@end

@implementation CadastroFotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_ivPerfil setClipsToBounds:YES];
    _presenter = [CadastroFotoPresenterImpl new];
    [_presenter initWithView:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:kCadastroDadosSegue]){
        CadastroFotoSegue* ns = (CadastroFotoSegue*) segue;
        ns.foto = _imageSelected;
    }

    
}

- (IBAction)onBackClick:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onTapFoto:(id)sender {
    
    [UIHelper showSelectionImage:self delegate:self];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *, id> *)info {
    
    
    UIImage *pickedImage = info[UIImagePickerControllerOriginalImage];
    _imageSelected = [UIHelper imageToNSString:pickedImage];
    [picker dismissViewControllerAnimated:NO completion:nil];
    [_ivPerfil setImage:pickedImage];
    [_ivPerfil setClipsToBounds:YES];

    
    
    
}


-(NSString *)getImageSelected{
    
    return _imageSelected;
}


-(void)toNextStep{
    
    [self performSegueWithIdentifier:kCadastroDadosSegue sender:self];
    
}
- (IBAction)onNextClick:(id)sender {
    
    [_presenter nextStepClick];
}


@end
