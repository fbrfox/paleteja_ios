//
//  CadastroPaleteViewController.h
//  PaleteJa
//
//  Created by Pedro  on 26/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Palete.h"

@interface CadastroPaleteViewController : BaseViewController <TextFieldPickerHelperDelegate,UITextFieldDelegate, UIHelperDelegate>

@property Palete *palete;

@end
