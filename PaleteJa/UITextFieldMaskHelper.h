//
// Created by Movida on 08/10/15.
// Copyright (c) 2015 UvaRosa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMaskTextField.h"
#import "EnumHelper.h"


@interface UITextFieldMaskHelper : VMaskTextField


- (BOOL)isEmpty;

- (NSString *)textUnmasked;

- (void)configuraMaskedTextfield:(MaskType)type target:(id)target action:(SEL)action;

- (BOOL)validEmail;

- (BOOL)validCPF;
@end