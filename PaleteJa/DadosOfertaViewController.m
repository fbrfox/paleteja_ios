//
//  DadosOfertaViewController.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "DadosOfertaViewController.h"
#import "Oferta.h"

@interface DadosOfertaViewController ()
{
    __weak IBOutlet UIImageView *ivPerfil;
    __weak IBOutlet UILabel *lbQuantidade;
    __weak IBOutlet UILabel *lbTipoProduto;
    __weak IBOutlet UILabel *lbValorNegociado;
    __weak IBOutlet UILabel *lbTipoEntrega;
    __weak IBOutlet UILabel *lbTelefone;
    __weak IBOutlet UILabel *lbEmail;
    __weak IBOutlet UILabel *lbEndereco;
    __weak IBOutlet UILabel *lbNome;
    __weak IBOutlet UILabel *lbTotal;
    
}
@end

@implementation DadosOfertaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self carregaCamposOferta];
}

- (void)carregaCamposOferta {

    [ivPerfil sd_setImageWithURL:[[NSURL alloc] initWithString:[NSString stringWithFormat:UrlFormatPerfilUsuario,_oferta.Usuario.UsuarioId]]];
    [ivPerfil setClipsToBounds:YES];
    lbQuantidade.text = [NSString stringWithFormat:@"%d",_oferta.Quantidade];
    lbEmail.text = _oferta.Usuario.Email;
    lbNome.text = _oferta.Usuario.Nome;
    lbEndereco.text = _oferta.Usuario.Endereco;
    lbTelefone.text = _oferta.Usuario.Telefone;
    lbTipoEntrega.text = _oferta.TipoEntrega;
    lbTipoProduto.text = _oferta.TipoProdutoDescricao;
    lbValorNegociado.text = [UIHelper formatarPreco:_oferta.PrecoPago];
    
    double total = [_oferta.PrecoPago doubleValue] * _oferta.Quantidade;
    
    lbTotal.text = [UIHelper formatarPreco:@(total)];
    

}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onTelClick:(id)sender {

    [UIHelper callNumber:_oferta.Usuario.Telefone];
    
}
- (IBAction)onTelMail:(id)sender {

    [UIHelper sendMail:_oferta.Usuario.Email];

}
- (IBAction)onTelMaps:(id)sender {

    NSString *endereco = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@",_oferta.Usuario.Endereco];


    [[UIApplication sharedApplication] openURL:[[NSURL alloc] initWithString:[endereco stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]] options:nil completionHandler:^(BOOL success) {

    }];
}

@end
