//
//  VendedorSegue.m
//  PaleteJa
//
//  Created by Pedro  on 22/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "VendedorSegue.h"
#import "CadastroDadosVendedorViewController.h"

@implementation VendedorSegue
{

}


- (void)perform {

    UIViewController* src = self.sourceViewController;
    UIViewController* dst = self.destinationViewController;

    if([dst isKindOfClass:[CadastroDadosVendedorViewController class]]){
        CadastroDadosVendedorViewController* dvc = (CadastroDadosVendedorViewController*) dst;
        dvc.request = self.request;
    }

    [src.navigationController pushViewController:dst animated:YES];
}


@end
