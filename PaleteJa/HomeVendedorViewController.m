//
//  HomeVendedorViewController.m
//  PaleteJa
//
//  Created by Pedro  on 23/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "HomeVendedorViewController.h"
#import "MeusPaletesViewController.h"
#import "OfertasAvulsasViewController.h"
#import "ObterPaletesVendidosResponse.h"

@interface HomeVendedorViewController () {
    __weak IBOutlet UILabel *lbPaletes;
    __weak IBOutlet UILabel *lbOfertas;
    __weak IBOutlet UILabel *lbMinhasOfertas;
    __weak IBOutlet UIActivityIndicatorView *aiPaletes;
    __weak IBOutlet UIActivityIndicatorView *AiOfertas;
    __weak IBOutlet UIActivityIndicatorView *aiMinhasOfertas;
    __weak IBOutlet UIActivityIndicatorView *aiPaletesVendidos;
    __weak IBOutlet UILabel *lbPaletesVendidos;
    __weak IBOutlet UILabel *lbValorTotal;
    __weak IBOutlet UILabel *lbPrecoMedio;

}
@end

@implementation HomeVendedorViewController

- (void)viewDidLoad {
    [super viewDidLoad];


    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toMeusPaletes) name:@"MeusPaletes" object:nil];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self performSelectorInBackground:@selector(carregaPaletes) withObject:nil];
    [self performSelectorInBackground:@selector(carregaOfertas) withObject:nil];
    [self performSelectorInBackground:@selector(carregaMinhasOfertas) withObject:nil];
    [self performSelectorInBackground:@selector(carregarPaletesVendidos) withObject:nil];

}


- (void)toMeusPaletes {

    [self performSegueWithIdentifier:@"meusPaletesSegue" sender:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)carregaPaletes {

    [aiPaletes startAnimating];

    [[APIClient sharedManager] meusPaletescompletion:^(MeusPaletesResponse *response, NSString *errorMessage) {

        [aiPaletes stopAnimating];

        self.paletes = response.Paletes;

        lbPaletes.text = [NSString stringWithFormat:@"%lu", (unsigned long) self.paletes.count];
        lbPrecoMedio.text = [NSString stringWithFormat:@"Preço médio: %@",[UIHelper formatarPreco:response.PrecoMedio]];


    }];

}

- (void)carregaOfertas {

    [AiOfertas startAnimating];

    [[APIClient sharedManager] ofertasAvulsasCompletion:^(ObterOfertasAvulsasResponse *response, NSString *errorMessage) {

        [AiOfertas stopAnimating];

        self.ofertasAvulsas = response.Ofertas;

        lbOfertas.text = [NSString stringWithFormat:@"%lu", (unsigned long) self.ofertasAvulsas.count];

    }];


}

- (void)carregaMinhasOfertas {


    [aiMinhasOfertas startAnimating];

    [[APIClient sharedManager] obterMinhasOfertasCompletion:^(ObterOfertasAvulsasResponse *response, NSString *errorMessage) {

        [aiMinhasOfertas stopAnimating];

        self.minhasOfertas = response.Ofertas;

        lbMinhasOfertas.text = [NSString stringWithFormat:@"%lu", (unsigned long) self.minhasOfertas.count];

    }];

}

- (void)carregarPaletesVendidos {


    [aiPaletesVendidos startAnimating];

    [[APIClient sharedManager] obterPaletesVendidos:^(ObterPaletesVendidosResponse *response, NSString *errorMessage) {

        [aiPaletesVendidos stopAnimating];

        lbPaletesVendidos.text = [NSString stringWithFormat:@"%@", response.Quantidade];
        lbValorTotal.text = [UIHelper formatarPreco:response.Preco];

    }];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {


    if ([segue.identifier isEqualToString:@"meusPaletesSegue"]) {

        MeusPaletesViewController *meusPaletesVC = segue.destinationViewController;
        meusPaletesVC.paletes = _paletes;

    } else if ([segue.identifier isEqualToString:@"ofertasAvulsasSegue"]) {

        OfertasAvulsasViewController *ofertasVC = segue.destinationViewController;

        ofertasVC.ofertas = _ofertasAvulsas;

    }
}

- (IBAction)onRefresh:(id)sender {

    [self performSelectorInBackground:@selector(carregaPaletes) withObject:nil];
    [self performSelectorInBackground:@selector(carregaOfertas) withObject:nil];
    [self performSelectorInBackground:@selector(carregaMinhasOfertas) withObject:nil];
    [self performSelectorInBackground:@selector(carregarPaletesVendidos) withObject:nil];

}

@end
