//
//  AutenticacaoHelper.h
//  Movida Condominio
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 09/03/17.
//  Copyright © 2017 Movida Locação de Veículos Ltda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AutenticarResponse.h"

@interface AutenticacaoHelper : NSObject

+(void)logout;
+(void)loginAutenticacao:(AutenticarResponse *)autenticacao;

+(BOOL)isAutenticado;

@end
