//
//  CadastroPaleteViewController.m
//  PaleteJa
//
//  Created by Pedro  on 26/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "CadastroPaleteViewController.h"
#import "TipoProduto.h"
#import "TipoProdutoRepository.h"
#import "MPNumericTextField.h"
#import "PrecoMedioHoje.h"

@interface CadastroPaleteViewController () {
    __weak IBOutlet UITextField *tfDescricao;
    __weak IBOutlet UISegmentedControl *scNotaFiscal;
    __weak IBOutlet UITextField *tfQuantidade;
    __weak IBOutlet TextFieldPickerHelper *tfTipoProduto;

    __weak IBOutlet MPNumericTextField *tfPrecoAlvo;
    __weak IBOutlet MPNumericTextField *tfPrecoPromocao;
    
    __weak IBOutlet MPNumericTextField *tfDescontoA;
    __weak IBOutlet UITextField *tfQtdA;
    __weak IBOutlet UITextField *tfQtdB;
    __weak IBOutlet MPNumericTextField *tfDescontoB;
    __weak IBOutlet UITextField *tfQtdC;

    __weak IBOutlet MPNumericTextField *tfDescontoC;
    __weak IBOutlet UILabel *lbPrecoMedio;
    

    NSArray *tiposProduto;

    int tipoProdutoSelecionado;
}
@end

@implementation CadastroPaleteViewController {
    NSNumberFormatter *formatter;
    NSString *precoMedio;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    tfPrecoPromocao.type = tfPrecoAlvo.type = MPNumericTextFieldCurrency;
    tfPrecoPromocao.locale = tfPrecoAlvo.locale = NSLocale.currentLocale;

    tfDescontoC.type = tfDescontoB.type = tfDescontoA.type= MPNumericTextFieldPercentage;

    [super configdismissOnTap:@[tfDescontoA, tfDescontoB, tfDescontoC, tfDescricao, tfPrecoAlvo, tfPrecoPromocao, tfQtdA,
            tfQtdB, tfQtdB, tfQtdC, tfQuantidade, tfTipoProduto]];

    [self carregaTiposProduto];

    tfTipoProduto.delegate = self;

    if (self.palete)
        [self carregaCamposPalete];


    [UIHelper showProgress];

    [[APIClient sharedManager] obterPrecoMedioCompletion:^(PrecoMedioHoje *response, NSString *errorMessage) {

        [UIHelper hideProgress];

        if(response.Sucesso) {
            precoMedio = response.PrecoMedio;
            lbPrecoMedio.text = [NSString stringWithFormat:@"O preço médio hoje é: %@", precoMedio];
        }


    }];


}

- (void)carregaTiposProduto {


    tiposProduto = [TipoProdutoRepository getTiposProduto];

}

- (void)carregaCamposPalete {

    tfDescricao.text = _palete.Descricao;
    tfDescontoC.numericValue = @(_palete.TabelaC);
    tfDescontoB.numericValue = @(_palete.TabelaB);
    tfDescontoA.numericValue = @(_palete.TabelaA);
    tfQtdC.text = [NSString stringWithFormat:@"%d",_palete.QuantidadeC];
    tfQtdB.text = [NSString stringWithFormat:@"%d",_palete.QuantidadeB];
    tfQtdA.text = [NSString stringWithFormat:@"%d",_palete.QuantidadeA];
    tfPrecoPromocao.numericValue = _palete.PrecoPromocao;
    tfPrecoAlvo.numericValue = _palete.PrecoAlvo;
    tfQuantidade.text = [NSString stringWithFormat:@"%d",_palete.Saldo];;

    TipoProduto *tipo = tiposProduto[_palete.TipoProduto-1];
    tipoProdutoSelecionado = _palete.TipoProduto;
    tfTipoProduto.text = tipo.descricao;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onCancelClick:(id)sender {

    [self.navigationController popViewControllerAnimated:YES];

}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView withTextField:(TextFieldPickerHelper *)sender {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender {
    return tiposProduto.count;


}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender {
    TipoProduto *tipo = tiposProduto[row];
    return tipo.descricao;
}

- (void)doneClicked:(TextFieldPickerHelper *)sender withIndex:(NSInteger)index {


    [sender resignFirstResponder];

    if ([sender isEqual:tfTipoProduto]) {
        TipoProduto *tipo = tiposProduto[index];
        tipoProdutoSelecionado = index + 1;
        tfTipoProduto.text = tipo.descricao;
    }


}


- (IBAction)onRegistrarClick:(id)sender {

    if(![self validaCampos]){
        return;
    }

    
    [UIHelper showProgress];

    SalvarPaleteRequest *request = [[SalvarPaleteRequest alloc] init];

    request.Descricao = tfDescricao.text;
    request.NotaFiscal = @(scNotaFiscal.selectedSegmentIndex == 0);
    
    if(_palete)
        request.PaleteId = _palete.PaleteId;
    
    request.PrecoAlvo = tfPrecoAlvo.numericValue;
    request.PrecoPromocao = tfPrecoPromocao.numericValue;
    NSString *precoMedio = [[precoMedio stringByReplacingOccurrencesOfString:@"R$" withString:@""] stringByReplacingOccurrencesOfString:@"," withString:@"."];
    request.PrecoMedio = @([precoMedio doubleValue]);
    request.QuantidadeA = @([tfQtdA.text intValue]);
    request.QuantidadeB = @([tfQtdB.text intValue]);
    request.QuantidadeC = @([tfQtdC.text intValue]);
    request.Saldo = @([tfQuantidade.text intValue]);
    request.TipoProduto = @(tipoProdutoSelecionado);
    request.TabelaA = tfDescontoA.numericValue;
    request.TabelaB = tfDescontoB.numericValue;
    request.TabelaC = tfDescontoC.numericValue;

    [[APIClient sharedManager] salvarPalete:request completion:^(ResponseBase *base, NSString *string) {

        [UIHelper hideProgress];

        if (string) {

            [UIHelper mostrarAlertaErro:string viewController:self];
        } else if (!base.Sucesso)
            [UIHelper mostrarAlertaErro:base.Mensagem viewController:self];
        else
            [UIHelper mostrarAlertaSucesso:base.Mensagem viewController:self delegate:self];


    }];


}

- (BOOL)validaCampos {

    NSMutableString *erros = [[NSMutableString alloc] init];


    if(![ValidacaoHelper validarString:tfDescricao.text])
        [erros appendString:@"o Campo descrição é de preenchimento obrigatório!\n"];

    if([tfQuantidade.text intValue] == 0)
        [erros appendString:@"o Campo quantidade é de preenchimento obrigatório!\n"];

    if(tipoProdutoSelecionado == 0)
        [erros appendString:@"Selecione o tipo de produto\n"];

    if([tfPrecoAlvo.numericValue intValue] == 0)
        [erros appendString:@"o Campo preço alvo deve ser maior que zero!\n"];

    if([tfPrecoPromocao.numericValue intValue] == 0)
        [erros appendString:@"o Campo preço promoção deve ser maior que zero!\n"];

    if(![erros isEqualToString:@""])
        [UIHelper mostrarAlertaErro:erros viewController:self];

    return [erros isEqualToString:@""];


}
-(void)alertControllerOkClicked {

    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewWillDisappear:(BOOL)animated {


}

@end
