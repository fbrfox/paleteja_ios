//
//  CadastrarUsuarioRequest.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "CadastrarUsuarioRequest.h"

@implementation CadastrarUsuarioRequest

-(id)initWithEmail:(NSString *)email nome:(NSString *)nome senha:(NSString *)senha andTipoUsuario:(NSNumber *)tipoUsuario{
    
    self = [super init];
    
    if(self){
        
        self.Email = email;
        self.Nome = nome;
        self.Senha = senha;
        self.TipoUsuario = tipoUsuario;
        
    }
    
    return self;
    
}

@end
