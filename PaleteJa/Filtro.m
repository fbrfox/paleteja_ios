//
//  Filtro.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "Filtro.h"

@implementation Filtro
- (instancetype)initWithEstado:(NSString *)Estado Raio:(int)Raio {
    self = [super init];
    if (self) {
        self.Estado = Estado;
        self.Raio = Raio;
    }

    return self;
}

+ (instancetype)filtroWithEstado:(NSString *)Estado Raio:(int)Raio {
    return [[self alloc] initWithEstado:Estado Raio:Raio];
}


@end
