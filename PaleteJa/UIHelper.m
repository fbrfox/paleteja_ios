//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import "AppDelegate.h"
#import "UIHelper.h"
#import "SVProgressHUD.h"
#import "Reachability.h"





@implementation UIHelper

//+ (UIHelper *)sharedInstance {
//
//    static UIHelper *_sharedManager = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        _sharedManager = [[UIHelper alloc] init];
//    });
//
//    return _sharedManager;
//}

+ (void)mostrarAlerta:(NSString *)mensagem  viewController:(UIViewController *)vc{

    [self mostrarAlerta:@"" mensagem:mensagem viewController:vc];
}

+ (void)mostrarAlertaErro:(NSString *)mensagem  viewController:(UIViewController *)vc{

    [self mostrarAlerta:@"Ocorreu um erro!" mensagem:mensagem viewController:vc];
}

+ (void)mostrarAlertaInfo:(NSString *)mensagem  viewController:(UIViewController *)vc{

    [self mostrarAlerta:@"Atenção" mensagem:mensagem  viewController:vc];
}

+ (void)mostrarAlertaSucesso:(NSString *)mensagem  viewController:(UIViewController *)vc{
    
    [self mostrarAlertaSucesso:mensagem viewController:vc delegate:nil];
}


+ (void)mostrarAlertaSucesso:(NSString *)mensagem viewController:(UIViewController *)vc delegate:(id<UIHelperDelegate>)delegate {

    [self mostrarAlerta:@"Sucesso" mensagem:mensagem delegate:delegate viewController:vc] ;
}

+ (void)mostrarAlerta:(NSString *)titulo mensagem:(NSString *)mensagem  viewController:(UIViewController *)vc {

    [self mostrarAlerta:titulo mensagem:mensagem delegate:nil viewController:vc];
}

+ (void)mostrarAlerta:(NSString *)titulo mensagem:(NSString *)mensagem delegate:(id<UIHelperDelegate>)delegate viewController:(UIViewController *)vc {
   
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:titulo message:mensagem preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {


        [delegate alertControllerOkClicked];
    
        
    }]];
    
    
    [vc presentViewController:alertController animated:YES completion:nil];
    
}

+(void)mostraAlertaConfimacao:(NSString *)texto delegate:(id<UIHelperDelegate>)delegate viewController:(UIViewController *)vc{

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirme sua ação" message:texto
                                                                      preferredStyle:UIAlertControllerStyleAlert];

    [alertController addAction:[UIAlertAction actionWithTitle:@"Sim" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        [delegate alertControllerSimClicked];


    }]];

    [alertController addAction:[UIAlertAction actionWithTitle:@"Não" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {

        [delegate alertControllerNaoClicked];


    }]];


    [vc presentViewController:alertController animated:YES completion:nil];
}



+ (NSString *)formatarPreco:(NSNumber *)number {

    if([number doubleValue] == 0)
        return @"R$ 0,00";
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setDecimalSeparator:@","];
    [formatter setMinimumFractionDigits:2];

    NSString *amountString = [formatter stringFromNumber:number];

    return [NSString stringWithFormat:@"R$ %@", amountString];

}



+ (void)showProgress {

    [self showProgresswithText:nil];

}

+ (void)showProgresswithText:(NSString *)text {

    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    if (!text) {
        
        [SVProgressHUD show];
    } else {
        [SVProgressHUD showWithStatus:text];
    }
}


+(void)hideProgress{

    [SVProgressHUD dismiss];
}


+ (UIImage *)obterImagemPorCor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);

    UIImage *imageNav = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return imageNav;
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


+(void)htmlInLabel:(UILabel *)label html:(NSString *)html{

    NSError *err = nil;
    label.attributedText = [[NSAttributedString alloc] initWithData: [html dataUsingEncoding:NSUTF8StringEncoding]
                                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                 documentAttributes: nil
                                                              error: &err];

}

+ (void)configuraNumberPadComDoneButton:(UITextField *)textField target:(id)target action:(SEL)action {

    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Próximo"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:target
                                                                  action:action];

    doneButton.tag = textField.tag;

    UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];

    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexableItem, doneButton, nil]];

    textField.inputAccessoryView = keyboardDoneButtonView;

}

NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

+(NSString *) randomStringWithLength: (int) len {

    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];

    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }

    return randomString;
}





+ (NSString *)dataParaString:(NSDate *)date formato:(NSString *)formato {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:formato];
    return [formatter stringFromDate:date];
}


+(NSString *)adicionaDias:(int)dias data:(NSDate *)data formato:(NSString *)formato{
    

    NSDate *newDate1 = [data dateByAddingTimeInterval:60*60*24*dias];
    
    return    [self dataParaString:newDate1 formato:formato];
    
}

+(NSDate *)adicionaMinutos:(int)minutos data:(NSDate *)data{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSCalendarUnit unit = NSCalendarUnitMinute;

    NSDate *today = [NSDate date];
    return [calendar dateByAddingUnit:unit value:minutos toDate:today options:NSCalendarMatchStrictly];
    
}


+ (NSDate *)stringParaData:(NSString *)date formato:(NSString *)formato {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:formato];
    return [formatter dateFromString:date];
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(NSMutableString*) timeLeftSinceDate: (NSDate *) dateT {
    
    NSMutableString *timeLeft = [[NSMutableString alloc]init];
    
    NSDate *today10am =[NSDate date];
    
    NSInteger seconds = [today10am timeIntervalSinceDate:dateT];
    
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) seconds -= days * 3600 * 24;
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) seconds -= hours * 3600;
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds -= minutes * 60;
    
    if(days) {
        [timeLeft appendString:[NSString stringWithFormat:@"%ld dias", (long)days]];
    }
    
    if(hours) {
        [timeLeft appendString:[NSString stringWithFormat: @"%ld h", (long)hours]];
    }
    
    if(minutes) {
        [timeLeft appendString: [NSString stringWithFormat: @"%ld min",(long)minutes]];
    }
    
    if(seconds) {
        [timeLeft appendString:[NSString stringWithFormat: @"%ld seg", (long)seconds]];
    }
    
    return timeLeft;
}

+(NSString *)imageToNSString:(UIImage *)image
{
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    return [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

+(NSData *)imageToData:(UIImage *)image
{
    NSData *imageData = UIImageJPEGRepresentation(image,0.6f);
    return  imageData;
}

+(BOOL)isConnected{
    
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}


+(void)addFloatButtonInView:(UIView *)viewAdd delegate:(id<LGPlusButtonsViewDelegate>)delegate {
    
    
    LGPlusButtonsView *view = [[LGPlusButtonsView alloc]initWithNumberOfButtons:1 firstButtonIsPlusButton:YES showAfterInit:YES delegate:delegate];
    
    view.position = LGPlusButtonsViewPositionBottomRight;
    view.plusButtonAnimationType = LGPlusButtonAnimationTypeRotate;
    
    [view setButtonsTitles:@[@"+"] forState:UIControlStateNormal];
    [view setDescriptionsTexts:@[@""]];
    [view setButtonsImages:@[[NSNull new]]
                  forState:UIControlStateNormal
            forOrientation:LGPlusButtonsViewOrientationAll];
    
    [view setButtonsAdjustsImageWhenHighlighted:NO];
    [view setButtonsBackgroundColor:[UIColor colorWithHexString:@"7F007F"] forState:UIControlStateNormal];
    
    [view setDescriptionsBackgroundColor:[UIColor whiteColor]];
    [view setDescriptionsTextColor:[UIColor blackColor]];
    
    [view setButtonsSize:CGSizeMake(44.f, 44.f) forOrientation:LGPlusButtonsViewOrientationAll];
    [view setButtonsLayerCornerRadius:44.f/2.f forOrientation:LGPlusButtonsViewOrientationAll];
    
    [view setDescriptionsLayerShadowColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.f]];
    [view setDescriptionsLayerShadowOpacity:0.25];
    [view setDescriptionsLayerShadowRadius:1.f];
    [view setDescriptionsLayerShadowOffset:CGSizeMake(0.f, 1.f)];
    [view setDescriptionsLayerCornerRadius:6.f forOrientation:LGPlusButtonsViewOrientationAll];
    [view setDescriptionsContentEdgeInsets:UIEdgeInsetsMake(4.f, 8.f, 4.f, 8.f) forOrientation:LGPlusButtonsViewOrientationAll];
    
    [viewAdd addSubview:view];
    [viewAdd bringSubviewToFront:view];


}


+(NSString *)imageUrlToBase64Image:(NSString *)imageUrl{
    
    NSData*dataImage =  [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
    
    UIImage *image = [UIImage imageWithData:dataImage];
    
    return [self imageToNSString:image];
    
}

+(void)callNumber:(NSString *)tel{
    
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",tel]] options:[NSDictionary new] completionHandler:nil];
}

+(void)sendMail:(NSString *)email{
    
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@",email]] options:[NSDictionary new] completionHandler:nil];
}



+ (void)showSelectionImage:(UIViewController *)controller delegate:(id <UINavigationControllerDelegate, UIImagePickerControllerDelegate>) delegate {
    UIAlertController *alertaPhotoController = [UIAlertController alertControllerWithTitle:@"Selecione uma opção" message:@"Selecione se deseja capturar o selecionar uma foto da galeria" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [alertaPhotoController addAction:[UIAlertAction actionWithTitle:@"Galeria" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self showGaleria:controller delegate:delegate];
        
    }]];
    
    
    [alertaPhotoController addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self showCamera:controller delegate:delegate];
        
    }]];
    
    
    [alertaPhotoController addAction:[UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        

        
    }]];
    [controller presentViewController:alertaPhotoController animated:YES completion:nil];
    
}


+(void)showGaleria:(UIViewController *)controller delegate:(id <UINavigationControllerDelegate, UIImagePickerControllerDelegate>) delegate{
    
    [self showDevicePhoto:UIImagePickerControllerSourceTypeSavedPhotosAlbum controller:controller delegate:delegate];
    
}

+(void)showCamera:(UIViewController *)controller delegate:(id <UINavigationControllerDelegate, UIImagePickerControllerDelegate>) delegate {
    
    [self showDevicePhoto:UIImagePickerControllerSourceTypeCamera controller:controller delegate:delegate];
}


+(void)showDevicePhoto:(UIImagePickerControllerSourceType)type controller:(UIViewController *)controller delegate:(id <UINavigationControllerDelegate, UIImagePickerControllerDelegate>) delegate{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = type;
    picker.delegate = delegate;
    [controller presentViewController:picker animated:YES completion:Nil];
    
    
}






@end
