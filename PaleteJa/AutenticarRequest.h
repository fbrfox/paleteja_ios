//
//  AutenticarRequest.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "ModelBase.h"

@interface AutenticarRequest : ModelBase


@property(nonatomic,strong) NSString* Email;
@property(nonatomic,strong) NSString* Senha;


-(id)initWithEmail:(NSString *)email andSenha:(NSString *)senha;

@end
