//
// Created by Movida on 22/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MinhasOfertasView <NSObject>

-(void) onListOfertasReceived:(NSArray<Oferta> *)ofertas;
-(void) showLoading;
-(void) hideLoading;
-(void) showAlertErro:(NSString *)error;

@end