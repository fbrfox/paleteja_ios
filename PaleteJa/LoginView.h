//
//  LoginView.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 22/01/18.
//  Copyright © 2018 phep. All rights reserved.
//
#import "Autenticacao.h"

@protocol LoginView <NSObject>

-(NSString*)getEmail;
-(NSString*)getSenha;

-(void)showAlertSucess:(NSString *)success;
-(void)showAlertError:(NSString *)error;
-(void)registerWithSuccess:(Autenticacao *)autenticacao;
-(void)showProgress;
-(void)hideProgress;

- (void)toHome;
@end
