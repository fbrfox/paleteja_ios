//
//  RealizarOfertaAvulsaRequest.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 18/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "RealizarOfertaAvulsaRequest.h"

@implementation RealizarOfertaAvulsaRequest

-(id)initWithQuantidadePaletes:(NSNumber *)quantidadePaletes valorPago:(NSNumber *)valorPago tipoEntrega:(NSNumber *)tipoEntrega endereco:(NSString *)endereco latitude:(NSNumber *)latitude longitude:(NSNumber *)longitude andTipoProduto:(NSNumber *)tipoProduto{
    
    
      self =  [self initWithPaleteId:nil quantidadePaletes:quantidadePaletes valorPago:valorPago tipoEntrega:tipoEntrega endereco:endereco latitude:latitude longitude:longitude andTipoProduto:tipoProduto];
        
    
    return self;
    
}


-(id)initWithPaleteId:(NSNumber *)paleteId quantidadePaletes:(NSNumber *)quantidadePaletes valorPago:(NSNumber *)valorPago tipoEntrega:(NSNumber *)tipoEntrega endereco:(NSString *)endereco latitude:(NSNumber *)latitude longitude:(NSNumber *)longitude andTipoProduto:(NSNumber *)tipoProduto{
    
    
    self = [super init];
    
    if(self){
        
        self.PaleteId = paleteId;
        self.QuantidadePaletes = quantidadePaletes;
        self.ValorPago = valorPago;
        self.TipoEntrega = tipoEntrega;
        self.Endereco = endereco;
        self.Latitude = latitude;
        self.Longitude = longitude;
        self.TipoProduto = tipoProduto;
        
    }
    
    return  self;
}


@end
