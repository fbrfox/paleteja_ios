//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//


#import <AFNetworking.h>
#import "AllRequestResponse.h"

@class Filtro;
@class PrecoMedioHoje;
@class ObterUsuarioResponse;
@class ObterUsuarioResponse;
@class ObterPaletesVendidosResponse;


@interface APIClient : AFHTTPSessionManager

+ (APIClient *)sharedManager;




- (NSURLSessionDataTask *)obterUsuarioWithCompletion:(void (^)(ObterUsuarioResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)atualizarUsuario:(CadastrarDadosVendedorRequest *)loginRequest completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)autenticar:(AutenticarRequest *)loginRequest completion:(void (^)(AutenticarResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)esqueciSenha:(AutenticarRequest *)loginRequest completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion;


-(NSURLSessionDataTask *)cadastrarUsuario:(CadastrarUsuarioRequest *)cadastroRequest completion:(void (^)(AutenticarResponse *response, NSString *errorMessage))completion;

-(NSURLSessionDataTask *)cadastrarDadosComprador:(CadastrarDadosCompradorRequest *)cadastroRequest completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion;

-(NSURLSessionDataTask *)obterUsuariosPaletes:(Filtro *)request completion:(void (^)(ObterUsuariosPaletesResponse *response, NSString *errorMessage))completion;

-(NSURLSessionDataTask *)obterPrecoMedioCompletion:(void (^)(PrecoMedioHoje *response, NSString *errorMessage))completion;

-(NSURLSessionDataTask *)realizarOferta:(RealizarOfertaAvulsaRequest *)request completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion;


-(NSURLSessionDataTask *)obterMinhasOfertasCompradorCompletion:(void (^)(ObterOfertasAvulsasResponse *response, NSString *errorMessage))completion;

-(NSURLSessionDataTask *)cadastrarDadosVendedor:(CadastrarDadosVendedorRequest *)cadastroRequest completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)obterMinhasOfertasCompletion:(void (^)(ObterOfertasAvulsasResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)obterPaletesVendidos:(void (^)(ObterPaletesVendidosResponse *response, NSString *errorMessage))completion;

-(NSURLSessionDataTask *)meusPaletescompletion:(void (^)(MeusPaletesResponse *response, NSString *errorMessage))completion;


- (NSURLSessionDataTask *)ofertasAvulsasCompletion:(void (^)(ObterOfertasAvulsasResponse *response, NSString *errorMessage))completion;


- (NSURLSessionDataTask *)aceitarOferta:(AceitarOfertaRequest *)request completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion;

-(NSURLSessionDataTask *)salvarPalete:(SalvarPaleteRequest *)request completion:(void (^)(ResponseBase *, NSString *))completion;
@end
