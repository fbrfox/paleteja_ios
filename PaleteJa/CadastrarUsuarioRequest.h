//
//  CadastrarUsuarioRequest.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "ModelBase.h"

@interface CadastrarUsuarioRequest : ModelBase

@property (nonatomic,strong) NSString* Email;
@property (nonatomic,strong) NSString* Nome;
@property (nonatomic,strong) NSString* Senha;
@property (nonatomic,strong) NSNumber* TipoUsuario;



-(id)initWithEmail:(NSString *)email nome:(NSString *)nome senha:(NSString *)senha andTipoUsuario:(NSNumber *)tipoUsuario;


@end
