//
// Created by Movida on 22/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import "MinhasOfertasDataSource.h"
#import "Oferta.h"
#import "MinhasOfertasViewController.h"
#import "SWTableViewCell.h"

#define kCellOferta @"cellOferta"

@implementation MinhasOfertasDataSource {

}
- (instancetype)initWithOfertas:(NSArray <Oferta> *)Ofertas andViewController:(MinhasOfertasViewController *)controller {
    self = [super init];
    if (self) {
        self.Ofertas = Ofertas;
        self.controller = controller;
    }

    return self;
}

+ (instancetype)sourceWithOfertas:(NSArray <Oferta> *)Ofertas {
    return [[self alloc] initWithOfertas:Ofertas andViewController:NULL];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (_Ofertas.count == 0)
        [UIHelper mostrarAlertaInfo:@"Você não tem nenhuma oferta cadastrada" viewController:_controller];

    return _Ofertas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {


    Oferta *oferta = _Ofertas[(NSUInteger) indexPath.row];

    SWTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellOferta forIndexPath:indexPath];

    cell.rightUtilityButtons = [self rightButtons];
    cell.delegate = self;


    UILabel *lbNome = [cell viewWithTag:tagNome];
    UILabel *lbQuantidade = [cell viewWithTag:1001];
    UILabel *lbStatus = [cell viewWithTag:1002];
    UILabel *lbPreco = [cell viewWithTag:1003];


    lbNome.text = oferta.TipoProdutoDescricao;
    lbQuantidade.text = [NSString stringWithFormat:@"%d unidade(s)", oferta.Quantidade];
    lbStatus.text = oferta.OfertaAceita ? @"Oferta Aceita" : @"Oferta Pendente";
    lbPreco.text = [UIHelper formatarPreco:oferta.PrecoPago];


    return cell;
}

- (NSArray *)rightButtons {
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
                    [UIColor redColor] title:@"Excluir"];


    return rightUtilityButtons;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {

    [UIHelper mostraAlertaConfimacao:@"Deseja realmente excluir está oferta?" delegate:self.controller viewController:self.controller];

}


@end
