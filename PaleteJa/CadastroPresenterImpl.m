//
//  CadastroPresenterImpl.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 19/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "CadastroPresenterImpl.h"
#import "ValidacaoHelper.h"
#import "APIClient.h"
#import "AllRequestResponse.h"

@implementation CadastroPresenterImpl

@synthesize view;
@synthesize typeUser;

-(void) initWithView: (id < CadastroView >) theView typeUser:(NSNumber*) type{
    
    self.view = theView;
    self.typeUser = type;
}


-(void)onRegisterClicked{
    
    if([self validateFields]){
        
        CadastrarUsuarioRequest *request = [self createRequest];
        [self callServerApi:request];
    }
    
}



-(void)callServerApi:(CadastrarUsuarioRequest *)request{
    
    [view showProgress];
    
    [[APIClient sharedManager]cadastrarUsuario:request completion:^(AutenticarResponse *response, NSString *errorMessage) {
        
        [view hideProgress];
        if(errorMessage)
            [view showAlertError:errorMessage];
        else if(response.Sucesso)
            [view registerWithSuccess:response.Autenticacao];
        else
            [view showAlertError:response.Mensagem];
    }];
}

-(CadastrarUsuarioRequest *)createRequest{
    
    CadastrarUsuarioRequest* request = [[CadastrarUsuarioRequest alloc]initWithEmail:view.getEmail nome:view.getNome
                                                                               senha:view.getSenha andTipoUsuario:self.typeUser];

    return request;
}

-(BOOL)validateFields{
    
    NSMutableString* error = [NSMutableString new];
    
    if([ValidacaoHelper validarString:view.getNome])
        [error appendString:@"O Campo nome é obrigatório\n"];
    
    if([ValidacaoHelper validarEmail:view.getEmail])
        [error appendString:@"O Campo e-mail é inválido\n"];
    
    if([ValidacaoHelper validarString:view.getSenha])
        [error appendString:@"O Campo senha é obrigatório\n"];
    
    if([error isEqualToString:@""])
    {
        [view showAlertError:error];
        return NO;
    }
    
    return YES;
    
}

@end
