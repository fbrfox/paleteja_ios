//
//  MinhasOfertasAceitasViewController.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "MinhasOfertasAceitasViewController.h"
#import "Oferta.h"
#import "DadosOfertaViewController.h"

@interface MinhasOfertasAceitasViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    
    __weak IBOutlet UITableView *tbMinhasOfertasAceitas;
    
}
@end

@implementation MinhasOfertasAceitasViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self carregarMinhasOfertas];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)carregarMinhasOfertas{
    
    [UIHelper showProgress];
    
    [[APIClient sharedManager] obterMinhasOfertasCompletion:^(ObterOfertasAvulsasResponse *response, NSString *errorMessage) {
        
        [UIHelper hideProgress];
        
        self.minhasOfertas = response.Ofertas;

        [tbMinhasOfertasAceitas reloadData];
        
    
        
    }];

    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    Oferta *oferta = _minhasOfertas[(NSUInteger) indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellOferta" forIndexPath:indexPath];
    
    
    UILabel *lbNome = [cell viewWithTag:1000];
    UILabel *lbQuantidade = [cell viewWithTag:1001];
    UILabel *lbPreco = [cell viewWithTag:1003];
    
    
    lbNome.text = [oferta.TipoProduto isEqualToNumber:@1] ? @"Pedido de Palete Novo" : @"Pedido de Palete Usado";
    lbQuantidade.text = [NSString stringWithFormat:@"%d unidade(s) solicitadas", oferta.Quantidade];
    
    lbPreco.text =[NSString stringWithFormat:@"Preço ofertado:%@",[UIHelper formatarPreco:oferta.PrecoPago]];
    
    
    return cell;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _minhasOfertas.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    Oferta *oferta = _minhasOfertas[(NSUInteger) indexPath.row];

    DadosOfertaViewController *dadosOfertaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DadosOfertaViewController"];
    dadosOfertaViewController.oferta = oferta;
    [self.navigationController pushViewController:dadosOfertaViewController animated:YES];
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
