//
//  UsuariosPaletesDataSource.m
//  PaleteJa
//
//  Created by Movida on 01/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "UsuariosPaletesDataSource.h"
#import "Usuario.h"

#define kCellUsuarioIdentifier @"cellUsuario"
#define kTagImage 1000
#define kTagNome 1001
#define kTagPreco 1002

@implementation UsuariosPaletesDataSource


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(_usuarios.count == 0)
        [UIHelper mostrarAlertaInfo:@"Nenhum vendedor encontrado, para o filtro definido" viewController:_viewController];
    return _usuarios.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Usuario* usuario = [_usuarios objectAtIndex:indexPath.row];
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellUsuarioIdentifier forIndexPath:indexPath];
    UIImageView *image = [cell viewWithTag:kTagImage];
    UILabel* lbNome = [cell viewWithTag:kTagNome];
    UILabel* lbPreco = [cell viewWithTag:kTagPreco];
    
    
    
    [image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:UrlFormatPerfilUsuario,usuario.UsuarioId]]];
    [image setClipsToBounds:YES];
    lbNome.text = usuario.Nome;
    lbPreco.text = usuario.PrecoMedio;
    
    
    return cell;
    
    
}

-(id)initWithUsuarios:(NSArray<Usuario>*)usuarios andViewController:(UIViewController *)controller{
    
    self = [super init];
    if(self){
        
        self.usuarios = usuarios;
        self.viewController = controller;
    }
    
    return self;
}

@end
