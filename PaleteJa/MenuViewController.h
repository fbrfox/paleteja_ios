//
//  MenuViewController.h
//  PaleteJa
//
//  Created by Movida on 02/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuView.h"

@class MenuPresenterImpl;

@interface MenuViewController : BaseViewController<MenuView>


@property (weak, nonatomic) IBOutlet UIImageView *iv_perfil;
@property (weak, nonatomic) IBOutlet UILabel *lbNome;

@property MenuPresenterImpl *presenter;

@end
