//
//  MinhasOfertasViewController.m
//  PaleteJa
//
//  Created by Movida on 22/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "MinhasOfertasViewController.h"
#import "MinhasOfertasDataSource.h"
#import "MInhasOfertasPresenterImpl.h"

@interface MinhasOfertasViewController ()
{
    __weak IBOutlet UITableView *tbOfertas;
    
}
@end

@implementation MinhasOfertasViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    tbOfertas.delegate = self;
    tbOfertas.tableFooterView = [UIView new];
    _presenter = [[MInhasOfertasPresenterImpl alloc] init];
    [_presenter initWithView:self];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onBackClicked:(id)sender {

    if(_origem == 1)
        [self.navigationController popToRootViewControllerAnimated:YES];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (void)onListOfertasReceived:(NSArray <Oferta> *)ofertas {

    _ofertas = ofertas;
    _dataSource = [[MinhasOfertasDataSource alloc] initWithOfertas:_ofertas andViewController:self];
    tbOfertas.dataSource = _dataSource;
    [tbOfertas reloadData];
}

- (void)showLoading {

    [UIHelper showProgress];

}

- (void)hideLoading {

    [UIHelper hideProgress];
}

- (void)showAlertErro:(NSString *)error {

    [UIHelper mostrarAlertaErro:error viewController:self];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

*/

- (void)alertControllerSimClicked {

}

- (void)alertControllerNaoClicked {

}


@end
