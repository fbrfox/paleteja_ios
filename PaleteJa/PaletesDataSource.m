//
//  PaletesDataSource.m
//  PaleteJa
//
//  Created by Movida on 05/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "PaletesDataSource.h"
#import "Palete.h"

#define kCellIdentifier @"cellPalete"
#define kTagNome 1001
#define kTagPreco 1002
#define kTagDescricao 1003
#define kTagStatus 1004


@implementation PaletesDataSource

- (id)initWithPaletes:(NSArray <Palete> *)paletes {
    self = [super init];

    if (self) {

        self.paletes = paletes;

    }

    return self;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _paletes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cellTablet = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];

    UILabel *lbNome = [cellTablet viewWithTag:kTagNome];
    UILabel *lbPreco = [cellTablet viewWithTag:kTagPreco];
    UILabel *lbDescricao = [cellTablet viewWithTag:kTagDescricao];
    UILabel *lbStatus = [cellTablet viewWithTag:kTagStatus];

    Palete *palete = _paletes[(NSUInteger) indexPath.row];

    lbNome.text = palete.TipoProdutoDesc;
    lbPreco.text = [UIHelper formatarPreco:palete.PrecoPromocao];
    lbDescricao.text = palete.Descricao;
    lbStatus.text = palete.Saldo > 0 ? @"Em estoque" : @"Indisponível";


    return cellTablet;
}




@end
