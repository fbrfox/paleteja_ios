//
//  Oferta.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 18/01/18.
//  Copyright © 2018 phep. All rights reserved.
//


/*
 
 PrecoPago": 10.9,
 "TipoEntrega": "Para Entregar",
 "EnderecoEntrega": "Rua noêmia 115 casa 1",
 "Latitude": -23.5034999,
 "Longitude": -46.714765,
 "Quantidade": 50,
 "Usuario": {
 "UsuarioId": 15,
 "Nome": "Pedro Henrique Borges",
 "PrecoMedio": null,
 "Paletes": null,
 "Endereco": "Rua nôemia 115, casa 1",
 "Telefone": "1139788690",
 "Celular": "11961066283"
 },
 "IdOferta": 1,
 "TipoProduto": 1,
 "OfertaAceita": false,
 "TipoProdutoDescricao": null
 
 */

#import "ModelBase.h"
#import "Usuario.h"


@protocol Oferta <NSObject>


@end

@interface Oferta : ModelBase

@property(nonatomic,strong) NSNumber* PrecoPago;
@property(nonatomic,strong) NSString* TipoEntrega;
@property(nonatomic,strong) NSString* EnderecoEntrega;
@property(nonatomic,strong) NSNumber* Latitude;
@property(nonatomic,strong) NSNumber* Longitude;
@property(nonatomic) int Quantidade;
@property(nonatomic,strong) Usuario* Usuario;
@property(nonatomic,strong) NSNumber* IdOferta;
@property(nonatomic,strong) NSNumber* TipoProduto;
@property(nonatomic) BOOL OfertaAceita;
@property(nonatomic,strong) NSString* TipoProdutoDescricao;


@end
