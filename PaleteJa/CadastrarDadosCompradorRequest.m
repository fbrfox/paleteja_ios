//
//  CadastrarDadosCompradorRequest.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "CadastrarDadosCompradorRequest.h"

@implementation CadastrarDadosCompradorRequest

-(id)initWithDocumento:(NSString *)documento foto:(NSString *)foto telefoneFixo:(NSString *)telefone celular:(NSString *)celular endereco:(NSString *)endereco andCelularWhatsapp:(BOOL)whatsapp{
    
    self = [super init];
    
    if(self){
        
        self.Documento = documento;
        self.Foto = foto;
        self.TelefoneFixo = telefone;
        self.Celular = celular;
        self.Endereco = endereco;
        self.CelularWhatsapp = whatsapp;
    }
    
    return self;
    
    
}

@end
