//
//  HomeViewController.h
//  PaleteJa
//
//  Created by Movida on 01/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseGPSViewController.h"
#import "HomeView.h"

#import "UsuariosPaletesDataSource.h"


@class HomePresenterImpl;

@interface HomeViewController : BaseGPSViewController <HomeView,TextFieldPickerHelperDelegate, UITableViewDelegate>



@property NSArray* estados;
@property UsuariosPaletesDataSource *dataSource;
@property HomePresenterImpl *presenter;
@property int indexSelected;
@property NSArray<Usuario> *usuarios;


@property (weak, nonatomic) IBOutlet UILabel *tfPrecoMedio;
@property (weak, nonatomic) IBOutlet UILabel *tfPrecoMedio2;
@property (weak, nonatomic) IBOutlet UITableView *tbVendedores;
@property (weak, nonatomic) IBOutlet TextFieldPickerHelper *tfEstado;
@property (weak, nonatomic) IBOutlet UIButton *btMinhaLocalizacao;
@property (weak, nonatomic) IBOutlet UILabel *lbKM;
@property (weak, nonatomic) IBOutlet UISlider *slKm;

@end
