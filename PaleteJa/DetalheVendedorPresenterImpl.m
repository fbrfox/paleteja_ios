//
// Created by Movida on 05/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import "DetalheVendedorPresenterImpl.h"
#import "DetalheVendedorView.h"
#import "Usuario.h"


@implementation DetalheVendedorPresenterImpl {

}

@synthesize view,usuario;


- (void)initWith:(id <DetalheVendedorView>)view1 andUsuario:(Usuario *)usuario1 {

    self.view = view1;
    self.usuario = usuario1;
    [self.view setName:usuario1.Nome foto:[NSString stringWithFormat:UrlFormatPerfilUsuario,usuario.UsuarioId] andPaletes:usuario1.Paletes];

}

- (void)selectPaleteToOrder:(int)paleteIndex {

    [self.view toOferta:paleteIndex];
}


@end