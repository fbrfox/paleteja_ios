//
//  SelecionarTipoUsuarioPresenterImpl.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 19/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "SelecionarTipoUsuarioPresenterImpl.h"

@implementation SelecionarTipoUsuarioPresenterImpl

@synthesize view;


-(void)buttonVendedorClicked{
    
    [view selectTypeRegister:@1];
}

-(void)buttonCompradorClicked{
    
    [view selectTypeRegister:@2];
}

-(void)initWithView:(id<SelecionarTipoUsuarioView>)theView{
    
    self.view = theView;
}

@end
