//
// Created by Pedro  on 19/04/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ObterPaletesVendidosResponse : ResponseBase


@property (nonatomic,strong) NSNumber *Quantidade;
@property (nonatomic, strong) NSNumber *Preco;

@end