//
// Created by Movida on 08/10/15.
// Copyright (c) 2015 UvaRosa. All rights reserved.
//

#import "UITextFieldMaskHelper.h"
#import "ValidacaoHelper.h"
#import "EnumHelper.h"
#import "UIHelper.h"

NSString *const CPFMASK = @"###.###.###-##";
NSString *const CEPMASK = @"#####-###";
NSString *const TELEFONEMASK = @"(##)####-####";
NSString *const CELULARMASK = @"(##)#####-####";

@implementation UITextFieldMaskHelper {

}

-(BOOL)isEmpty{

    return [self.text isEqualToString:@""];
}


-(NSString *)textUnmasked{

    return [ValidacaoHelper  removerCaracteres:self.text];
}




- (void)configuraNumberPadComDoneButtontarget:(id)target action:(SEL)action {


    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Concluir"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:target
                                                                  action:action];
    doneButton.tag = self.tag;

    UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];

    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexableItem, doneButton, nil]];

    self.inputAccessoryView = keyboardDoneButtonView;

}


- (void)configuraMaskedTextfield:(MaskType)type target:(id)target action:(SEL)action{

    switch (type){

        case Telefone:
            self.mask = TELEFONEMASK;
            break;
        case CPF:
            self.mask = CPFMASK;
            break;
        case CEP:
            self.mask = CEPMASK;
            break;
        case Celular:
            self.mask = CELULARMASK;
            break;
    }

    [self configuraNumberPadComDoneButtontarget:target action:action];
}

-(BOOL)validEmail{
    return [ValidacaoHelper validarEmail:self.text];
}

-(BOOL)validCPF{
    return [ValidacaoHelper validarCPF:self.text];
}



@end
