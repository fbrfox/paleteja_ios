//
//  AppDelegate.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/11/17.
//  Copyright © 2017 phep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


- (void)changeRootViewController:(UIViewController *)viewController;
@end

