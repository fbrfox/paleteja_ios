//
//  OfertasAvulsasViewController.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 25/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MeusPaletesViewController.h"

@protocol Oferta;


@interface OfertasAvulsasViewController : UIViewController

@property NSArray<Oferta> *ofertas;

@property MeusPaletesViewController * meusPaletesVC;

@end
