//
//  UsuariosPaletesDataSource.h
//  PaleteJa
//
//  Created by Movida on 01/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Usuario;

@interface UsuariosPaletesDataSource : NSObject<UITableViewDataSource>

@property NSArray<Usuario> *usuarios;

@property(nonatomic, strong) UIViewController *viewController;

-(id)initWithUsuarios:(NSArray<Usuario>*)usuarios andViewController:(UIViewController *)controller;

@end
