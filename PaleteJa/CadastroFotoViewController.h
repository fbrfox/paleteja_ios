//
//  CadastroFotoViewController.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 19/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CadastroFotoView.h"
#import "CadastroFotoPresenterImpl.h"

@interface CadastroFotoViewController : BaseViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate,CadastroFotoView>


@property Autenticacao *autenticacao;
@property CadastroFotoPresenterImpl* presenter;
@property NSString* imageSelected;
@property (weak, nonatomic) IBOutlet UIImageView *ivPerfil;

@end
