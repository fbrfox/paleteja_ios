//
// Created by Movida on 06/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OfertaView;
@class Palete;

@protocol OfertaPresenter <NSObject>

@property id <OfertaView> ofertaView;
@property Palete* palete;
@property UIViewController *viewController;



-(void)initWithOfertaView:(id<OfertaView> )view viewController:(UIViewController *)controller andPalete:(Palete *)oPalete;
-(void)onRegisterOferta;
- (void)onAlertClickOk;
-(void)onTextEnderecoBeginEditing;




@end