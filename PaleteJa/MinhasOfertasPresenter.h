//
// Created by Movida on 22/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MinhasOfertasView;

@protocol MinhasOfertasPresenter <NSObject>


-(void)initWithView:(id<MinhasOfertasView>)view;

@property id<MinhasOfertasView> minhasOfertasView;

@end