//
//  HomeVendedorViewController.h
//  PaleteJa
//
//  Created by Pedro  on 23/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol Palete;
@protocol Oferta;

@interface HomeVendedorViewController : UIViewController


@property(nonatomic, strong) NSArray<Palete> *paletes;
@property(nonatomic, strong) NSArray <Oferta> *ofertasAvulsas;
@property(nonatomic, strong) NSArray <Oferta> *minhasOfertas;
@end
