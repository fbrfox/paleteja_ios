//
// Created by Movida on 06/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OfertaView <NSObject>

-(int)getQuantidade;
-(double)getValor;
-(int)getTipoEntrega;
-(int)getTipoProduto;
-(GMSPlace *)getEndereco;
-(void)setEndereco:(GMSPlace *)place;

-(void)setPalete;

-(void)fillTipoEntrega:(NSArray *)tiposEntrega;
-(void)fillTipoProduto:(NSArray *)tiposProdutos;

-(void)showProgress;
-(void)hideProgress;
-(void)showAlertError:(NSString *)alertError;

- (void)showSucess:(NSString *)mensagem;
-(void)toMinhasOfertasView;
-(void)limpaCampos;

@end