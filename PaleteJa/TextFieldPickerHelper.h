//
// Created by pedro henrique borges on 3/6/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TextFieldPickerHelper;


@protocol TextFieldPickerHelperDelegate

@required
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView withTextField:(TextFieldPickerHelper *)sender;

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender;

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender;

- (void)doneClicked:(TextFieldPickerHelper *)sender withIndex:(NSInteger)index;


@end


@interface TextFieldPickerHelper : UITextField <UIPickerViewDataSource, UIPickerViewDelegate>

@property(nonatomic, strong) UIPickerView *picker;
@property(assign) id <TextFieldPickerHelperDelegate> delegate;

@property(nonatomic) NSInteger *row;

@end
