//
//  DetalheVendedorViewController.m
//  PaleteJa
//
//  Created by Movida on 05/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "DetalheVendedorViewController.h"
#import "Usuario.h"
#import "DetalheVendedorPresenterImpl.h"
#import "OfertaSegue.h"


@interface DetalheVendedorViewController () {

    __weak IBOutlet UITableView *tbPaletes;
    __weak IBOutlet UIImageView *ivFoto;
    __weak IBOutlet UILabel *lbNome;
}
@end

@implementation DetalheVendedorViewController


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {

    self.navigationController.navigationBarHidden = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    tbPaletes.delegate = self;
    tbPaletes.tableFooterView = [UIView new];
    _presenter = [[DetalheVendedorPresenterImpl alloc] init];
    [_presenter initWith:self andUsuario:_usuario];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setName:(NSString *)name foto:(NSString *)userFoto andPaletes:(NSArray <Palete> *)paletes {
    self.paletes = paletes;
    lbNome.text = name;
    [ivFoto sd_setImageWithURL:[[NSURL alloc] initWithString:userFoto]];
    [ivFoto setClipsToBounds:YES];
    [self initTableDataSource];

}

- (void)initTableDataSource {

    _dataSource = [[PaletesDataSource alloc] initWithPaletes:self.paletes];
    tbPaletes.dataSource = _dataSource;
    [tbPaletes reloadData];
}

- (void)toOferta:(int)indexPalete {

    _indexPalete = indexPalete;
    [self performSegueWithIdentifier:identifier sender:nil];


}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [_presenter selectPaleteToOrder:indexPath.row];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

*/


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:identifier]) {
        OfertaSegue *ns = (OfertaSegue *) segue;
        ns.palete = _paletes[(NSUInteger) _indexPalete];
    }
}


@end
