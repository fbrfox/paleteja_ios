//
//  MeuPerfilViewController.m
//  PaleteJa
//
//  Created by Pedro  on 02/04/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "MeuPerfilViewController.h"
#import "TipoProdutoRepository.h"
#import "TipoEntregaRepository.h"
#import "TipoProduto.h"
#import "TipoEntrega.h"
#import "ObterUsuarioResponse.h"
#import "Usuario.h"
#import "UsuarioConfiguracao.h"

@interface MeuPerfilViewController ()
{
    __weak IBOutlet UIImageView *ivPerfil;
    __weak IBOutlet UILabel *lbNome;
    __weak IBOutlet UITextField *tfTelefone;
    __weak IBOutlet UITextField *tfCelular;
    __weak IBOutlet UISwitch *swWhats;
    __weak IBOutlet UITextField *lbEndereco;
    __weak IBOutlet TextFieldPickerHelper *lbTipoProduto;
    __weak IBOutlet TextFieldPickerHelper *lbTipoEntrega;
    __weak IBOutlet UITextField *lbRaioEntrega;
    __weak IBOutlet UITextField *lbQuantidadeMinima;
    __weak IBOutlet UITextField *tfSite;
    __weak IBOutlet UIView *viewVendedor;
    
    GMSPlace *localSelecionado;

    int tipoEntregaSelecionado;
    int tipoProdutoSelecionado;
    __weak IBOutlet NSLayoutConstraint *constraintVendedor;
    
}
@end

@implementation MeuPerfilViewController {
    BOOL trocouLocal;
    NSString *_imageSelected;
    NSString *estado;
    NSString *cidade;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [super configdismissOnTap:@[tfTelefone,tfCelular,lbEndereco,lbTipoEntrega,lbTipoProduto,lbRaioEntrega,lbQuantidadeMinima]];

    lbTipoEntrega.delegate = lbTipoProduto.delegate = self;
    [self fillTiposEntrega];
    [self fillTiposProduto];

    [ivPerfil sd_setImageWithURL:ImagemPerfil];
    [ivPerfil setClipsToBounds:YES];

    if([[SharedPreferences userTipo] isEqualToNumber:@2])
    {
        constraintVendedor.constant = 0;
        viewVendedor.alpha = 0;
    }


    [UIHelper showProgress];

    [[APIClient sharedManager] obterUsuarioWithCompletion:^(ObterUsuarioResponse *response, NSString *errorMessage) {

        [UIHelper hideProgress];
        
        if(response.Sucesso){


            Usuario *usuario = response.Usuario;

            estado = usuario.Estado;
            cidade = usuario.Cidade;
            tfCelular.text = usuario.Celular;
            swWhats.on = usuario.CelularWhatsapp;
            tfTelefone.text = usuario.TelefoneFixo;
            tfSite.text = usuario.Site;
            UsuarioConfiguracao *configuracao = usuario.UsuarioConfiguracao;
            lbQuantidadeMinima.text = [NSString stringWithFormat:@"%@", configuracao.QuantidadeMinimaEntrega];
            lbRaioEntrega.text = [NSString stringWithFormat:@"%@", configuracao.RaioEntrega];
            lbEndereco.text = usuario.Endereco;
            lbNome.text = usuario.Nome;


            int iTipo = [configuracao.TipoProduto intValue] -1;
            TipoProduto *tipo = _tiposProduto[iTipo];
            tipoProdutoSelecionado = [configuracao.TipoProduto intValue];
            lbTipoProduto.text = tipo.descricao;


            int iEntrega = [configuracao.TipoEntrega intValue] -1;
            TipoEntrega *tipoEntrega = _tiposEntrega[iEntrega];
            tipoEntregaSelecionado = [configuracao.TipoEntrega intValue];
            lbTipoEntrega.text = tipoEntrega.descricao;

            

            
            
        }
        
        
    }];


}

- (void)fillTiposProduto {

    self.tiposProduto = [TipoProdutoRepository getTiposProduto];


    tipoProdutoSelecionado = 1;


}

- (void)fillTiposEntrega {

    self.tiposEntrega = [TipoEntregaRepository getTiposEntrega];

    tipoEntregaSelecionado = 1;


}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onCloseTap:(id)sender {

    [self dismissViewControllerAnimated:YES completion:nil];

}
- (IBAction)onImagemTap:(id)sender {
    
    [UIHelper showSelectionImage:self delegate:self];
    
}
- (IBAction)onRegistrarClick:(id)sender {


    CadastrarDadosVendedorRequest *request = [[CadastrarDadosVendedorRequest alloc] init];

    request.TipoUsuario = [SharedPreferences userTipo];
    request.Site = tfSite.text;
    request.RaioEntrega = @(lbRaioEntrega.text.intValue);
    request.QuantidadeMinimaEntrega = @(lbQuantidadeMinima.text.intValue);
    if(tipoProdutoSelecionado > 0)
        request.TipoProduto = @(tipoProdutoSelecionado);
    if(tipoEntregaSelecionado > 0)
        request.TipoEntrega = @(tipoEntregaSelecionado);

    request.TelefoneFixo = tfTelefone.text;
    request.Longitude = @(localSelecionado.coordinate.longitude);
    request.Latitude = @(localSelecionado.coordinate.latitude);
    request.Foto = _imageSelected;
    request.Cidade = cidade;
    request.Estado = estado;
    request.Endereco = lbEndereco.text;
    request.Celular = tfCelular.text;
    request.CelularWhatsapp = swWhats.on;




    [UIHelper showProgress];


    [[APIClient sharedManager] atualizarUsuario:request completion:^(ResponseBase *response, NSString *errorMessage) {

        [UIHelper hideProgress];


        if (errorMessage) {

            [UIHelper mostrarAlertaErro:errorMessage viewController:self];
        } else if (!response.Sucesso)
            [UIHelper mostrarAlertaErro:response.Mensagem viewController:self];
        else
            [UIHelper mostrarAlertaSucesso:response.Mensagem viewController:self];
    }];

}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView withTextField:(TextFieldPickerHelper *)sender {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender {
    if([sender isEqual:lbTipoProduto])
        return _tiposProduto.count;

    return _tiposEntrega.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender {

    if([sender isEqual:lbTipoProduto])
    {
        TipoProduto *tipo = _tiposProduto[row];
        return tipo.descricao;
    }
    else{

        TipoEntrega *tipo = _tiposEntrega[row];
        return tipo.descricao;
    }



}

- (void)doneClicked:(TextFieldPickerHelper *)sender withIndex:(NSInteger)index {


    [sender resignFirstResponder];

    if([sender isEqual:lbTipoProduto])
    {
        TipoProduto *tipo = _tiposProduto[index];
        tipoProdutoSelecionado = index + 1;
        lbTipoProduto.text = tipo.descricao;
    }
    else{

        TipoEntrega *tipo = _tiposEntrega[index];
        tipoEntregaSelecionado = index + 1;
        lbTipoEntrega.text = tipo.descricao;
    }


}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{



    if([textField isEqual:lbEndereco])
    {
        GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
        GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
        filter.type = kGMSPlacesAutocompleteTypeFilterAddress;
        acController.autocompleteFilter = filter;
        acController.delegate = self;
        [self presentViewController:acController animated:YES completion:nil];
        return NO;
    }

    return YES;


}



-(void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place{

    [viewController dismissViewControllerAnimated:YES completion:nil];
    trocouLocal = YES;
    localSelecionado = place;

    lbEndereco.text = localSelecionado.formattedAddress;

    for (GMSAddressComponent *addressComponent in localSelecionado.addressComponents){
        
        if([addressComponent.type isEqualToString:@"administrative_area_level_1"])
            estado = addressComponent.name;

        if([addressComponent.type isEqualToString:@"administrative_area_level_2"])
            cidade = addressComponent.name;
        
        
    }
    

}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error{

    trocouLocal = NO;
    [self dismissViewControllerAnimated:YES completion:nil];

}


-(void)wasCancelled:(GMSAutocompleteViewController *)viewController{

    trocouLocal = NO;
    [self dismissViewControllerAnimated:YES completion:nil];

}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    if(![textField isEqual:tfTelefone] && ![textField isEqual:tfCelular])
        return YES;

    int length = (int)[self getLength:textField.text];
    //NSLog(@"Length  =  %d ",length);

    if(length == 12)
    {
        if(range.length == 0)
            return NO;
    }

    if(length == 3)
    {
        NSString *num = [self formatNumber:textField.text];
        textField.text = [NSString stringWithFormat:@"(%@) ",num];

        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
    }
    else if(length == 7 && [textField isEqual:tfTelefone])
    {
        NSString *num = [self formatNumber:textField.text];
        //NSLog(@"%@",[num  substringToIndex:3]);
        //NSLog(@"%@",[num substringFromIndex:3]);

        textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];

        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];

    }
    else if(length == 8 && [textField isEqual:tfCelular])
    {
        NSString *num = [self formatNumber:textField.text];

        textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];

        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
    }

    return YES;
}

- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];

    NSLog(@"%@", mobileNumber);

    int length = (int)[mobileNumber length];
    if(length > 12)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-12];
        NSLog(@"%@", mobileNumber);

    }

    return mobileNumber;
}

- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];

    int length = (int)[mobileNumber length];

    return length;
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *, id> *)info {


    UIImage *pickedImage = info[UIImagePickerControllerOriginalImage];
    _imageSelected = [UIHelper imageToNSString:pickedImage];
    [picker dismissViewControllerAnimated:NO completion:nil];
    [ivPerfil setImage:pickedImage];
    [ivPerfil setClipsToBounds:YES];




}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {

    _imageSelected = nil;

}


@end
