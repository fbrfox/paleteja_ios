//
//  MeusPaletesViewController.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 25/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "MeusPaletesViewController.h"
#import "AceitarOfertaRequest.h"
#import "CadastroPaleteViewController.h"

@interface MeusPaletesViewController () <UITableViewDelegate, UITableViewDataSource, UIHelperDelegate> {

    __weak IBOutlet UITableView *tbPaletes;
}
@end

@implementation MeusPaletesViewController {
    BOOL alertPaleteSelecionado;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    tbPaletes.tableFooterView = [UIView new];


}

- (void)carregaPaletes {

    [UIHelper showProgress];

    [[APIClient sharedManager] meusPaletescompletion:^(MeusPaletesResponse *response, NSString *errorMessage) {

        [UIHelper hideProgress];

        self.paletes = response.Paletes;
        [tbPaletes reloadData];

    }];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onAddPalete:(id)sender {
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _paletes.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"paleteCell" forIndexPath:indexPath];


    UILabel *lbNome = [cell viewWithTag:1000];
    UILabel *lbQuantidade = [cell viewWithTag:1001];
    UILabel *lbPreco = [cell viewWithTag:1002];


    Palete *palete = _paletes[indexPath.row];

    lbNome.text = palete.TipoProdutoDesc;
    lbQuantidade.text = [NSString stringWithFormat:@"%d unidades de saldo", palete.Saldo];
    lbPreco.text = [NSString stringWithFormat:@"Preço alvo: %@", [UIHelper formatarPreco:palete.PrecoAlvo]];


    return cell;


}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {


    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    _paleteSelecionado = _paletes[indexPath.row];

    if (_idOferta) {
        alertPaleteSelecionado = YES;
        [UIHelper mostraAlertaConfimacao:@"Deseja selecionar este palete, para aceitar a oferta?" delegate:self viewController:self];
    } else {

        CadastroPaleteViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CadastroPaleteViewController"];
        vc.palete = _paleteSelecionado;

        [self.navigationController pushViewController:vc animated:YES];

    }
}

- (void)alertControllerSimClicked {

    if (alertPaleteSelecionado) {

        AceitarOfertaRequest *request = [AceitarOfertaRequest new];
        request.IdPalete = _paleteSelecionado.PaleteId;
        request.IdPedido = _idOferta;

        [UIHelper showProgress];

        [[APIClient sharedManager] aceitarOferta:request completion:^(ResponseBase *response, NSString *errorMessage) {

            [UIHelper hideProgress];

            if (errorMessage) {
                [UIHelper mostrarAlertaErro:errorMessage viewController:self];
            } else if (!response.Sucesso) {
                [UIHelper mostraAlertaConfimacao:@"Você não possui estoque disponível.\nDeseja atualizar o estoque?" delegate:self viewController:self];
                alertPaleteSelecionado = NO;

            } else
                [UIHelper mostrarAlertaSucesso:@"Oferta aceita com sucesso!" viewController:self delegate:self];

        }];
    }
    else{

        CadastroPaleteViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CadastroPaleteViewController"];
        vc.palete = _paleteSelecionado;

        [self.navigationController pushViewController:vc animated:YES];

    }

}

- (void)alertControllerNaoClicked {


}


- (void)alertControllerOkClicked {

    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [self carregaPaletes];
}


@end
