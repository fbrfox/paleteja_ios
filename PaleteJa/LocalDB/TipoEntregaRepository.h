//
// Created by Movida on 06/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol TipoEntrega;

@interface TipoEntregaRepository : NSObject

+(NSArray<TipoEntrega> *)getTiposEntrega;

@end