//
// Created by Movida on 06/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import "TipoProdutoRepository.h"
#import "TipoProduto.h"


@implementation TipoProdutoRepository {

}

+ (NSArray <TipoProduto> *)getTiposProduto {


    NSArray <TipoProduto> *tiposProduto = (NSArray <TipoProduto> *) @[[TipoProduto produtoWithIdTipoProduto:1 descricao:@"Produto Novo"],
                [TipoProduto produtoWithIdTipoProduto:2 descricao:@"Produto Usado"],[TipoProduto produtoWithIdTipoProduto:3 descricao:@"Ambos"] ];

    return tiposProduto;
}

@end