//
// Created by Movida on 06/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import "TipoEntregaRepository.h"
#import "TipoEntrega.h"


@implementation TipoEntregaRepository {

}


+ (NSArray <TipoEntrega> *)getTiposEntrega {

    NSArray *tipos = @[[TipoEntrega entregaWithIdTipoEntrega:1 descricao:@"Entrega"],[TipoEntrega entregaWithIdTipoEntrega:2 descricao:@"Retirada"],[TipoEntrega entregaWithIdTipoEntrega:3 descricao:@"Ambos"]];

    return (NSArray <TipoEntrega> *) tipos;
}

@end