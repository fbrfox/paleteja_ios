//
// Created by Movida on 02/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//


/*
 *
 * Acre	AC
Alagoas	AL
Amapá	AP
Amazonas	AM
Bahia	BA
Ceará	CE
Distrito Federal	DF
Espírito Santo	ES
Goiás	GO
Maranhão	MA
Mato Grosso	MT
Mato Grosso do Sul	MS
Minas Gerais	MG
Pará	PA
Paraíba	PB
Paraná	PR
Pernambuco	PE
Piauí	PI
Rio de Janeiro	RJ
Rio Grande do Norte	RN
Rio Grande do Sul	RS
Rondônia	RO
Roraima	RR
Santa Catarina	SC
São Paulo	SP
Sergipe	SE
Tocantins	TO
 * */

#import "EstadoRepository.h"


@implementation EstadoRepository {

}
+ (NSArray *)getEstados {

    NSArray *estados = @[@"AC", @"AL",@"AP",@"AM",@"BA",@"CE",@"DF",@"ES",@"GO",@"MA",@"MT",@"MS"
            ,@"MG",@"PA",@"PB",@"PR",@"PE",@"PI",@"RJ",@"RN",@"RS",@"RO",@"RR",@"SC",@"SP",@"SE",@"TO"];

    return estados;

}


@end