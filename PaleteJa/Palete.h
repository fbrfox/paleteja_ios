//
//  Palete.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//
/*
 
 "PaleteId": 1,
 "TipoProduto": 1,
 "TipoProdutoDesc": "Palete Novo",
 "PrecoAlvo": 10,
 "PrecoPromocao": 6.99,
 "NotaFiscal": false,
 "PrecoMedio": 13.49,
 "Saldo": 100,
 "Descricao": "Paletes praticamente novos",
 "TabelaA": 15,
 "TabelaB": 10,
 "TabelaC": 5,
 "QuantidadeA": 50,
 "QuantidadeB": 25,
 "QuantidadeC": 15
 
 */


#import "ModelBase.h"

@protocol Palete <NSObject>


@end

@interface Palete : ModelBase


@property(nonatomic,strong) NSNumber *PaleteId;
@property(nonatomic) int TipoProduto;
@property(nonatomic,strong) NSString *TipoProdutoDesc;
@property(nonatomic,strong) NSNumber *PrecoAlvo;
@property(nonatomic,strong) NSNumber *PrecoPromocao;
@property(nonatomic) BOOL NotaFiscal;
@property(nonatomic,strong) NSNumber *PrecoMedio;
@property(nonatomic) int Saldo;
@property(nonatomic,strong) NSString *Descricao;
@property(nonatomic) int TabelaA;
@property(nonatomic) int TabelaB;
@property(nonatomic) int TabelaC;
@property(nonatomic) int QuantidadeA;
@property(nonatomic) int QuantidadeB;
@property(nonatomic) int QuantidadeC;

@end
