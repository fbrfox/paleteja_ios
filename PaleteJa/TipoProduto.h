//
// Created by Movida on 06/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TipoProduto;

@interface TipoProduto : NSObject

@property int idTipoProduto;
@property NSString *descricao;

- (instancetype)initWithIdTipoProduto:(int)idTipoProduto descricao:(NSString *)descricao;

+ (instancetype)produtoWithIdTipoProduto:(int)idTipoProduto descricao:(NSString *)descricao;


@end