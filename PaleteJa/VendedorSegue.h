
//
//  VendedorSegue.h
//  PaleteJa
//
//  Created by Pedro  on 22/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface VendedorSegue : UIStoryboardSegue


@property CadastrarDadosVendedorRequest *request;


@end
