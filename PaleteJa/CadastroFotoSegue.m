//
//  CadastroFotoSegue.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 23/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "CadastroFotoSegue.h"
#import "CadastrarDadosComplementaresViewController.h"

@implementation CadastroFotoSegue


-(void)perform{
    
    
    UIViewController* src = self.sourceViewController;
    UIViewController* dst = self.destinationViewController;
    
    if([dst isKindOfClass:[CadastrarDadosComplementaresViewController class]]){
        CadastrarDadosComplementaresViewController* dvc = (CadastrarDadosComplementaresViewController*) dst;
        dvc.foto = _foto;
    }
    
    [src.navigationController pushViewController:dst animated:YES];
    
}


@end
