//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ValidacaoHelper : NSObject





+ (NSString *)validarStringNull:(NSString *)string;

+ (BOOL)validarString:(NSString *)string;

+ (BOOL)senhaConfere:(NSString *)senha comConfirmacaoSenha:(NSString *)confirmacaoSenha;

+ (BOOL)validarCPF:(NSString *)cpf;

+ (BOOL)validarEmail:(NSString *)checkString;

+ (BOOL)validarTelefone:(NSString *)telefone;

+ (NSString *)removerCaracteres:(NSString *)string;
@end
