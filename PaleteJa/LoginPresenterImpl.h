//
//  LoginPresenterImpl.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 22/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginView.h"
#import "LoginPresenter.h"


@interface LoginPresenterImpl : NSObject <LoginPresenter>

@end
