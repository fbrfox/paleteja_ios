//
//  SharedPreferences.h
//  Movida Condominio
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/04/17.
//  Copyright © 2017 Movida Locação de Veículos Ltda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedPreferences : NSObject

+(void)setUserToken:(NSString *)token;
+(void)setUserId:(NSNumber *)userId;
+(void)setUserName:(NSString *)name;
+(void)setUserTipo:(NSNumber *)local;
+(void)setDeviceToken:(NSString *)token;
+(void)setCadastroCompleto:(NSNumber *)usuarioNovo;

+(BOOL)getCadastroCompleto;

+(NSNumber *)userTipo;
+(NSString *)userName;
+(NSString *)userToken;
+(NSNumber *)userId;
+(NSString *)deviceToken;


@end
