//
//  TokenHelper.h
//  Movida Condominio
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 06/03/17.
//  Copyright © 2017 Movida Locação de Veículos Ltda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TokenHelper : NSObject

+(NSString *)retornaToken;

@end
