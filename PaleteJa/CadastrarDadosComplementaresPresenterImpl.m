//
//  CadastrarDadosComplementaresPresenterImpl.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 23/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "CadastrarDadosComplementaresPresenterImpl.h"

@implementation CadastrarDadosComplementaresPresenterImpl

@synthesize view;
@synthesize foto;
@synthesize viewController;


-(void)initWithView:(id<CadastrarDadosComplementaresView>)theView viewController:(UIViewController *)theViewController andFoto:(NSString *)fotoPerfil{
    

    self.view = theView;
    self.foto = fotoPerfil;
    self.viewController = theViewController;
}

-(void)onSalvarClicked{
    
    
    if(![self validateFields])
        return;
    
    

    if([[SharedPreferences userTipo] isEqualToNumber:@1])
    {
        CadastrarDadosVendedorRequest *request = [self createRequestVendedor];
        [view registerWithSucessToVendor:request];
    }
    else{
        CadastrarDadosCompradorRequest *request = [self createRequest];
        [self callServerApi:request];
    }
    
}

- (CadastrarDadosVendedorRequest *)createRequestVendedor {

    CadastrarDadosVendedorRequest *request = [[CadastrarDadosVendedorRequest alloc] init];
    request.Celular = view.getCelular;
    request.CelularWhatsapp = view.getIsWhatsapp;
//    request.Cidade = view.getPlace.
    request.Documento = view.getDocumento;
    request.Endereco = view.getEndereco;
//    request.Estado = view.getPlace;
    request.Foto = self.foto;
    request.Latitude = @(view.getPlace.coordinate.latitude);
    request.Longitude = @(view.getPlace.coordinate.longitude);
    request.TelefoneFixo = view.getTelefone;

    return request;
}


-(void)callServerApi:(CadastrarDadosCompradorRequest *)request{
    
    [view showProgress];
    
    [[APIClient sharedManager]cadastrarDadosComprador:request completion:^(ResponseBase *response, NSString *errorMessage) {
        
        [view hideProgress];
        if(errorMessage)
            [view showAlertError:errorMessage];
        else if(response.Sucesso)
        {
            [SharedPreferences setCadastroCompleto:@YES];
            [view registerWithSuccessToHome];
        }
        else
            [view showAlertError:response.Mensagem];
    }];
}

-(CadastrarDadosCompradorRequest *)createRequest{
    
    CadastrarDadosCompradorRequest* request = [[CadastrarDadosCompradorRequest alloc]initWithDocumento:view.getDocumento foto:self.foto telefoneFixo:view.getTelefone celular:view.getCelular endereco:view.getEndereco andCelularWhatsapp:view.getIsWhatsapp];
    
    
    return request;
}

-(BOOL)validateFields{
    
    NSMutableString* error = [NSMutableString new];
    
    
    if([ValidacaoHelper validarEmail:view.getDocumento])
        [error appendString:@"O campo documento é de preenchimento obrigatório, utilizamos o seu documento, para validar o processo de compra e venda\n"];
    
    if([ValidacaoHelper validarString:view.getTelefone])
        [error appendString:@"O Campo Telefone é obrigatório, utilizamos o seu número de telefone, para o contato entre o vendedor e comprador\n"];
    
    if([ValidacaoHelper validarString:view.getEndereco])
        [error appendString:@"O Campo Endereço é obrigatório, utilizamos o seu endereço, para que o comprador/vendedor saiba aonde buscar ou entregar\n"];
    
    if([error isEqualToString:@""])
    {
        [view showAlertError:error];
        return NO;
    }
    
    return YES;
    
}



// Places Methods

-(void)onTextEnderecoBeginEditing{
    
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
    filter.type = kGMSPlacesAutocompleteTypeFilterAddress;
    acController.autocompleteFilter = filter;
    acController.delegate = self;
    [self.viewController presentViewController:acController animated:YES completion:nil];
}


-(void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place{
    
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
    [self.view setEndereco:place];
    
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error{
    
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)wasCancelled:(GMSAutocompleteViewController *)viewController{
    
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
    
}


@end
