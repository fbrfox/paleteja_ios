//
// Created by Movida on 22/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SWTableViewCell.h"

@class MinhasOfertasViewController;


static const int tagNome = 1000;

@interface MinhasOfertasDataSource : NSObject<UITableViewDataSource, SWTableViewCellDelegate>

@property(nonatomic,strong) NSArray<Oferta>* Ofertas;

@property(nonatomic, strong) MinhasOfertasViewController *controller;

- (instancetype)initWithOfertas:(NSArray <Oferta> *)Ofertas andViewController:(MinhasOfertasViewController *)controller;

+ (instancetype)sourceWithOfertas:(NSArray <Oferta> *)Ofertas;


@end