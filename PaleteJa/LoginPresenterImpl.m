//
//  LoginPresenterImpl.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 22/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "LoginPresenterImpl.h"

@implementation LoginPresenterImpl


@synthesize view;



-(void)initWithView:(id<LoginView> )theView{
    
    
    self.view = theView;

}


-(void)onLoginClicked{

    if([self validateFields]){

        AutenticarRequest *request = [self createRequest];
        [self callServerApi:request];
    }

}


-(void)onEsqueciSenhaClicked{

    if([self validateFieldsEsqueciSenha]){

        AutenticarRequest *request = [self createRequest];

        [view showProgress];

        [[APIClient sharedManager] esqueciSenha:request completion:^(ResponseBase *response, NSString *errorMessage) {

            [view hideProgress];

            if(errorMessage)
                [view showAlertError:errorMessage];
            else if(response.Sucesso) {

                [view showAlertSucess:response.Mensagem];
            }
            else
                [view showAlertError:response.Mensagem];

        }];
    }

}



-(void)callServerApi:(AutenticarRequest *)request{
    
    [view showProgress];
    
    [[APIClient sharedManager]autenticar:request completion:^(AutenticarResponse *response, NSString *errorMessage) {
        
        [view hideProgress];
        if(errorMessage)
            [view showAlertError:errorMessage];
        else if(response.Sucesso) {

            [self sendToCorrectViewController:response];
        }
        else
            [view showAlertError:response.Mensagem];
    }];
}

- (void)sendToCorrectViewController:(AutenticarResponse *)response {

    if(response.CadastroCompleto)
        [view toHome];
    else
        [view registerWithSuccess:response.Autenticacao];


}

-(AutenticarRequest *)createRequest{
    
    AutenticarRequest* request = [[AutenticarRequest alloc]initWithEmail:view.getEmail andSenha:view.getSenha];
    return request;
}

-(BOOL)validateFields{
    
    NSMutableString* error = [NSMutableString new];
    

    if(![ValidacaoHelper validarEmail:view.getEmail])
        [error appendString:@"O Campo e-mail é inválido\n"];
    
    if(![ValidacaoHelper validarString:view.getSenha])
        [error appendString:@"O Campo senha é obrigatório\n"];
    
    if(![error isEqualToString:@""])
    {
        [view showAlertError:error];
        return NO;
    }
    
    return YES;
    
}

-(BOOL)validateFieldsEsqueciSenha{

    NSMutableString* error = [NSMutableString new];


    if(![ValidacaoHelper validarEmail:view.getEmail])
        [error appendString:@"O Campo e-mail é inválido\n"];


    if(![error isEqualToString:@""])
    {
        [view showAlertError:error];
        return NO;
    }

    return YES;

}

@end
