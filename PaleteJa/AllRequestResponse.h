//
//  AllRequestResponse.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AutenticarRequest.h"
#import "SalvarPaleteRequest.h"
#import "AceitarOfertaRequest.h"
#import "CadastrarUsuarioRequest.h"
#import "RealizarOfertaAvulsaRequest.h"
#import "CadastrarDadosVendedorRequest.h"
#import "CadastrarDadosCompradorRequest.h"
#import "AutenticarResponse.h"
#import "CadastroUsuarioResponse.h"
#import "MeusPaletesResponse.h"
#import "ObterOfertasAvulsasResponse.h"
#import "ObterUsuariosPaletesResponse.h"



@interface AllRequestResponse : NSObject

@end
