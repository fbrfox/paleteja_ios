//
// Created by Movida on 06/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import "TipoProduto.h"


@implementation TipoProduto {

}
- (instancetype)initWithIdTipoProduto:(int)idTipoProduto descricao:(NSString *)descricao {
    self = [super init];
    if (self) {
        self.idTipoProduto = idTipoProduto;
        self.descricao = descricao;
    }

    return self;
}

+ (instancetype)produtoWithIdTipoProduto:(int)idTipoProduto descricao:(NSString *)descricao {
    return [[self alloc] initWithIdTipoProduto:idTipoProduto descricao:descricao];
}

@end