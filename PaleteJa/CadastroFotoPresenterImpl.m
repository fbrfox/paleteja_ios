//
//  CadastroFotoPresenterImpl.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 23/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "CadastroFotoPresenterImpl.h"

@implementation CadastroFotoPresenterImpl

@synthesize view;

-(void)initWithView:(id<CadastroFotoView> )theView{
    
    self.view = theView;
    
}



-(void)nextStepClick{
    
    [self.view toNextStep];
}
@end
