//
//  Autenticacao.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "ModelBase.h"

@interface Autenticacao : ModelBase

@property(nonatomic,strong) NSString* Nome;
@property(nonatomic,strong) NSNumber* Id;
@property(nonatomic,strong) NSString* Token;
@property(nonatomic,strong) NSNumber* TipoUsuario;



@end
