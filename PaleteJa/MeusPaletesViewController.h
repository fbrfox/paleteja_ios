//
//  MeusPaletesViewController.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 25/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Palete.h"

@interface MeusPaletesViewController : UIViewController

@property NSArray<Palete> *paletes;

@property NSNumber *idOferta;

@property Palete *paleteSelecionado;

@end
