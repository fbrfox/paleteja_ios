//
//  CadastroDadosVendedorViewController.m
//  PaleteJa
//
//  Created by Pedro  on 21/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "CadastroDadosVendedorViewController.h"
#import "TipoProdutoRepository.h"
#import "TipoEntregaRepository.h"
#import "TipoProduto.h"
#import "TipoEntrega.h"

@interface CadastroDadosVendedorViewController ()
{
    __weak IBOutlet JVFloatLabeledTextField *tfSite;
    __weak IBOutlet JVFloatLabeledTextField *tfRaioEntrega;
    __weak IBOutlet TextFieldPickerHelper *tfTipoEntrega;
    __weak IBOutlet JVFloatLabeledTextField *tfQtdMinima;
    __weak IBOutlet TextFieldPickerHelper *tfTipoProduto;

    GMSPlace *localSelecionado;

    int tipoEntregaSelecionado;
    int tipoProdutoSelecionado;
    
}
@end

@implementation CadastroDadosVendedorViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [super configdismissOnTap:@[tfTipoEntrega,tfSite,tfRaioEntrega,tfQtdMinima,tfTipoProduto]];
    tfTipoEntrega.delegate = self;

    [self fillTiposEntrega];
    [self fillTiposProduto];

}

- (void)fillTiposProduto {

    self.tiposProduto = [TipoProdutoRepository getTiposProduto];

  
    tipoProdutoSelecionado = 1;
   

}

- (void)fillTiposEntrega {

    self.tiposEntrega = [TipoEntregaRepository getTiposEntrega];
  
    tipoEntregaSelecionado = 1;
   

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView withTextField:(TextFieldPickerHelper *)sender {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender {
    if([sender isEqual:tfTipoProduto])
        return _tiposProduto.count;

    return _tiposEntrega.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender {

    if([sender isEqual:tfTipoProduto])
    {
        TipoProduto *tipo = _tiposProduto[row];
        return tipo.descricao;
    }
    else{

        TipoEntrega *tipo = _tiposEntrega[row];
        return tipo.descricao;
    }



}

- (void)doneClicked:(TextFieldPickerHelper *)sender withIndex:(NSInteger)index {


    [sender resignFirstResponder];

    if([sender isEqual:tfTipoProduto])
    {
        TipoProduto *tipo = _tiposProduto[index];
        tipoProdutoSelecionado = index + 1;
        tfTipoProduto.text = tipo.descricao;
    }
    else{

        TipoEntrega *tipo = _tiposEntrega[index];
        tipoEntregaSelecionado = index + 1;
        tfTipoEntrega.text = tipo.descricao;
    }


}
- (IBAction)onSalvarClick:(id)sender {

    [self fillRequest];

    [UIHelper showProgress];

    [[APIClient sharedManager] cadastrarDadosVendedor:self.request completion:^(ResponseBase *response, NSString *errorMessage) {

        [UIHelper hideProgress];
        if(errorMessage)
            [UIHelper mostrarAlertaErro:errorMessage viewController:self];
        else if(response.Sucesso)
        {
            [SharedPreferences setCadastroCompleto:@YES];
            [UIHelper mostrarAlertaSucesso:@"Cadastro efetuado com sucesso!" viewController:self delegate:self];

        }
        else
            [UIHelper mostrarAlertaErro:response.Mensagem viewController:self];


    }];

}

- (void)fillRequest {

    self.request.TipoEntrega = @(tipoEntregaSelecionado);
    self.request.TipoProduto = @(tipoProdutoSelecionado);
    self.request.QuantidadeMinimaEntrega = @([tfQtdMinima.text intValue]);
    self.request.RaioEntrega = @([tfRaioEntrega.text intValue]);
    self.request.Site = tfSite.text;

}

- (void)alertControllerOkClicked {

    [Delegate changeRootViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateInitialViewController]];

}




@end
