//
//  MeusPaletesResponse.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

/*
 
 "PaleteId": 1,
 "TipoProduto": 1,
 "TipoProdutoDesc": "Palete Novo",
 "PrecoAlvo": 10,
 "PrecoPromocao": 6.99,
 "NotaFiscal": false,
 "PrecoMedio": 13.49,
 "Saldo": 100,
 "Descricao": "Paletes praticamente novos",
 "TabelaA": 15,
 "TabelaB": 10,
 "TabelaC": 5,
 "QuantidadeA": 50,
 "QuantidadeB": 25,
 "QuantidadeC": 15
 
 */


#import "ResponseBase.h"

@protocol Palete;

@interface MeusPaletesResponse : ResponseBase

@property(nonatomic,strong) NSArray<Palete>* Paletes;
@property(nonatomic,strong) NSNumber* PrecoMedio;



@end
