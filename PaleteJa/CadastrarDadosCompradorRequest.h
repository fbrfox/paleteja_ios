//
//  CadastrarDadosCompradorRequest.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "ModelBase.h"

@interface CadastrarDadosCompradorRequest : ModelBase

@property(nonatomic,strong) NSString* Documento;
@property(nonatomic,strong) NSString* Foto;
@property(nonatomic,strong) NSString* TelefoneFixo;
@property(nonatomic,strong) NSString* Celular;
@property(nonatomic,strong) NSString* Endereco;
@property(nonatomic) BOOL CelularWhatsapp;


-(id)initWithDocumento:(NSString *)documento foto:(NSString *)foto telefoneFixo:(NSString *)telefone
               celular:(NSString *)celular endereco:(NSString *)endereco andCelularWhatsapp:(BOOL)whatsapp;


@end
