//
// Created by Movida on 06/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import "TipoEntrega.h"


@implementation TipoEntrega {

}
- (instancetype)initWithIdTipoEntrega:(int)idTipoEntrega descricao:(NSString *)descricao {
    self = [super init];
    if (self) {
        self.idTipoEntrega = idTipoEntrega;
        self.descricao = descricao;
    }

    return self;
}

+ (instancetype)entregaWithIdTipoEntrega:(int)idTipoEntrega descricao:(NSString *)descricao {
    return [[self alloc] initWithIdTipoEntrega:idTipoEntrega descricao:descricao];
}

@end