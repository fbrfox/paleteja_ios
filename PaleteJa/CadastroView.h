//
//  CadastroVIew.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 19/01/18.
//  Copyright © 2018 phep. All rights reserved.
//
#import "Autenticacao.h"

@protocol CadastroView <NSObject>

-(NSString*)getNome;
-(NSString*)getEmail;
-(NSString*)getSenha;

-(void)showAlertError:(NSString *)error;
-(void)registerWithSuccess:(Autenticacao *)autenticacao;
-(void)showProgress;
-(void)hideProgress;

@end
