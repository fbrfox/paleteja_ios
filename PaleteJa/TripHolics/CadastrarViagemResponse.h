//
//  CadastrarViagemResponse.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ResponseBase.h"

@interface CadastrarViagemResponse : ResponseBase

@property(nonatomic,strong) NSArray *Usuarios;

@end
