//
//  ObterPerfilResponse.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 27/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ModelBase.h"

@interface ObterPerfilResponse : ResponseBase

@property(nonatomic,strong) NSString *Nome;
@property(nonatomic,strong) NSString *Email;
@property(nonatomic,strong) NSString *Cidade;
@property(nonatomic,strong) NSString *Estado;
@property(nonatomic,strong) NSString *Pais;
@property(nonatomic,strong) NSString *Sexo;
@property(nonatomic,strong) NSString *TelefonePais;
@property(nonatomic,strong) NSString *TelefoneNumero;
@property(nonatomic,strong) NSString *PerfilID;
@property(nonatomic,strong) NSString *Sobre;

@end
