//
//  AdicionarMatchRequest.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 27/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ModelBase.h"

@interface AdicionarMatchRequest : ModelBase

@property(nonatomic,strong) NSNumber *IdPerfil;
@property (nonatomic,strong) NSNumber *IdViagem;
@property (nonatomic,strong) NSNumber *IdAmigo;

@end
