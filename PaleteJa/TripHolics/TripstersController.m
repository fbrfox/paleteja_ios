//
//  TripstersController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 27/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "TripstersController.h"

@implementation TripstersController

-(id)initWithController:(UIViewController *)vc delegate:(id<TripstersControllerDelegate>)delegate{
    
    self = [super init];
    
    if(self) {
        
        self.vc = vc;
        self.delegate = delegate;
    }
    
    return self;
}


-(void)loadTripstersViagem:(NSNumber *)idViagem{
    
    [UIHelper showProgress];
    
    [[APIClient sharedManager]obterTripstersViagem:idViagem completion:^(PerfisViagemResponse *response, NSString *errorMessage) {
       
        [UIHelper hideProgress];
        
        if(errorMessage || !response.Sucesso)
            [_delegate loadTripstersErro:!response.Sucesso? response.Mensagem : errorMessage];
        else
            [_delegate loadTripstersSuccess:response.Usuarios];
        
    }];
    
}

-(void)loadMeusTripsters{
    
    [UIHelper showProgress];
    
    [[APIClient sharedManager]obterMeusTripstersCompletion:^(PerfisViagemResponse *response, NSString *errorMessage) {
        
        [UIHelper hideProgress];
        
        if(errorMessage || !response.Sucesso)
            [_delegate loadTripstersErro:!response.Sucesso? response.Mensagem : errorMessage];
        else
            [_delegate loadTripstersSuccess:response.Usuarios];
        
    }];
}

-(void)adicionarMatchIdUsuario:(NSNumber *)idUsuario viagemId:(NSNumber *)idViagem{
    
    [UIHelper showProgress];
    
    AdicionarMatchRequest *request = [AdicionarMatchRequest new];
    request.IdAmigo = idUsuario;
    request.IdViagem = idViagem;
    request.IdPerfil = IdUsuarioLogado;
    
    
    [[APIClient sharedManager] adicionarMatch:request completion:^(ResponseBase *response, NSString *errorMessage) {
       
        [UIHelper hideProgress];
        
        if(errorMessage || !response.Sucesso)
            [_delegate loadTripstersErro:errorMessage];
        else
            [_delegate matchAdicionadoComSucesso:response.Mensagem];
        
    }];
}

-(void)denunciarPerfil:(NSNumber *)idPerfil{
    
    [UIHelper showProgress];

    
    [[APIClient sharedManager] denunciarPerfil:idPerfil completion:^(ResponseBase *response, NSString *errorMessage) {
        
        [UIHelper hideProgress];
        
        if(errorMessage || !response.Sucesso)
            [_delegate loadTripstersErro:!response.Sucesso? response.Mensagem : errorMessage];

        else
            [_delegate perfilDenunciadoSuccess:response.Mensagem];
        
    }];

    
}

@end
