//
//  SexoHelper.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "SexoHelper.h"

@implementation SexoHelper

+(NSArray *)retornaSexos{
    
    NSMutableArray *sexos = [NSMutableArray new];
    
    SexoHelper *masculino = [SexoHelper new];
    masculino.descricao = @"Masculino";
    
    SexoHelper *feminino = [SexoHelper new];
    feminino.descricao = @"Feminino";
    
    SexoHelper *outro = [SexoHelper new];
    outro.descricao = @"Outro";
    
    
    [sexos addObject:masculino];
    [sexos addObject:feminino];
    [sexos addObject:outro];
    
    return sexos;
}

@end
