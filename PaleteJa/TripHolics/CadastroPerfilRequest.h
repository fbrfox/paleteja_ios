//
//  CadastroPerfilRequest.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 27/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ModelBase.h"

@interface CadastroPerfilRequest : ModelBase

@property(nonatomic,strong) NSString *Nome;
@property(nonatomic,strong) NSString *Email;
@property(nonatomic,strong) NSString *Senha;
@property(nonatomic,strong) NSString *FacebookID;
@property(nonatomic,strong) NSString *Cidade;
@property(nonatomic,strong) NSString *Pais;;
@property(nonatomic,strong) NSString *DigitalID;
@property(nonatomic,strong) NSString *Sexo;;
@property(nonatomic,strong) NSString *Foto;
@property(nonatomic,strong) NSString *TelefonePais;
@property(nonatomic,strong) NSString *TelefoneCodigo;
@property(nonatomic,strong) NSString *TelefoneNumero;
@property(nonatomic,strong) NSNumber *PerfilID;
@property(nonatomic,strong) NSNumber *FotoNova;
@property(nonatomic,strong) NSString *Sobre;

@end
