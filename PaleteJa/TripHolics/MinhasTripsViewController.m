//
//  MinhasTripsViewController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "MinhasTripsViewController.h"
#import "ExcluirTripButton.h"
#import "BaseController.h"
#import "MeusTripstersViewController.h"

@interface MinhasTripsViewController ()<UITableViewDelegate,UITableViewDataSource,LGPlusButtonsViewDelegate,MinhasTripsControllerDelegate,UIHelperDelegate>
{
    __weak IBOutlet UITableView *tbTrips;
    MinhasTripsController *controller;
    
    NSMutableArray<Viagem> *minhasTrips;
}
@end

@implementation MinhasTripsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [UIHelper addFloatButtonInView:self.view delegate:self];
    controller = [[MinhasTripsController alloc]initWithViewController:self delegate:self];
    
    [controller loadMinhasTrips];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    Viagem *viagem = minhasTrips[indexPath.row];
    MeusTripstersViewController *vc =[self.storyboard instantiateViewControllerWithIdentifier:@"MeusTripstersViewController"];
    vc.viagem =viagem;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return minhasTrips.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    Viagem *viagem = minhasTrips[indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellTrip" forIndexPath:indexPath];
    
    
    UILabel *lbDataInicio = [cell viewWithTag:100];
    UILabel *lbDataFim = [cell viewWithTag:101];
    UILabel *lbLocal = [cell viewWithTag:102];
    UILabel *lbMotivo = [cell viewWithTag:103];
    
    ExcluirTripButton *btExcluir = [cell viewWithTag:104];
    btExcluir.trip = viagem;
    
    
    lbDataFim.text = viagem.DataFim;
    lbDataInicio.text = viagem.DataInicio;
    lbLocal.text = viagem.Destino;
    lbMotivo.text = viagem.MotivoViagem;
    
    
    [btExcluir addTarget:self action:@selector(onExcluirTap:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
}

-(void)onExcluirTap:(ExcluirTripButton *)sender{
    
    Viagem *viagem = (Viagem *)sender.trip;
    [controller deletarTrip:viagem.ViagemID];
    [minhasTrips removeObject:viagem];
    
    
}

- (void)plusButtonsView:(LGPlusButtonsView *)plusButtonsView buttonPressedWithTitle:(NSString *)title description:(NSString *)description index:(NSUInteger)index{
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NovaTripViewController"];
    [self.navigationController pushViewController:vc animated:YES];

    
}

-(void)minhasTripsLoadSuccess:(NSArray<Viagem> *)trips msg:(NSString *)msg{
    
    minhasTrips = [trips mutableCopy];
    [tbTrips reloadData];
}

-(void)minhasTripsLoadError:(NSString *)msg{
    
    [UIHelper mostrarAlertaErro:msg viewController:self];
}

-(void)tripDeletada:(NSString *)msg{
    
    [UIHelper mostrarAlertaSucesso:msg viewController:self];
    [tbTrips reloadData];
}



@end
