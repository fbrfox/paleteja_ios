//
//  LoginRequest.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "LoginRequest.h"

@implementation LoginRequest

-(id)initWithFacebookID:(NSString *)facebookID{
    
    self = [super init];
    
    if(self){
        
        self.FacebookID = facebookID;
    }
    
    return self;
}

-(id)initWithEmail:(NSString *)email senha:(NSString *)senha{
    
    self = [super init];
    
    if(self){
        
        self.Email = email;
        self.Senha = senha;
    }
    
    return self;
}

@end
