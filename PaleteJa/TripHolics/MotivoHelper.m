//
//  MotivoHelper.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 25/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "MotivoHelper.h"

@implementation MotivoHelper


+(NSArray *)retornaMotivos{
    
    NSMutableArray *motivos = [NSMutableArray new];
    
    MotivoHelper *trabalho = [MotivoHelper new];
    trabalho.descricao = @"Trabalho";
    trabalho.idMotivo = 1;
    
    [motivos addObject:trabalho];

    
    
    
    MotivoHelper *turismo = [MotivoHelper new];
    turismo.descricao = @"Turismo";
    turismo.idMotivo = 2;
    
    [motivos addObject:turismo];
    
    return motivos;
}

@end
