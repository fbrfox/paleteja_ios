//
//  TripController.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CadastrarViagemRequest.h"

@protocol TripControllerDelegate <NSObject>

@optional

-(void)erroOnRegisterTrip:(NSString *)msg;
-(void)tripRegisteredSuccess:(NSArray *)usuarios msg:(NSString *)msg;

@end

@interface TripController : NSObject

@property(nonatomic,weak) id<TripControllerDelegate> delegate;

@property(nonatomic,strong) UIViewController *viewController;



-(id)initWithViewController:(UIViewController *)vc delegate:(id<TripControllerDelegate>)delegate;

-(void)cadastrarTrip:(CadastrarViagemRequest *)request;

@end
