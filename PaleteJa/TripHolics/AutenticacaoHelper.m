//
//  AutenticacaoHelper.m
//  Movida Condominio
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 09/03/17.
//  Copyright © 2017 Movida Locação de Veículos Ltda. All rights reserved.
//

#import "AutenticacaoHelper.h"

@implementation AutenticacaoHelper

+(void)logout{
    
    [SharedPreferences setUserToken:nil];
    [SharedPreferences setUserId:nil];
    [SharedPreferences setUserName:nil];
    [SharedPreferences setUserLocal:nil];
    
}

+(void)loginWithUsuario:(Usuario *)usuario token:(NSString *)token{

    
    [SharedPreferences setUserToken:token];
    [SharedPreferences setUserLocal:usuario.Localidade];
    [SharedPreferences setUserName:usuario.Nome];
    [SharedPreferences setUserId:usuario.Id];
    [SharedPreferences setUsuarioNovo:usuario.UsuarioNovo];
    
}


+(BOOL)isAutenticado{
    
    return [SharedPreferences userId] != NULL;
    
}

@end
