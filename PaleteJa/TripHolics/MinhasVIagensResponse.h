//
//  MinhasVIagensResponse.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 27/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ResponseBase.h"

@protocol Viagem;


@interface MinhasVIagensResponse : ResponseBase

@property(nonatomic,strong) NSArray<Viagem> *Viagens;


@end
