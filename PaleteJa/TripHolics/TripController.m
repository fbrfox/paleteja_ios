//
//  TripController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "TripController.h"


@implementation TripController

-(id)initWithViewController:(UIViewController *)vc delegate:(id<TripControllerDelegate>)delegate{
    
    self = [super init];
    
    if(self){
        
        self.viewController = vc;
        self.delegate = delegate;
    }
    return self;
    
}



-(void)cadastrarTrip:(CadastrarViagemRequest *)request{
    
    
    if(![self validarCampos:request])
    {
     
        return;
    }
    
    
    [UIHelper showProgress];
    
    [[APIClient sharedManager]cadastrarViagem:request completion:^(CadastrarViagemResponse *response, NSString *errorMessage) {
       
        [UIHelper hideProgress];
        
        if(response.Sucesso){
            
            [_delegate tripRegisteredSuccess:response.Usuarios msg:response.Mensagem];
        }
        else
            [_delegate erroOnRegisterTrip:errorMessage];
        
    }];
    
}

-(BOOL)validarCampos:(CadastrarViagemRequest *)request{
    
    NSMutableString *string = [NSMutableString new];
    
    if([request.DataInicio isEqualToString:@""])
        [string appendString:@"O campo data de início é obrigatório!\n"];
    
    if([request.DataFim isEqualToString:@""])
        [string appendString:@"O campo data fim é obrigatório!\n"];
    
    if([request.Acompanhante isEqualToString:@""])
        [string appendString:@"Selecione se viaja sozinho ou acompanhado\n"];
    
    if(!request.MotivoViagemID)
        [string appendString:@"Selecione o motivo da viagem\n"];
    
    
    if([string isEqualToString:@""])
        return YES;
    else
    {
        [UIHelper mostrarAlertaErro:string viewController:_viewController];
        return NO;
    }
}


@end
