//
//  SexoHelper.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SexoHelper : NSObject

@property(nonatomic,strong) NSString*descricao;

+(NSArray *)retornaSexos;

@end
