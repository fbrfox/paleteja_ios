//
//  CadastrarViagemRequest.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ModelBase.h"

@interface CadastrarViagemRequest : ModelBase

@property(nonatomic,strong) NSString* DataInicio;
@property(nonatomic,strong) NSString* Acompanhante;
@property(nonatomic,strong) NSString* DataFim;
@property(nonatomic,strong) NSString* CidadeDestino;
@property(nonatomic,strong) NSString* EstadoDestino;
@property(nonatomic,strong) NSString* PaisDestino;
@property(nonatomic,strong) NSNumber* LatitudeDestino;
@property(nonatomic,strong) NSNumber* LongitudeDestino;
@property(nonatomic,strong) NSNumber* MotivoViagemID;
@property(nonatomic,strong) NSNumber* PerfilID;




@end
