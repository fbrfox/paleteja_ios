//
//  AppDelegate.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 24/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "AppDelegate.h"
@import Firebase;
@import GooglePlaces;


@interface AppDelegate ()

@end

@implementation AppDelegate


-(void)changeRootViewController:(UIViewController *)viewController{
    
      self.window.rootViewController = viewController;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
                                                           NSFontAttributeName : [UIFont boldSystemFontOfSize:15.0f]}];

    if([AutenticacaoHelper isAutenticado])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
        
        self.window.rootViewController = [storyboard instantiateInitialViewController];

    }
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    // Add any custom logic here.
    
    
    [FIRApp configure];
    
    [GMSPlacesClient provideAPIKey:NSLocalizedString(@"PlacesAPI", nil)];
    
    [GADMobileAds configureWithApplicationID:NSLocalizedString(@"ADMobAppID", nil)];
    
    
    if ([[UIApplication sharedApplication]respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }

    
    
    return YES;
}




- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"My token is: %@", deviceToken);
    
    
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [SharedPreferences setDeviceToken:token];
    
    
    [self atualizarPush];
    
    
}


-(void)atualizarPush{
    
    AtualizarPushRequest *request =[AtualizarPushRequest new];
    request.TokenIos = [SharedPreferences deviceToken];
    request.IdPerfil = IdUsuarioLogado;
    
    [[APIClient sharedManager] atualizarPush:request completion:^(ResponseBase *response, NSString *errorMessage) {
        
    }];
    
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failed to get token, error: %@", error);
    
}


//-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
//    UIApplicationState state = [application applicationState];
//    if (state == UIApplicationStateActive) {
//
//
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"KNIP"
//                                                        message:notification.alertBody
//                                                       delegate:self cancelButtonTitle:@"Close"
//                                              otherButtonTitles:nil];
//
//        [alert show];
//    }
//}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Received notification APNS: %@", userInfo);
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Atenção" message:[userInfo objectForKey:@"alert"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
} 

@end
