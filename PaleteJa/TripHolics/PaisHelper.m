//
//  PaisHelper.m
//  Movida
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 24/07/17.
//  Copyright © 2017 Movida. All rights reserved.
//

#import "PaisHelper.h"

@implementation PaisHelper

+(NSArray *)retornaPaises{
    
    
    NSMutableArray *arrayPaises = [NSMutableArray new];
    
    PaisHelper *brasil = [[PaisHelper alloc]init];
    brasil.descricao = @"+55 Brasil";
    brasil.codigo = @"+55";
    
    [arrayPaises addObject:brasil];
    
    PaisHelper *eua = [[PaisHelper alloc]init];
    eua.descricao = @"+1 Estados Únidos";
    eua.codigo = @"+1";
    
    [arrayPaises addObject:eua];
    
    PaisHelper *australia = [[PaisHelper alloc]init];
    australia.descricao = @"+61 Austrália";
    australia.codigo = @"+61";
    
    [arrayPaises addObject:australia];
    
    
    PaisHelper *italia = [[PaisHelper alloc]init];
    italia.descricao = @"+39 Itália";
    italia.codigo = @"+39";
    
    
    [arrayPaises addObject:italia];
    
    PaisHelper *espanha = [[PaisHelper alloc]init];
    espanha.descricao = @"+34 Espanha";
    espanha.codigo = @"+34";
    
    [arrayPaises addObject:espanha];
    
    
    PaisHelper *portugal = [[PaisHelper alloc]init];
    portugal.descricao = @"+351 Portugal";
    portugal.codigo = @"+351";
    
    [arrayPaises addObject:portugal];
    
    
    PaisHelper *reinoUnido = [[PaisHelper alloc]init];
    reinoUnido.descricao = @"+44 Reino Unido";
    reinoUnido.codigo = @"+44";
    
    [arrayPaises addObject:reinoUnido];
    
    
    
    
    return  arrayPaises;
    
}

@end
