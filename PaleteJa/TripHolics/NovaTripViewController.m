//
//  NovaTripViewController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "NovaTripViewController.h"
#import "AcompanhanteHelper.h"
#import "MotivoHelper.h"
#import <HSDatePickerViewController.h>
#import "MenuViewController.h"
#import "AllRequestResponse.h"
#import "TripController.h"



@interface NovaTripViewController ()<TripControllerDelegate,TextFieldPickerHelperDelegate,HSDatePickerViewControllerDelegate,GMSAutocompleteViewControllerDelegate,UIAlertViewDelegate,UIHelperDelegate>
{
    
    __weak IBOutlet UITextField *tfLocalViagem;
    __weak IBOutlet UITextField *tfDataIda;
    __weak IBOutlet UITextField *tfDataVolta;
    __weak IBOutlet TextFieldPickerHelper *tfMotivo;
    __weak IBOutlet TextFieldPickerHelper *tfAcompanhante;
    
    NSArray*acompanhantes;
    NSArray*motivos;
    
    MotivoHelper* motivoSelecionado;
    AcompanhanteHelper *acompanhanteSelecionado;
    
    BOOL ida;
    GMSPlace *localSelecionado;
    
    
    TripController  *tripController;
}
@end

@implementation NovaTripViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    tripController = [[TripController alloc]initWithViewController:self delegate:self];
    
    
    [self setTitleNavigation:@"Nova Trip"];
    
    [super configdismissOnTap:@[tfLocalViagem,tfDataIda,tfDataVolta,tfMotivo,tfAcompanhante]];
    
    acompanhantes = [AcompanhanteHelper retornaAcompanhantes];
    motivos = [MotivoHelper retornaMotivos];
    
    tfAcompanhante.delegate = tfMotivo.delegate = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView withTextField:(TextFieldPickerHelper *)sender{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender{
    
    if([sender isEqual:tfMotivo])
        return motivos.count;
    else
        return acompanhantes.count;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender{
    
    if([sender isEqual:tfMotivo]){
        
        MotivoHelper *motivo = motivos[row];
        return motivo.descricao;
    }
    else{
        
        AcompanhanteHelper *acompanhante = acompanhantes[row];
        return acompanhante.descricao;
    }
}

- (void)doneClicked:(TextFieldPickerHelper *)sender withIndex:(NSInteger)index{
    
    if([sender isEqual:tfMotivo]){
        
        motivoSelecionado = motivos[index];
        tfMotivo.text = motivoSelecionado.descricao;
        
        
    }
    else{
        AcompanhanteHelper *acompanhante = acompanhantes[index];
        tfAcompanhante.text = acompanhante.descricao;
        acompanhanteSelecionado = acompanhante;
    }
    
    [super dismissKeyboard];
    
}




-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if([textField isEqual:tfDataIda])
    {
        ida = YES;
        [self showDatePicker];
        return NO;
    }
    else if([textField isEqual:tfDataVolta]){
        ida = NO;
        [self showDatePicker];
        return NO;
    }else if([textField isEqual:tfLocalViagem]){
        
        [self showPlacesAutoComplete];
        return NO;
    }
    
    return YES;
    
}


- (void)showDatePicker {
    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    hsdpvc.delegate = self;
    hsdpvc.minDate = [NSDate date];
    hsdpvc.backButtonTitle  = @"Cancelar";
    hsdpvc.confirmButtonTitle =@"Selecionar";
    
    [self presentViewController:hsdpvc animated:YES completion:nil];
}

- (void)showPlacesAutoComplete {
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
    filter.type = kGMSPlacesAutocompleteTypeFilterCity;
    acController.autocompleteFilter = filter;
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
}

-(void)hsDatePickerPickedDate:(NSDate *)date{
    
    if(ida)
        tfDataIda.text = [UIHelper dataParaString:date formato:@"dd/MM/yyyy"];
    else
        tfDataVolta.text = [UIHelper dataParaString:date formato:@"dd/MM/yyyy"];
    
}


-(void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place{
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    tfLocalViagem.text = place.name;
    localSelecionado = place;
    
    
    
}

-(void)viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

-(void)wasCancelled:(GMSAutocompleteViewController *)viewController{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    
}

- (IBAction)onCadastrarTap:(id)sender {
    
    CadastrarViagemRequest *request =[CadastrarViagemRequest new];
    
    
    
    request.DataInicio = tfDataIda.text;
    request.DataFim = tfDataVolta.text;
    request.Acompanhante = tfAcompanhante.text;
    
    NSArray *endereco = [localSelecionado.formattedAddress componentsSeparatedByString:@","];

    
    if(endereco.count > 2)
    {
        request.CidadeDestino = [[endereco[0] stringByReplacingOccurrencesOfString:@"," withString:@""] stringByTrimmingCharactersInSet:
        [NSCharacterSet whitespaceCharacterSet]];
        request.EstadoDestino = [[endereco[1] stringByReplacingOccurrencesOfString:@"," withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        request.PaisDestino = [[endereco[2] stringByReplacingOccurrencesOfString:@"," withString:@""] stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    }
    else{
        
        request.CidadeDestino = [[endereco[0] stringByReplacingOccurrencesOfString:@"," withString:@""] stringByTrimmingCharactersInSet:
                                 [NSCharacterSet whitespaceCharacterSet]];
        request.EstadoDestino = [[endereco[0] stringByReplacingOccurrencesOfString:@"," withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        request.PaisDestino = [[endereco[1] stringByReplacingOccurrencesOfString:@"," withString:@""] stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    }
    
    request.LatitudeDestino = @(localSelecionado.coordinate.latitude);
    request.LongitudeDestino = @(localSelecionado.coordinate.longitude);
    
    request.MotivoViagemID = @(motivoSelecionado.idMotivo);
    request.PerfilID = IdUsuarioLogado;
    
    
    [tripController cadastrarTrip:request];
    
    
    
}




-(void)erroOnRegisterTrip:(NSString *)msg{
    
    [UIHelper mostrarAlertaErro:@"Ocorreu um erro ao cadastrar sua viagem, tente mais tarde!" viewController:self];
    
}
-(void)tripRegisteredSuccess:(NSArray *)usuarios msg:(NSString *)msg{
    
    [UIHelper mostrarAlerta:@"Sucesso!" mensagem:msg delegate:self viewController:self];
    
}

-(void)alertControllerOkClicked{
    
     [self.navigationController popViewControllerAnimated:YES];
}





@end
