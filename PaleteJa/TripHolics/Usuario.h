//
//  Usuario.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ModelBase.h"

@protocol Usuario <NSObject>



@end

@interface Usuario : ModelBase

@property(nonatomic,strong) NSString *Nome;
@property(nonatomic,strong) NSString *Email;
@property(nonatomic,strong) NSString *Localidade;
@property(nonatomic,strong) NSString *Telefone;
@property(nonatomic,strong) NSNumber *Id;
@property(nonatomic,strong) NSNumber *Match;
@property(nonatomic,strong) NSNumber *UsuarioNovo;
@property(nonatomic,strong) NSString *Acompanhante;
@property(nonatomic,strong) NSString *Sexo;
@property(nonatomic,strong) NSString *Sobre;

@end
