//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//


#import <AFNetworking.h>
#import "AllRequestResponse.h"


@interface APIClient : AFHTTPSessionManager

+ (APIClient *)sharedManager;


- (NSURLSessionDataTask *)login:(LoginRequest *)loginRequest completion:(void (^)(CadastroPerfilResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)cadastrarViagem:(CadastrarViagemRequest *)request completion:(void (^)(CadastrarViagemResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)minhasViagens:(void (^)(MinhasVIagensResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)deletarViagens:(NSNumber *)idViagem completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)obterMeusTripstersCompletion:(void (^)(PerfisViagemResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)obterTripstersViagem:(NSNumber *)idViagem completion:(void (^)(PerfisViagemResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)adicionarMatch:(AdicionarMatchRequest *)request completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)obterProximaViagemCompletion:(void (^)(ProximaViagemResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)cadastraPerfil:(CadastroPerfilRequest *)loginRequest completion:(void (^)(CadastroPerfilResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)obterPerfilCompletion:(void (^)(ObterPerfilResponse *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)atualizarPerfil:(CadastroPerfilRequest *)loginRequest completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)denunciarPerfil:(NSNumber *)idPerfil completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion;

- (NSURLSessionDataTask *)atualizarPush:(AtualizarPushRequest *)request completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion;

@end
