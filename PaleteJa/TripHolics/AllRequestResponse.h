//
//  AllRequestResponse.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginRequest.h"
#import "CadastroPerfilResponse.h"
#import "CadastrarViagemRequest.h"
#import "CadastrarViagemResponse.h"
#import "MinhasVIagensResponse.h"
#import "PerfisViagemResponse.h"
#import "AdicionarMatchRequest.h"
#import "ProximaViagemResponse.h"
#import "CadastroPerfilRequest.h"
#import "ObterPerfilResponse.h"
#import "AtualizarPushRequest.h"


@interface AllRequestResponse : NSObject

@end
