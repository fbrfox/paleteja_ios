//
//  FacebookUser.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ModelBase.h"

@interface FacebookUser : ModelBase

@property(nonatomic,strong) NSString *idFacebook;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *email;
@property(nonatomic,strong) NSString *gender;
@property(nonatomic,strong) NSString *imageUrl;


@end
