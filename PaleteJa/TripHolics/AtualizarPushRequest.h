//
//  AtualizarPushRequest.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 01/09/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ModelBase.h"

@interface AtualizarPushRequest : ModelBase
@property(nonatomic,strong) NSString *TokenIos;
@property(nonatomic,strong) NSNumber *IdPerfil;


@end
