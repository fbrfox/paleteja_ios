//
//  HomeViewController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 25/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "HomeViewController.h"
#import <LGPlusButtonsView.h>
#import "AcompanhanteHelper.h"
#import "MotivoHelper.h"
#import <HSDatePickerViewController.h>
#import "MenuViewController.h"
#import "NovaTripViewController.h"
#import "BaseController.h"
#import "ExcluirTripButton.h"
#import "PerfilViewController.h"


@interface HomeViewController ()<LGPlusButtonsViewDelegate,UITableViewDelegate,UITableViewDataSource,TextFieldPickerHelperDelegate,HSDatePickerViewControllerDelegate,GMSAutocompleteViewControllerDelegate,MenuViewControllerDelegate,TripControllerDelegate,MinhasTripsControllerDelegate,TripstersControllerDelegate>
{
    __weak IBOutlet UIView *viewNovaViagem;
    __weak IBOutlet UIView *viewProxima;
    __weak IBOutlet GADBannerView *bannerBottom;
    __weak IBOutlet UITableView *tbTripsters;
    __weak IBOutlet UILabel *lbProximaTrip;
    __weak IBOutlet UILabel *lbQuando;
    __weak IBOutlet UITextField *tfLocal;
    __weak IBOutlet UITextField *tfIda;
    __weak IBOutlet UITextField *tfVolta;
    __weak IBOutlet TextFieldPickerHelper *tfMotivo;
    __weak IBOutlet TextFieldPickerHelper *tfViajando;
    
    NSArray *tripsters;
    
    NSArray*acompanhantes;
    NSArray*motivos;
    
    MotivoHelper* motivoSelecionado;
    AcompanhanteHelper *acompanhanteSelecionado;
    
    Viagem* proximaViagem;
    
    BOOL ida;
    
    
    TripController *tripController;
    MinhasTripsController *minhasTripsController;
    TripstersController *tripstersController;
    
    GMSPlace *localSelecionado;
    
}
@end

@implementation HomeViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    [self configuraBanner];
    [super configdismissOnTap:@[tfLocal,tfIda,tfVolta,tfMotivo,tfViajando]];
    
    tbTripsters.tableFooterView = [UIView new];
    
    
    tripController = [[TripController alloc]initWithViewController:self delegate:self];
    minhasTripsController =[[MinhasTripsController alloc]initWithViewController:self delegate:self];
    tripstersController = [[TripstersController alloc] initWithController:self delegate:self];
    
    acompanhantes = [AcompanhanteHelper retornaAcompanhantes];
    motivos = [MotivoHelper retornaMotivos];
    
    tfViajando.delegate = tfMotivo.delegate = self;
    
    [UIHelper addFloatButtonInView:self.view delegate:self];
    
    
    if([SharedPreferences getUsuarioNovo])
    {
        [self.view bringSubviewToFront:viewNovaViagem];
    }
    else
    {
        [self.view bringSubviewToFront:viewProxima];
        [self configuraProximaViagem];
    }
    
    
}



-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    

    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configuraProximaViagem{
    
    [minhasTripsController obterProximaTrip];
}


- (void)configuraBanner {
    // Do any additional setup after loading the view.
    
    GADRequest *request = [GADRequest request];
//    request.testDevices = @[ @"05fe66a3438576af3483b922c5ab5a7d" ];
    
    
    bannerBottom.adUnitID = NSLocalizedString(@"ADMobKeyBanner", nil);
    bannerBottom.rootViewController = self;
    [bannerBottom loadRequest:request];
}


-(void)plusButtonsView:(LGPlusButtonsView *)plusButtonsView buttonPressedWithTitle:(NSString *)title description:(NSString *)description index:(NSUInteger)index{
    
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NovaTripViewController"];
    [self.navigationController pushViewController:vc animated:YES];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return tripsters.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellTripster" forIndexPath:indexPath];
    
    Usuario *usuario = tripsters[indexPath.row];
    
    
    UIImageView *imageview = [cell viewWithTag:100];
    
    UILabel* lbNome = [cell viewWithTag:101];
    UILabel *lbEndereco =[cell viewWithTag:102];
    UILabel *lbAcompanhante = [cell viewWithTag:103];
    UILabel *lbSexo = [cell viewWithTag:110];
    
    
    ExcluirTripButton *btAdd = [cell viewWithTag:104];
    
    if(![usuario.Match boolValue])
    {
        btAdd.enabled = YES;
        btAdd.alpha = 1;
    }
    
    btAdd.usuario = usuario;
    btAdd.viagem = proximaViagem;
    
    [btAdd addTarget:self action:@selector(btAddClick:) forControlEvents:UIControlEventTouchUpInside];
    
    lbAcompanhante.alpha = 1;
    lbAcompanhante.text = usuario.Acompanhante;
    
    
    lbNome.text = usuario.Nome;
    lbEndereco.text = usuario.Localidade;
    [imageview sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:UrlFormatPerfilUsuario,usuario.Id]]];
    imageview.clipsToBounds = YES;
    
    lbSexo.text = usuario.Sexo;
    
    return cell;

}

-(void)btAddClick:(ExcluirTripButton *)button{
    
    [tripstersController adicionarMatchIdUsuario:button.usuario.Id viagemId:button.viagem.ViagemID];
    
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView withTextField:(TextFieldPickerHelper *)sender{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender{
    
    if([sender isEqual:tfMotivo])
        return motivos.count;
    else
        return acompanhantes.count;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender{
    
    if([sender isEqual:tfMotivo]){
        
        MotivoHelper *motivo = motivos[row];
        return motivo.descricao;
    }
    else{
        
        AcompanhanteHelper *acompanhante = acompanhantes[row];
        return acompanhante.descricao;
    }
}

- (void)doneClicked:(TextFieldPickerHelper *)sender withIndex:(NSInteger)index{

    if([sender isEqual:tfMotivo]){
        
        motivoSelecionado = motivos[index];
        tfMotivo.text = motivoSelecionado.descricao;
        
        
    }
    else{
        AcompanhanteHelper *acompanhante = acompanhantes[index];
        tfViajando.text = acompanhante.descricao;
        acompanhanteSelecionado = acompanhante;
    }
    
    [super dismissKeyboard];
    
}




-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if([textField isEqual:tfIda])
    {
        ida = YES;
        [self showDatePicker];
        return NO;
    }
    else if([textField isEqual:tfVolta]){
        ida = NO;
        [self showDatePicker];
        return NO;
    }else if([textField isEqual:tfLocal]){
        
        [self showPlacesAutoComplete];
        return NO;
    }
    
    return YES;
    
}


- (void)showDatePicker {
    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    hsdpvc.delegate = self;
    hsdpvc.minDate = [NSDate date];
    hsdpvc.backButtonTitle  = @"Cancelar";
    hsdpvc.confirmButtonTitle =@"Selecionar";
    
    [self presentViewController:hsdpvc animated:YES completion:nil];
}

- (void)showPlacesAutoComplete {
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
    filter.type = kGMSPlacesAutocompleteTypeFilterCity;
    acController.autocompleteFilter = filter;
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
}

-(void)hsDatePickerPickedDate:(NSDate *)date{
    
    if(ida)
        tfIda.text = [UIHelper dataParaString:date formato:@"dd/MM/yyyy"];
    else
        tfVolta.text = [UIHelper dataParaString:date formato:@"dd/MM/yyyy"];
    
}


-(void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place{
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    tfLocal.text = place.name;
    
    localSelecionado = place;
    
    
    
}

-(void)viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

-(void)wasCancelled:(GMSAutocompleteViewController *)viewController{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

-(void)viewWillDisappear:(BOOL)animated{
    
    
}
- (IBAction)onMenuTap:(id)sender {
    
    MenuViewController *menuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
    menuVC.delegate = self;
    
    [self presentViewController:menuVC animated:YES completion:nil];
}


-(void)menuMinhasTripsClicked{
    
    [self dismissViewControllerAnimated:YES completion:^{
       
        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MinhasTripsViewController"] animated:YES];
        
    }];
}

-(void)menuMeusTripstersClicked{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MeusTripstersViewController"] animated:YES];
        
    }];
}

-(void)meuPerfilClicked{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MeuPerfilViewController"] animated:YES];
        
    }];
}


-(void)proximaTripLoadSuccess:(Viagem *)viagem usuarios:(NSArray<Usuario> *)usuarios{
    
    
    proximaViagem = viagem;
    lbProximaTrip.text = viagem.Destino;
    lbQuando.text = [NSString stringWithFormat:@"%@ - %@",viagem.DataInicio,viagem.DataFim];
    tripsters = usuarios;
    [tbTripsters reloadData];
    
    
}

-(void)minhasTripsLoadError:(NSString *)msg{
    
    [self.view bringSubviewToFront:viewNovaViagem];
    
}

-(void)tripRegisteredSuccess:(NSArray *)usuarios msg:(NSString *)msg{
 
    [self.view bringSubviewToFront:viewProxima];
    [UIHelper mostrarAlertaSucesso:msg viewController:self];
    [self configuraProximaViagem];
    [SharedPreferences setUsuarioNovo:@NO];
}


-(void)erroOnRegisterTrip:(NSString *)msg{
    
    [UIHelper mostrarAlertaErro:msg viewController:self];
}

-(void)matchAdicionadoComSucesso:(NSString *)msg{
    
    [UIHelper mostrarAlertaSucesso:msg viewController:self];
}

-(void)loadTripstersErro:(NSString *)error{
    
    [UIHelper mostrarAlertaErro:error viewController:self];
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    Usuario*usuario = tripsters[indexPath.row];
    
    
    
    PerfilViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PerfilViewController"];
    vc.usuario = usuario;
    vc.viagem = proximaViagem;
    
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (IBAction)btCadastrarViagemTap:(id)sender {
    
    
    CadastrarViagemRequest *request =[CadastrarViagemRequest new];
    
    
    NSArray *endereco = [localSelecionado.formattedAddress componentsSeparatedByString:@","];
    
    request.DataInicio = tfIda.text;
    request.DataFim = tfVolta.text;
    request.Acompanhante = tfViajando.text;
    
    if(endereco.count > 2)
    {
        request.CidadeDestino = [[endereco[0] stringByReplacingOccurrencesOfString:@"," withString:@""] stringByTrimmingCharactersInSet:
                                 [NSCharacterSet whitespaceCharacterSet]];
        request.EstadoDestino = [[endereco[1] stringByReplacingOccurrencesOfString:@"," withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        request.PaisDestino = [[endereco[2] stringByReplacingOccurrencesOfString:@"," withString:@""] stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    }
    else{
        
        request.CidadeDestino = [[endereco[0] stringByReplacingOccurrencesOfString:@"," withString:@""] stringByTrimmingCharactersInSet:
                                 [NSCharacterSet whitespaceCharacterSet]];
        request.EstadoDestino = [[endereco[0] stringByReplacingOccurrencesOfString:@"," withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        request.PaisDestino = [[endereco[1] stringByReplacingOccurrencesOfString:@"," withString:@""] stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    }
    
    request.LatitudeDestino = @(localSelecionado.coordinate.latitude);
    request.LongitudeDestino = @(localSelecionado.coordinate.longitude);
    
    request.MotivoViagemID = @(motivoSelecionado.idMotivo);
    request.PerfilID = IdUsuarioLogado;
    
    
    [tripController cadastrarTrip:request];

    
}

@end
