//
//  MinhasTripsController.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 27/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ResponseBase.h"
#import "Viagem.h"


@protocol MinhasTripsControllerDelegate <NSObject>

-(void)minhasTripsLoadSuccess:(NSArray<Viagem> *)trips msg:(NSString *)msg;
-(void)minhasTripsLoadError:(NSString *)msg;
-(void)tripDeletada:(NSString *)msg;

-(void)proximaTripLoadSuccess:(Viagem *)viagem usuarios:(NSArray<Usuario>*)usuarios;


@end

@interface MinhasTripsController : ResponseBase

@property(nonatomic,weak) id<MinhasTripsControllerDelegate> delegate;


@property(nonatomic,strong) UIViewController *viewController;


-(id)initWithViewController:(UIViewController *)vc delegate:(id<MinhasTripsControllerDelegate>)delegate;

-(void)loadMinhasTrips;

-(void)deletarTrip:(NSNumber *)idViagem;

-(void)obterProximaTrip;
@end
