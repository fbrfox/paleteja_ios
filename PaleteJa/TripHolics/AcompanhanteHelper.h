//
//  AcompanhanteHelper.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 25/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AcompanhanteHelper : NSObject

@property(nonatomic,strong) NSString *descricao;

+(NSArray *)retornaAcompanhantes;

@end
