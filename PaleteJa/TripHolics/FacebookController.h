//
//  FacebookController.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FacebookUser.h"


@protocol FacebookControlerDelegate <NSObject>

@optional

-(void)facebookLoginCanceled;

-(void)facebookLoginSuccess:(FacebookUser *)facebookUser;

@end

@interface FacebookController : NSObject


@property(nonatomic,weak) id<FacebookControlerDelegate> delegate;

@property(nonatomic,strong) UIView *view;

-(id)initWithView:(UIView *)view delegate:(id<FacebookControlerDelegate>)delegate;

-(void)loginWithFacebook:(UIViewController *)viewController;

@end
