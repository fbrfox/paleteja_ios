//
//  SharedPreferences.m
//  Movida Condominio
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/04/17.
//  Copyright © 2017 Movida Locação de Veículos Ltda. All rights reserved.
//

#import "SharedPreferences.h"

@implementation SharedPreferences


+(void)setUserToken:(NSString *)token{
    
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"KeyUserToken"];
}

+(void)setUserId:(NSNumber *)userId{
    
    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"KeyUserID"];
    
}

+(void)setUserName:(NSString *)name{
    
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"Nome"];
    
}

+(void)setUserLocal:(NSString *)local{
    
    [[NSUserDefaults standardUserDefaults] setObject:local forKey:@"Local"];
    
}

+(NSString *)userLocal{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"Local"];
}

+(NSString *)userName{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"Nome"];
}

+(NSString *)userToken{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"KeyUserToken"];
}

+(NSNumber *)userId{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"KeyUserID"];
}


+(void)setUsuarioNovo:(NSNumber *)usuarioNovo{
    
    [[NSUserDefaults standardUserDefaults] setObject:usuarioNovo forKey:@"UsuarioNovo"];
    
}

+(BOOL)getUsuarioNovo{
    
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"UsuarioNovo"] boolValue];
}


+(void)setDeviceToken:(NSString *)token{
    
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"DeviceToken"];
    
}

+(NSString *)deviceToken{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"];
}

@end
