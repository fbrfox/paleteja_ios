//
// Created by pedro henrique borges on 4/27/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import <Foundation/Foundation.h>



typedef NS_ENUM(NSInteger, MaskType) {
    Telefone = 1,
    CPF = 2,
    CEP = 3,
    Celular = 4,
};


typedef NS_ENUM(NSInteger, TipoConsultaTripAdvisor) {
    Hoteis = 1,
    Restaurantes = 2,
    Atracoes = 3,
};




@interface EnumHelper : NSObject
@end
