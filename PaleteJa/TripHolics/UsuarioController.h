//
//  UsuarioController.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 27/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CadastroPerfilRequest.h"

@protocol UsuarioControllerDelegate <NSObject>

@optional
-(void)loginSucesso;
-(void)loginError:(NSString *)error;
-(void)toCadastroEndereco:(CadastroPerfilRequest *)request;
-(void)toCadastroTelefone:(CadastroPerfilRequest *)request;
-(void)cadastroEfetuadoComSucesso:(NSString *)msg;
-(void)erroAoCadastrar:(NSString *)error;
-(void)obterPerfilSuccess:(ObterPerfilResponse *)perfil;
-(void)perfilAtualizado:(NSString *)msg;

@end

@interface UsuarioController : NSObject


@property(nonatomic,strong) UIViewController *viewController;

@property(nonatomic,weak) id<UsuarioControllerDelegate> delegate;

-(id)initWithController:(UIViewController *)vc delegate:(id<UsuarioControllerDelegate>)delegate;


-(void)loginEmail:(NSString *)email senha:(NSString *)senha;

-(void)validateCadastroUsuario:(CadastroPerfilRequest *)request;

-(void)validateCadastroEndereco:(CadastroPerfilRequest *)request;

-(void)validateCadastroTelefone:(CadastroPerfilRequest *)request;

-(void)obterPerfil;

-(void)atualizarPerfil:(CadastroPerfilRequest *)request;

@end
