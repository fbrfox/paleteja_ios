//
//  UsuarioController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 27/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "UsuarioController.h"
#import "ValidacaoHelper.h"

@implementation UsuarioController

-(id)initWithController:(UIViewController *)vc delegate:(id<UsuarioControllerDelegate>)delegate{
    
    self = [super init];
    
    if(self) {
        
        self.viewController = vc;
        self.delegate = delegate;
    }
    
    return self;
}


-(void)obterPerfil{
    
    
    [UIHelper showProgress];
    
    [[APIClient sharedManager]obterPerfilCompletion:^(ObterPerfilResponse *response, NSString *errorMessage) {
       
        [UIHelper hideProgress];
        
        if(errorMessage || !response.Sucesso)
            [_delegate erroAoCadastrar:response.Mensagem ? response.Mensagem : errorMessage];
        else
            [_delegate obterPerfilSuccess:response];
        
    }];
    
}


-(void)loginEmail:(NSString *)email senha:(NSString *)senha{
    
    if(![self validaCampos:email senha:senha])
        return;
    
    
    [UIHelper showProgress];
    
    [[APIClient sharedManager]login:[[LoginRequest alloc] initWithEmail:email senha:senha] completion:^(CadastroPerfilResponse *response, NSString *errorMessage) {
      
        [UIHelper hideProgress];
        
        if(errorMessage || !response.Sucesso)
            [_delegate loginError:response.Mensagem ? response.Mensagem : errorMessage];
        else
            [_delegate loginSucesso];

    }];
    
    
}





-(BOOL)validaCampos:(NSString *)email senha:(NSString *)senha{
    
    NSMutableString *retorno = [NSMutableString new];
    
    
    if(![[ValidacaoHelper sharedInstance] validarEmail:email])
    {
        [retorno appendString:@"O campo e-mail é obrigatório!\n"];
    }
    
    if(![[ValidacaoHelper sharedInstance]validarString:senha]){
        [retorno appendString:@"O campo senha é obrigatório!"];
    }
    
    
    if([retorno isEqualToString:@""])
        return YES;
    else{
        
        [UIHelper mostrarAlertaErro:retorno viewController:self.viewController];
        return NO;
    }
    
}

-(void)validateCadastroUsuario:(CadastroPerfilRequest *)request{
    
    
    NSMutableString *retorno = [NSMutableString new];
    
    if(![[ValidacaoHelper sharedInstance] validarString:request.Nome])
    {
        [retorno appendString:@"O campo Nome é obrigatório!\n"];
    }

    
    if(![[ValidacaoHelper sharedInstance] validarEmail:request.Email])
    {
        [retorno appendString:@"O campo e-mail é obrigatório!\n"];
    }
    
    if(![[ValidacaoHelper sharedInstance]validarString:request.Senha]){
        [retorno appendString:@"O campo senha é obrigatório!"];
    }
    
  
    
    if([retorno isEqualToString:@""])
        [_delegate toCadastroEndereco:request];
    else{
        
        [UIHelper mostrarAlertaErro:retorno viewController:self.viewController];
    }

}

-(void)validateCadastroEndereco:(CadastroPerfilRequest *)request{
    
    
    NSMutableString *retorno = [NSMutableString new];
 
    
    if(![[ValidacaoHelper sharedInstance] validarString:request.Cidade])
    {
        [retorno appendString:@"O campo cidade é obrigatório!\n"];
    }
    
    if(![[ValidacaoHelper sharedInstance]validarString:request.Pais]){
        [retorno appendString:@"O campo pais é obrigatório!"];
    }
    
    
    if([retorno isEqualToString:@""])
        [_delegate toCadastroTelefone:request];
    else{
        
        [UIHelper mostrarAlertaErro:retorno viewController:self.viewController];
    }

    
}


-(void)validateCadastroTelefone:(CadastroPerfilRequest *)request{
    
    
    NSMutableString *retorno = [NSMutableString new];
    
    
    if(![[ValidacaoHelper sharedInstance] validarString:request.TelefoneNumero])
    {
        request.TelefoneNumero = @"0";
    }
    
    if(![[ValidacaoHelper sharedInstance]validarString:request.Foto]){
        [retorno appendString:@"É necessário tirar ou selecionar uma foto do perfil!"];
    }
    
    
    if([retorno isEqualToString:@""])
        [self efetuarCadastro:request];
    else{
        
        [UIHelper mostrarAlertaErro:retorno viewController:self.viewController];
    }
    
    
}

-(void)efetuarCadastro:(CadastroPerfilRequest *)request{
    
    
    [UIHelper showProgress];
    
    [[APIClient sharedManager] cadastraPerfil:request completion:^(CadastroPerfilResponse *response, NSString *errorMessage) {
    
        [UIHelper hideProgress];
        
        if(errorMessage || !response.Sucesso)
            [_delegate erroAoCadastrar:response.Mensagem ? response.Mensagem : errorMessage];
        else
            [_delegate cadastroEfetuadoComSucesso:response.Mensagem];
        

    }];
    
    
}



-(void)atualizarPerfil:(CadastroPerfilRequest *)request{
    
    
    NSMutableString *retorno = [NSMutableString new];
    
    
    if(![[ValidacaoHelper sharedInstance] validarString:request.TelefoneNumero])
    {
        [retorno appendString:@"O campo Telefone é obrigatório!\n"];
    }
    
    
    if([retorno isEqualToString:@""])
        [self efetuarAtualizacao:request];
    else{
        
        [UIHelper mostrarAlertaErro:retorno viewController:self.viewController];
    }
    
}

-(void)efetuarAtualizacao:(CadastroPerfilRequest *)request{
    
    
    [UIHelper showProgress];
    
    [[APIClient sharedManager] atualizarPerfil:request completion:^(ResponseBase *response, NSString *errorMessage) {
       
        [UIHelper hideProgress];
        
        if(errorMessage || !response.Sucesso)
            [_delegate erroAoCadastrar:response.Mensagem ? response.Mensagem : errorMessage];
        else
            [_delegate perfilAtualizado:response.Mensagem];
        
    }];
}



@end
