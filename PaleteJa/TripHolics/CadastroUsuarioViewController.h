//
//  CadastroUsuarioViewController.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 24/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacebookUser.h"

@interface CadastroUsuarioViewController : BaseViewController

@property(nonatomic,strong) FacebookUser* usuarioFacebook;


@end
