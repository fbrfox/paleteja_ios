//
//  PerfilViewController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "PerfilViewController.h"
#import "TripstersController.h"

@interface PerfilViewController ()<TripstersControllerDelegate>
{
    
    __weak IBOutlet UIImageView *ivPerfil;
    __weak IBOutlet UILabel *lbNome;
    __weak IBOutlet UILabel *lbLocal;
    __weak IBOutlet UIButton *btAdicionar;
    
    __weak IBOutlet UIButton *btEmail;
    __weak IBOutlet UIButton *btPhone;
    __weak IBOutlet UILabel *lbSobreDesc;
    __weak IBOutlet UILabel *lbSobre;

    TripstersController *controller;

}
@end

@implementation PerfilViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    controller = [[TripstersController alloc]initWithController:self delegate:self];
    
    NSString *telefone = [NSString stringWithFormat:@"+%@",[_usuario.Telefone stringByReplacingOccurrencesOfString:@"+" withString:@""]];
    
    lbNome.text = _usuario.Nome;
    lbLocal.text = _usuario.Localidade;
    [btEmail setTitle:_usuario.Email forState:UIControlStateNormal];
    [btPhone setTitle:telefone forState:UIControlStateNormal];
    [ivPerfil sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:UrlFormatPerfilUsuario,_usuario.Id]]];
    ivPerfil.clipsToBounds = YES;
    
    if(!_viagem)
    {
        btAdicionar.enabled = NO;
        btAdicionar.alpha = 0;
    }
    
    lbSobreDesc.text = [NSString stringWithFormat:@"Sobre %@",_usuario.Nome];
    lbSobre.text = _usuario.Sobre;
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];

    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onEmailTap:(id)sender {
    
    [UIHelper sendMail:btEmail.titleLabel.text];
}
- (IBAction)onTelefoneTap:(id)sender {
    
    [UIHelper callNumber:btPhone.titleLabel.text];
}
- (IBAction)onAdicionarTap:(id)sender {
    
    [controller adicionarMatchIdUsuario:_usuario.Id viagemId:_viagem.ViagemID];
}

-(void)matchAdicionadoComSucesso:(NSString *)msg{
    
    [UIHelper mostrarAlertaSucesso:msg viewController:self];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    
}
- (IBAction)onDenunciarTap:(id)sender {
    
    
    [controller denunciarPerfil:_usuario.Id];
    
    
}


-(void)perfilDenunciadoSuccess:(NSString *)msg{
    
    [UIHelper mostrarAlertaSucesso:msg viewController:self];
    
}

-(void)loadTripstersErro:(NSString *)error{
    
    [UIHelper mostrarAlertaErro:error viewController:self];
}




@end
