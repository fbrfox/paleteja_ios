//
//  MenuViewController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController ()
{
    
    __weak IBOutlet UIImageView *ivPerfil;
    __weak IBOutlet UILabel *lbNome;
    __weak IBOutlet UILabel *lbLocal;
}
@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [ivPerfil sd_setImageWithURL:ImagemPerfil];
    ivPerfil.clipsToBounds = YES;
    
    lbNome.text = [SharedPreferences userName];
    lbLocal.text = [SharedPreferences userLocal];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onMinhasTripTap:(id)sender {
    
    
    [self.delegate menuMinhasTripsClicked];
    
}
- (IBAction)onMeuPerfilTaop:(id)sender {
    [self.delegate meuPerfilClicked];
    
}
- (IBAction)onMeusTripstersTap:(id)sender {
    
    [self.delegate menuMeusTripstersClicked];
}
- (IBAction)onSairTap:(id)sender {
    
    [AutenticacaoHelper logout];
    
    if([FBSDKAccessToken currentAccessToken])
    {
        [[FBSDKLoginManager  alloc ]logOut];
    }
    
    [Delegate changeRootViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateInitialViewController]];
    
    
}
- (IBAction)onCloseMenu:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
