//
//  AcompanhanteHelper.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 25/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "AcompanhanteHelper.h"

@implementation AcompanhanteHelper


+(NSArray *)retornaAcompanhantes{
    
    NSMutableArray *acompanhantes = [NSMutableArray new];
    
    AcompanhanteHelper *sozinho = [AcompanhanteHelper new];
    sozinho.descricao = @"Sozinho(a)";
    
    AcompanhanteHelper *casal = [AcompanhanteHelper new];
    casal.descricao = @"Em Casal";
    
    AcompanhanteHelper *grupo = [AcompanhanteHelper new];
    grupo.descricao = @"Grupo de duas ou mais pessoas";
    
    
    [acompanhantes addObject:sozinho];
    [acompanhantes addObject:casal];
    [acompanhantes addObject:grupo];
    
    return acompanhantes;
}

@end
