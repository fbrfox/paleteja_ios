//
//  PaisHelper.h
//  Movida
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 24/07/17.
//  Copyright © 2017 Movida. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaisHelper : NSObject

@property(nonatomic,strong) NSString *descricao;
@property(nonatomic,strong) NSString *codigo;


+(NSArray *)retornaPaises;

@end
