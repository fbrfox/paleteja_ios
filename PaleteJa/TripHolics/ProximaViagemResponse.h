//
//  ProximaViagemResponse.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 27/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ResponseBase.h"
#import "Usuario.h"
#import "Viagem.h"

@interface ProximaViagemResponse : ResponseBase

@property(nonatomic,strong) Viagem* Viagem;
@property(nonatomic,strong) NSArray<Usuario> *Usuarios;

@end
