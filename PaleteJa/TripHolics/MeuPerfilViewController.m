//
//  MeuPerfilViewController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "MeuPerfilViewController.h"
#import "SexoHelper.h"

@interface MeuPerfilViewController ()<TextFieldPickerHelperDelegate,UsuarioControllerDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    
    __weak IBOutlet UIImageView *ivPerfil;
    __weak IBOutlet UILabel *lbNome;
    __weak IBOutlet UILabel *lbLocal;
    __weak IBOutlet JVFloatLabeledTextField *tfCidade;
    __weak IBOutlet JVFloatLabeledTextField *tfPais;
    __weak IBOutlet JVFloatLabeledTextField *tfTelefone;
    __weak IBOutlet TextFieldPickerHelper *tfSexo;
    __weak IBOutlet JVFloatLabeledTextField *tfSobre;
    
    NSArray*sexos;
    
    
    UsuarioController *usuarioController;
    Boolean fotoNova;
    NSString *imageSelected;
    
}
@end

@implementation MeuPerfilViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    sexos = [SexoHelper retornaSexos];
    
    [super configdismissOnTap:@[tfTelefone,tfSexo,tfSobre]];
    
    usuarioController = [[UsuarioController alloc] initWithController:self delegate:self];
    
    [usuarioController obterPerfil];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onSalvarClick:(id)sender {
    
    CadastroPerfilRequest *request = [CadastroPerfilRequest new];
    request.Sexo = tfSexo.text;
    request.Cidade = tfCidade.text;
    request.TelefonePais = tfPais.text;
    request.TelefoneNumero = tfTelefone.text;
    request.PerfilID = IdUsuarioLogado;
    request.Sobre = tfSobre.text;
    
    if(fotoNova)
        request.Foto = imageSelected;
    
    request.FotoNova = @(fotoNova);
    
    [usuarioController atualizarPerfil:request];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView withTextField:(TextFieldPickerHelper *)sender{
 
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender{
    
    return sexos.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender{
    
    SexoHelper *sexo = sexos[row];
    return sexo.descricao;
    
}

- (void)doneClicked:(TextFieldPickerHelper *)sender withIndex:(NSInteger)index{
    
    [super dismissKeyboard];
    SexoHelper *sexo = sexos[index];
    tfSexo.text = sexo.descricao;
    
    
}

-(void)perfilAtualizado:(NSString *)msg{
    
    [UIHelper mostrarAlertaSucesso:msg viewController:self];
}

-(void)obterPerfilSuccess:(ObterPerfilResponse *)perfil{
    
    lbNome.text = perfil.Nome;
    lbLocal.text = perfil.Cidade;
    tfCidade.text = perfil.Cidade;
    tfTelefone.text = perfil.TelefoneNumero;
    tfPais.text = perfil.TelefonePais;
    tfSexo.text = perfil.Sexo;
    tfSobre.text = perfil.Sobre;
    
    [ivPerfil sd_setImageWithURL:ImagemPerfil];
    ivPerfil.clipsToBounds = YES;
    
}


-(void)erroAoCadastrar:(NSString *)error{
    
    [UIHelper mostrarAlertaErro:error viewController:self];
}


- (IBAction)onAlterarImageTap:(id)sender {
    UIAlertController *alertaPhotoController = [UIAlertController alertControllerWithTitle:@"Selecione uma opção" message:@"Selecione se deseja capturar o selecionar uma foto da galeria" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [alertaPhotoController addAction:[UIAlertAction actionWithTitle:@"Galeria" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self showGaleria];
        
    }]];
    
    
    [alertaPhotoController addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self showCamera];
        
    }]];
    
    
    [alertaPhotoController addAction:[UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    [self presentViewController:alertaPhotoController animated:YES completion:nil];
    
}


-(void)showGaleria{
    
    [self showDevicePhoto:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    
}

-(void)showCamera{
    
    [self showDevicePhoto:UIImagePickerControllerSourceTypeCamera];
}


-(void)showDevicePhoto:(UIImagePickerControllerSourceType)type{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = type;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:Nil];
    
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *, id> *)info {
    
    fotoNova = YES;
    
    UIImage *pickedImage = info[UIImagePickerControllerOriginalImage];
    imageSelected = [UIHelper imageToNSString:pickedImage];
    [picker dismissViewControllerAnimated:NO completion:nil];
    [UIHelper mostrarAlerta:@"Foto capturada com sucesso!" viewController:self];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    
}

@end
