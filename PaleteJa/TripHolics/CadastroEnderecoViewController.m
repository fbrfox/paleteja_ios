//
//  CadastroEnderecoViewController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 24/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "CadastroEnderecoViewController.h"
#import "PaisHelper.h"
#import "CadastroTelefoneViewController.h"

@interface CadastroEnderecoViewController () <GMSAutocompleteViewControllerDelegate,TextFieldPickerHelperDelegate,UsuarioControllerDelegate>
{
    __weak IBOutlet UITextField *tfCidade;
    __weak IBOutlet TextFieldPickerHelper *tfPais;
    
    GMSPlace *localSelecionado;
    NSArray*paises;
    UsuarioController *usuarioController;
    
    PaisHelper *paisSelecionado;
    
}
@end

@implementation CadastroEnderecoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super configdismissOnTap:@[tfCidade,tfPais]];
    tfPais.delegate = self;
    paises = [PaisHelper retornaPaises];
    usuarioController = [[UsuarioController alloc]initWithController:self delegate:self];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



-(void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place{
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    tfCidade.text = place.name;
    localSelecionado = place;
    
    
    
}

-(void)viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error{
    [self dismissViewControllerAnimated:YES completion:nil];

    
}

-(void)wasCancelled:(GMSAutocompleteViewController *)viewController{
    
      [self dismissViewControllerAnimated:YES completion:nil];
}


// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if([textField isEqual:tfCidade]){
        
        GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
        
        GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
        filter.type = kGMSPlacesAutocompleteTypeFilterCity;
        
        acController.autocompleteFilter = filter;
        acController.delegate = self;
        [self presentViewController:acController animated:YES completion:nil];
        return NO;
        
    }
    
    return YES;
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView withTextField:(TextFieldPickerHelper *)sender{
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender{
    
    return paises.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender{
    
    
    PaisHelper *pais = paises[row];
    
    return pais.descricao;
    
}

- (void)doneClicked:(TextFieldPickerHelper *)sender withIndex:(NSInteger)index{

    paisSelecionado = paises[index];

    sender.text = paisSelecionado.descricao;
    
    [sender resignFirstResponder];
    
}


- (IBAction)onProximoTap:(id)sender {
    
 
    _cadastroPerfilRequest.Cidade = tfCidade.text;
    _cadastroPerfilRequest.Pais =paisSelecionado.descricao;
    _cadastroPerfilRequest.TelefonePais = paisSelecionado.codigo;
    
    [usuarioController validateCadastroEndereco:_cadastroPerfilRequest];

}

-(void)toCadastroTelefone:(CadastroPerfilRequest *)request{
    
    CadastroTelefoneViewController *tempVc=  [self.storyboard instantiateViewControllerWithIdentifier:@"CadastroTelefoneViewController"];
    tempVc.request = request;
    
    [self.navigationController pushViewController:tempVc animated:YES];
    
}





@end
