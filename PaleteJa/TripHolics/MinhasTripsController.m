//
//  MinhasTripsController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 27/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "MinhasTripsController.h"

@implementation MinhasTripsController


-(id)initWithViewController:(UIViewController *)vc delegate:(id<MinhasTripsControllerDelegate>)delegate{
    
    
    self = [super init];
    
    if(self){
        
        self.viewController = vc;
        self.delegate = delegate;
    }
    
    return self;
}


-(void)deletarTrip:(NSNumber *)idViagem{
    
    [UIHelper showProgress];
    
    [[APIClient sharedManager]deletarViagens:idViagem completion:^(ResponseBase *response, NSString *errorMessage) {
        
        [UIHelper hideProgress];
        
        if(errorMessage || !response.Sucesso)
            [_delegate minhasTripsLoadError:errorMessage];
        else
            [_delegate tripDeletada:response.Mensagem];
        
    }];
}


-(void)loadMinhasTrips{
    
    [UIHelper showProgress];
    
    [[APIClient sharedManager]minhasViagens:^(MinhasVIagensResponse *response, NSString *errorMessage) {
        
        [UIHelper hideProgress];
        
        if(errorMessage)
            [self.delegate minhasTripsLoadError:errorMessage];
        else
            [self.delegate minhasTripsLoadSuccess:response.Viagens msg:response.Mensagem];
            
    }];
    
}

-(void)obterProximaTrip{
    
    [UIHelper showProgress];
    
    [[APIClient sharedManager]obterProximaViagemCompletion:^(ProximaViagemResponse *response, NSString *errorMessage) {
        
        [UIHelper hideProgress];
        
        if(errorMessage || !response.Sucesso)
            [_delegate minhasTripsLoadError:errorMessage];
        else
            [_delegate proximaTripLoadSuccess:response.Viagem usuarios:response.Usuarios];

            
    }];
    
}

@end
