//
//  CadastroTelefoneViewController.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 24/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseController.h"

@interface CadastroTelefoneViewController : BaseViewController

@property(nonatomic,strong) CadastroPerfilRequest* request;

@end
