//
//  BaseController.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FacebookController.h"
#import "TripController.h"
#import "MinhasTripsController.h"
#import "TripstersController.h"
#import "UsuarioController.h"
@interface BaseController : NSObject

@end
