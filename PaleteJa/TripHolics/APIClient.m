//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import "APIClient.h"
#import "JSONResponseSerializerWithData.h"
#import "TokenHelper.h"
//EndPoints Urls


NSString *const APIBaseURL = @"http://www.trypholics.com/api/";

//NSString *const APIBaseURL = @"http://192.168.43.44/tripholics/api/";

//Producao
NSString *const APIAutenticacao = @"Perfil/Autenticar";
NSString *const APIViagemCadastro = @"Viagem/Cadastrar";
NSString *const APIMinhasViagens = @"Viagem/MinhasViagens?id_perfil=%@";
NSString *const APIDeletarViagens = @"Viagem/Deletar?id_viagem=%@";
NSString *const APITripstersViagem = @"Viagem/ListarPerfisViagem?id_viagem=%@&id_perfil=%@";
NSString *const APIMeusTripsters = @"Match/ObterMeusMatchs?idPerfil=%@";
NSString *const APIAdicionarMatch = @"Match/Adicionar";
NSString *const APIProximaViagem = @"Viagem/ProximaViagem?id_perfil=%@";
NSString *const APICadastrarPerfil = @"Perfil/Cadastrar";
NSString *const APIObterPerfil = @"Perfil/Obter?id_perfil=%@";
NSString *const APIAtualizarPerfil = @"Perfil/Atualizar";
NSString *const APIDenunciarPerfil = @"Perfil/Denunciar?id_perfil=%@";
NSString *const APIAtualizarPush = @"Perfil/AtualizarPush";


//Errors
int const kErrorUnauthorized = 401;


//Localized Strings
NSString *const serverDown = @"ServerDown";
NSString *const badCredentials = @"BadCredentials";

@implementation APIClient

#pragma mark Configurações Header



+ (APIClient *)sharedManager {
    static APIClient *_sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

        NSLog(APIBaseURL);
        
        _sharedManager = [[APIClient alloc] initWithBaseURL:[NSURL URLWithString:APIBaseURL] sessionConfiguration:configuration];
        [self configClient:_sharedManager];
    });

    return _sharedManager;
}

+ (void)configClient:(APIClient *)_sharedManager {
    [_sharedManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [_sharedManager setResponseSerializer:[JSONResponseSerializerWithData serializer]];
    [_sharedManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [_sharedManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
}

- (NSURLSessionDataTask *)denunciarPerfil:(NSNumber *)idPerfil completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion{
    
    
    NSString *url = [NSString stringWithFormat:APIDenunciarPerfil,idPerfil];
    
    NSURLSessionDataTask *task = [self PUTWithLogParams:url parameters:[NSDictionary new]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {
                                                    
                                                    NSError *error;
                                                    
                                                    ResponseBase *loginResponse = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];
                                                    
                                                    
                                                    if (error) {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    } else {
                                                        
                                                        completion(loginResponse, nil);
                                                    }
                                                    
                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                    
                                                    if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                        completion(nil, NSLocalizedString(badCredentials, nil));
                                                    } else {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    }
                                                }];
    
    return task;
}


- (NSURLSessionDataTask *)obterPerfilCompletion:(void (^)(ObterPerfilResponse *response, NSString *errorMessage))completion{
    
    
    NSString *url = [NSString stringWithFormat:APIObterPerfil,IdUsuarioLogado];
    
    NSURLSessionDataTask *task = [self GETWithLogParams:url parameters:[NSDictionary new]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {
                                                    
                                                    NSError *error;
                                                    
                                                    ObterPerfilResponse *loginResponse = [[ObterPerfilResponse alloc] initWithDictionary:responseObject error:&error];
                                                    
                                                    
                                                    if (error) {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    } else {
                                                        
                                                        completion(loginResponse, nil);
                                                    }
                                                    
                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                    
                                                    if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                        completion(nil, NSLocalizedString(badCredentials, nil));
                                                    } else {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    }
                                                }];
    
    return task;
}


- (NSURLSessionDataTask *)obterProximaViagemCompletion:(void (^)(ProximaViagemResponse *response, NSString *errorMessage))completion {
    
    
    NSString *url = [NSString stringWithFormat:APIProximaViagem,IdUsuarioLogado];
    
    NSURLSessionDataTask *task = [self GETWithLogParams:url parameters:[NSDictionary new]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {
                                                    
                                                    NSError *error;
                                                    
                                                    ProximaViagemResponse *loginResponse = [[ProximaViagemResponse alloc] initWithDictionary:responseObject error:&error];
                                                    
                                                    
                                                    if (error) {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    } else {
                                                        
                                                        completion(loginResponse, nil);
                                                    }
                                                    
                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                    
                                                    if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                        completion(nil, NSLocalizedString(badCredentials, nil));
                                                    } else {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    }
                                                }];
    
    return task;
}



- (NSURLSessionDataTask *)adicionarMatch:(AdicionarMatchRequest *)request completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion {
    
    
    NSDictionary *parametros = [request toDictionary];
    
    NSURLSessionDataTask *task = [self PUTWithLogParams:APIAdicionarMatch parameters:parametros
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {
                                                     
                                                     NSError *error;
                                                     
                                                     ResponseBase *loginResponse = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];
                                                     
                                                     
                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {
                                                         
                                                         completion(loginResponse, nil);
                                                     }
                                                     
                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                     
                                                     if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                         completion(nil, NSLocalizedString(badCredentials, nil));
                                                     } else {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     }
                                                 }];
    
    return task;
}


- (NSURLSessionDataTask *)cadastraPerfil:(CadastroPerfilRequest *)loginRequest completion:(void (^)(CadastroPerfilResponse *response, NSString *errorMessage))completion {
    
    
    NSDictionary *parametros = [loginRequest toDictionary];
    
    NSURLSessionDataTask *task = [self POSTWithLogParams:APICadastrarPerfil parameters:parametros
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {
                                                     
                                                     NSError *error;
                                                     
                                                     CadastroPerfilResponse *loginResponse = [[CadastroPerfilResponse alloc] initWithDictionary:responseObject error:&error];
                                                     
                                                     
                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {
                                                         
                                                         if(loginResponse.Sucesso)
                                                             [AutenticacaoHelper loginWithUsuario:loginResponse.Usuario token:loginResponse.Token];
                                                         
                                                         completion(loginResponse, nil);
                                                     }
                                                     
                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                     
                                                     if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                         completion(nil, NSLocalizedString(badCredentials, nil));
                                                     } else {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     }
                                                 }];
    
    return task;
}


- (NSURLSessionDataTask *)atualizarPerfil:(CadastroPerfilRequest *)loginRequest completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion {
    
    
    NSDictionary *parametros = [loginRequest toDictionary];
    
    NSURLSessionDataTask *task = [self POSTWithLogParams:APIAtualizarPerfil parameters:parametros
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {
                                                     
                                                     NSError *error;
                                                     
                                                     ResponseBase *loginResponse = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];
                                                     
                                                     
                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {
                                                         
                                                         completion(loginResponse, nil);
                                                     }
                                                     
                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                     
                                                     if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                         completion(nil, NSLocalizedString(badCredentials, nil));
                                                     } else {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     }
                                                 }];
    
    return task;
}

//
- (NSURLSessionDataTask *)login:(LoginRequest *)loginRequest completion:(void (^)(CadastroPerfilResponse *response, NSString *errorMessage))completion {
    
    
    NSDictionary *parametros = [loginRequest toDictionary];
    
    NSURLSessionDataTask *task = [self POSTWithLogParams:APIAutenticacao parameters:parametros
        success:^(NSURLSessionDataTask *task, id responseObject) {
                
            NSError *error;
                
            CadastroPerfilResponse *loginResponse = [[CadastroPerfilResponse alloc] initWithDictionary:responseObject error:&error];

        
            if (error) {
                completion(nil, NSLocalizedString(serverDown, nil));
            } else {
                
                if(loginResponse.Sucesso)
                    [AutenticacaoHelper loginWithUsuario:loginResponse.Usuario token:loginResponse.Token];
                
                completion(loginResponse, nil);
            }
                
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
                
            if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                completion(nil, NSLocalizedString(badCredentials, nil));
            } else {
                completion(nil, NSLocalizedString(serverDown, nil));
            }
        }];
    
    return task;
}


- (NSURLSessionDataTask *)cadastrarViagem:(CadastrarViagemRequest *)request completion:(void (^)(CadastrarViagemResponse *response, NSString *errorMessage))completion {
    
    
    NSDictionary *parametros = [request toDictionary];
    
    NSURLSessionDataTask *task = [self POSTWithLogParams:APIViagemCadastro parameters:parametros
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {
                                                     
                                                     NSError *error;
                                                     
                                                     CadastrarViagemResponse *loginResponse = [[CadastrarViagemResponse alloc] initWithDictionary:responseObject error:&error];
                                                     
                                                     
                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {
                                                         
                                                         completion(loginResponse, nil);
                                                     }
                                                     
                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                     
                                                     if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                         completion(nil, NSLocalizedString(badCredentials, nil));
                                                     } else {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     }
                                                 }];
    
    return task;
}



- (NSURLSessionDataTask *)minhasViagens:(void (^)(MinhasVIagensResponse *response, NSString *errorMessage))completion {
    
    
    NSString *url = [NSString stringWithFormat:APIMinhasViagens,IdUsuarioLogado];
    
    
    NSURLSessionDataTask *task = [self GETWithLogParams:url parameters:[NSDictionary new]
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {
                                                     
                                                     NSError *error;
                                                     
                                                     MinhasVIagensResponse *loginResponse = [[MinhasVIagensResponse alloc] initWithDictionary:responseObject error:&error];
                                                     
                                                     
                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {
                                                         
                                                         completion(loginResponse, nil);
                                                     }
                                                     
                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                     
                                                     if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                         completion(nil, NSLocalizedString(badCredentials, nil));
                                                     } else {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     }
                                                 }];
    
    return task;
}


- (NSURLSessionDataTask *)obterTripstersViagem:(NSNumber *)idViagem completion:(void (^)(PerfisViagemResponse *response, NSString *errorMessage))completion {
    
    
    NSString *url = [NSString stringWithFormat:APITripstersViagem,idViagem,IdUsuarioLogado];
    
    
    NSURLSessionDataTask *task = [self GETWithLogParams:url parameters:[NSDictionary new]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {
                                                    
                                                    NSError *error;
                                                    
                                                    PerfisViagemResponse *loginResponse = [[PerfisViagemResponse alloc] initWithDictionary:responseObject error:&error];
                                                    
                                                    
                                                    if (error) {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    } else {
                                                        
                                                        completion(loginResponse, nil);
                                                    }
                                                    
                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                    
                                                    if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                        completion(nil, NSLocalizedString(badCredentials, nil));
                                                    } else {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    }
                                                }];
    
    return task;
}

- (NSURLSessionDataTask *)obterMeusTripstersCompletion:(void (^)(PerfisViagemResponse *response, NSString *errorMessage))completion {
    
    
    NSString *url = [NSString stringWithFormat:APIMeusTripsters,IdUsuarioLogado];
    
    
    NSURLSessionDataTask *task = [self GETWithLogParams:url parameters:[NSDictionary new]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {
                                                    
                                                    NSError *error;
                                                    
                                                    PerfisViagemResponse *loginResponse = [[PerfisViagemResponse alloc] initWithDictionary:responseObject error:&error];
                                                    
                                                    
                                                    if (error) {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    } else {
                                                        
                                                        completion(loginResponse, nil);
                                                    }
                                                    
                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                    
                                                    if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                        completion(nil, NSLocalizedString(badCredentials, nil));
                                                    } else {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    }
                                                }];
    
    return task;
}


- (NSURLSessionDataTask *)deletarViagens:(NSNumber *)idViagem completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion {
    
    
    NSString *url = [NSString stringWithFormat:APIDeletarViagens,idViagem];
    
    
    NSURLSessionDataTask *task = [self PUTWithLogParams:url parameters:[NSDictionary new]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {
                                                    
                                                    NSError *error;
                                                    
                                                    ResponseBase *loginResponse = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];
                                                    
                                                    
                                                    if (error) {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    } else {
                                                        
                                                        completion(loginResponse, nil);
                                                    }
                                                    
                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                    
                                                    if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                        completion(nil, NSLocalizedString(badCredentials, nil));
                                                    } else {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    }
                                                }];
    
    return task;
}

- (NSURLSessionDataTask *)atualizarPush:(AtualizarPushRequest *)request completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion {
    
    
    
    NSURLSessionDataTask *task = [self PUTWithLogParams:APIAtualizarPush parameters:[request toDictionary]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {
                                                    
                                                    NSError *error;
                                                    
                                                    ResponseBase *loginResponse = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];
                                                    
                                                    
                                                    if (error) {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    } else {
                                                        
                                                        completion(loginResponse, nil);
                                                    }
                                                    
                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                    
                                                    if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                        completion(nil, NSLocalizedString(badCredentials, nil));
                                                    } else {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    }
                                                }];
    
    return task;
}



//Metodo que encapsula o método de POST somente para logar os parametros em desenvolvimento
- (NSURLSessionDataTask *)POSTWithLogParams:(NSString *)URLString
                                 parameters:(NSDictionary *)parameters
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {

//#if DEBUG
//    NSError *errorAutentication;
//    NSData *jsonDataAutentication = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&errorAutentication];
//    if (!errorAutentication) {
//        NSLog(@"POST URL :%@ >>>>>>>>>>>>> Json params:%@", URLString, [[NSString alloc] initWithData:jsonDataAutentication encoding:NSUTF8StringEncoding]);
//    }
//#endif
    
    
    [self.requestSerializer setValue:[TokenHelper retornaToken]  forHTTPHeaderField:@"Token"];
    

    NSURLSessionDataTask *task = [self POST:URLString parameters:parameters progress:nil
        success:^(NSURLSessionDataTask *task, id responseObject) {
            success(task, responseObject);
        }
        failure:^(NSURLSessionDataTask *task, NSError *error) {
#if DEBUG
            NSLog(@"%@", error);
#endif
            failure(task, error);
        }
    ];

    return task;
}

//Metodo que encapsula o método de PUT somente para logar os parametros em desenvolvimento
- (NSURLSessionDataTask *)PUTWithLogParams:(NSString *)URLString
                                parameters:(NSDictionary *)parameters
                                   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {

#if DEBUG
    NSError *errorAutentication;
    NSData *jsonDataAutentication = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&errorAutentication];
    if (!errorAutentication) {
        NSLog(@"PUT URL :%@ >>>>>>>>>>>>> Json params:%@", URLString, [[NSString alloc] initWithData:jsonDataAutentication encoding:NSUTF8StringEncoding]);
    }
#endif

    [self.requestSerializer setValue:[TokenHelper retornaToken]  forHTTPHeaderField:@"Token"];
    
    
    NSURLSessionDataTask *task = [self PUT:URLString parameters:parameters
        success:^(NSURLSessionDataTask *task, id responseObject) {
            success(task, responseObject);
        }
        failure:^(NSURLSessionDataTask *task, NSError *error) {
#if DEBUG
            NSLog(@"%@", error);
#endif
            failure(task, error);
        }
    ];

    return task;
}

- (NSURLSessionDataTask *)GETURLFormatedWithLogParams:(NSString *)URLString
                                           parameters:(NSDictionary *)parameters
                                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {

#if DEBUG
    NSError *errorAutentication;
    NSData *jsonDataAutentication = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&errorAutentication];
    if (!errorAutentication) {
        NSLog(@"GET URL :%@ >>>>>>>>>>>>> Json params:%@", URLString, [[NSString alloc] initWithData:jsonDataAutentication encoding:NSUTF8StringEncoding]);
    }
#endif

    [self.requestSerializer setValue:[TokenHelper retornaToken]  forHTTPHeaderField:@"Token"];
    
    
    NSURLSessionDataTask *task = [self GET:URLString parameters:nil progress:nil
        success:^(NSURLSessionDataTask *task, id responseObject) {
            success(task, responseObject);
        }
        failure:^(NSURLSessionDataTask *task, NSError *error) {
#if DEBUG
            NSLog(@"%@", error);
#endif
            failure(task, error);
        }
    ];

    return task;
}

//Metodo que encapsula o método de Get somente para logar os parametros em desenvolvimento
- (NSURLSessionDataTask *)GETWithLogParams:(NSString *)URLString
                                parameters:(NSDictionary *)parameters
                                   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {

    for (NSString *key in parameters.allKeys) {

        URLString = [URLString stringByAppendingString:[NSString stringWithFormat:@"/%@/%@", key, parameters[key]]];
    }

#if DEBUG
    NSError *errorAutentication;
    NSData *jsonDataAutentication = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&errorAutentication];
    if (!errorAutentication) {
        NSLog(@"GET URL :%@ >>>>>>>>>>>>> Json params:%@", URLString, [[NSString alloc] initWithData:jsonDataAutentication encoding:NSUTF8StringEncoding]);
    }
#endif
    
    
    [self.requestSerializer setValue:[TokenHelper retornaToken]  forHTTPHeaderField:@"Token"];
    

    NSURLSessionDataTask *task = [self GET:URLString parameters:nil progress:nil
        success:^(NSURLSessionDataTask *task, id responseObject) {
            success(task, responseObject);
        }
        failure:^(NSURLSessionDataTask *task, NSError *error) {
#if DEBUG
            NSLog(@"%@", error);
#endif
            failure(task, error);
        }];

    return task;
}



@end
