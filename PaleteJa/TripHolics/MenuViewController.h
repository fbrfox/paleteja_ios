//
//  MenuViewController.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuViewControllerDelegate <NSObject>

@required

-(void)menuMinhasTripsClicked;
-(void)menuMeusTripstersClicked;
-(void)meuPerfilClicked;
@end

@interface MenuViewController : UIViewController

@property(nonatomic,weak) id<MenuViewControllerDelegate>delegate;


@end
