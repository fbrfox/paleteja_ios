//
//  SharedPreferences.h
//  Movida Condominio
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/04/17.
//  Copyright © 2017 Movida Locação de Veículos Ltda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedPreferences : NSObject

+(void)setUserToken:(NSString *)token;
+(void)setUserId:(NSNumber *)userId;
+(void)setUserName:(NSString *)name;
+(void)setUserLocal:(NSString *)local;
+(void)setDeviceToken:(NSString *)token;
+(void)setUsuarioNovo:(NSNumber *)usuarioNovo;

+(BOOL)getUsuarioNovo;

+(NSString *)userLocal;
+(NSString *)userName;
+(NSString *)userToken;
+(NSNumber *)userId;
+(NSString *)deviceToken;


@end
