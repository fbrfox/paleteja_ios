//
//  LoginViewController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 25/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "LoginViewController.h"
#import "BaseController.h"
#import "CadastroUsuarioViewController.h"

@interface LoginViewController ()<FacebookControlerDelegate,UsuarioControllerDelegate>
{
 
    FacebookController *facebookController;
    UsuarioController *usuarioController;
    __weak IBOutlet JVFloatLabeledTextField *tfEmail;
    __weak IBOutlet JVFloatLabeledTextField *tfSenha;
    
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    facebookController = [[FacebookController alloc]initWithView:self.view delegate:self];
    usuarioController = [[UsuarioController alloc] initWithController:self delegate:self];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onIniciarSessaoTap:(id)sender {
    
    [usuarioController loginEmail:tfEmail.text senha:tfSenha.text];
    
}
- (IBAction)onFacebookLoginTap:(id)sender {
    
    [facebookController loginWithFacebook:self];
    
}

-(void)facebookLoginSuccess:(FacebookUser *)facebookUser{

    CadastroUsuarioViewController * cadastroVC =  [self.storyboard instantiateViewControllerWithIdentifier:@"CadastroUsuarioViewController"];
    
    cadastroVC.usuarioFacebook = facebookUser;
    
    [self.navigationController pushViewController:cadastroVC animated:YES];
    
    
}

-(void)facebookLoginCanceled{
    
    [UIHelper mostrarAlertaErro:@"Não foi possível efetuar o seu login com o facebook, possivelmente, você não deu acesso as permissões necessárias!" viewController:self];
}


-(void)loginSucesso{
    
    UIViewController *initialVC =  [[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateInitialViewController];
    
    [Delegate changeRootViewController:initialVC];
}

-(void)loginError:(NSString *)error{
    
    [UIHelper mostrarAlertaErro:error viewController:self];
}

@end
