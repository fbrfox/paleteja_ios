//
//  TokenHelper.m
//  Movida Condominio
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 06/03/17.
//  Copyright © 2017 Movida Locação de Veículos Ltda. All rights reserved.
//

#import "TokenHelper.h"
#import "RNEncryptor.h"



@implementation TokenHelper


+(NSString *)retornaToken{

    NSString *tokenKey = @"tripholics@2017";
    NSString*dataFormatada = [UIHelper dataParaString:[NSDate date] formato:@"yyyy"];
    
    NSString *stringParaCriptografar = [NSString stringWithFormat:@"%@%@",tokenKey,dataFormatada];
    
    NSError *error;
    
    
    NSData *dataCripto = [RNEncryptor encryptData:[stringParaCriptografar dataUsingEncoding:NSUTF8StringEncoding] withSettings:kRNCryptorAES256Settings password:tokenKey error:&error];
    
    
    NSString *tokenCripto = [dataCripto base64EncodedStringWithOptions:0];
    
    return tokenCripto;
}


@end
