//
//  Viagem.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 27/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ModelBase.h"


@protocol Viagem <NSObject>



@end

@interface Viagem : ModelBase

@property(nonatomic,strong) NSString *DataInicio;
@property(nonatomic,strong) NSString *DataFim;
@property(nonatomic,strong) NSString *Destino;
@property(nonatomic,strong) NSString *LatitudeDestino;
@property(nonatomic,strong) NSNumber *ViagemID;
@property(nonatomic,strong) NSNumber *LongitudeDestino;
@property(nonatomic,strong) NSString *MotivoViagem;
@property(nonatomic,strong) NSNumber *EmViagem;


@end
