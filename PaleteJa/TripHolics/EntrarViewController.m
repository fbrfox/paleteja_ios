//
//  EntrarViewController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 08/08/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "EntrarViewController.h"

@interface EntrarViewController ()

@end

@implementation EntrarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onTermoUsoTap:(id)sender {
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"http://www.tripholics.com.br/Termo-de-Uso.php"] options:[NSDictionary new] completionHandler:nil];
}

@end
