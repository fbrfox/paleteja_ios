//
//  MeusTripstersViewController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "MeusTripstersViewController.h"
#import "BaseController.h"
#import "ExcluirTripButton.h"
#import "PerfilViewController.h"


@interface MeusTripstersViewController ()<UITableViewDataSource,UITableViewDelegate,TripstersControllerDelegate>
{
    __weak IBOutlet UITableView *tbTripsters;
    
    TripstersController *controller;
    
    NSArray*tripsters;
}
@end

@implementation MeusTripstersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    controller = [[TripstersController alloc]initWithController:self delegate:self];
    
    
    if(_viagem)
    {
        [self setTitleNavigation:@"Tripsters Interessantes"];
        [controller loadTripstersViagem:_viagem.ViagemID];
    }
    else
    {
     [self setTitleNavigation:@"Meus Tripsters"];
        [controller loadMeusTripsters];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    Usuario *usuario = tripsters[indexPath.row];
    
    PerfilViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PerfilViewController"];
    
    vc.usuario = usuario;
    vc.viagem = _viagem;
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return tripsters.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellTripster" forIndexPath:indexPath];
    
    Usuario *usuario = tripsters[indexPath.row];
    
    
    UIImageView *imageview = [cell viewWithTag:100];
    
    UILabel* lbNome = [cell viewWithTag:101];
    UILabel *lbEndereco =[cell viewWithTag:102];
    UILabel *lbAcompanhante = [cell viewWithTag:103];
    UILabel *lbSexo = [cell viewWithTag:110];
    
    
    
    ExcluirTripButton *btAdd = [cell viewWithTag:104];
    if(_viagem)
    {
        if(![usuario.Match boolValue])
        {
            btAdd.enabled = YES;
            btAdd.alpha = 1;
        }
        
        btAdd.usuario = usuario;
        btAdd.viagem = _viagem;
        
        [btAdd addTarget:self action:@selector(btAddClick:) forControlEvents:UIControlEventTouchUpInside];
        
        lbAcompanhante.alpha = 1;
        lbAcompanhante.text = usuario.Acompanhante;
    }
    else
    {
        btAdd.enabled = NO;
        btAdd.alpha = 0.0f;
        lbAcompanhante.alpha = 0.0f;
    }
    
    
    lbNome.text = usuario.Nome;
    lbEndereco.text = usuario.Localidade;
    [imageview sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:UrlFormatPerfilUsuario,usuario.Id]]];
    imageview.clipsToBounds = YES;
    
    lbSexo.text = usuario.Sexo;
    
    return cell;
}


-(void)btAddClick:(ExcluirTripButton *)button{
    
    [controller adicionarMatchIdUsuario:button.usuario.Id viagemId:button.viagem.ViagemID];
    
}

-(void)loadTripstersSuccess:(NSArray<Usuario> *)usuarios{
    
    tripsters = usuarios;
    [tbTripsters reloadData];
    
}

-(void)loadTripstersErro:(NSString *)error{
    
    [UIHelper mostrarAlertaErro:error viewController:self];
}

-(void)matchAdicionadoComSucesso:(NSString *)msg{
    
    [UIHelper mostrarAlertaSucesso:msg viewController:self];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    
}

@end
