//
//  FacebookController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "FacebookController.h"


@implementation FacebookController



-(id)initWithView:(UIView *)view delegate:(id<FacebookControlerDelegate>)delegate{
    
    self = [super init];
    
    if(self)
    {
        self.view = view;
        self.delegate = delegate;
    }
    
    return self;
}


-(void)loginWithFacebook:(UIViewController *)viewController{
    
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc]init ];
    
    
    
    [loginManager logInWithReadPermissions:@[@"public_profile",@"email"] fromViewController:viewController handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
       
        if (error) {
            NSLog(@"Process error");
            [UIHelper hideProgress];
            [_delegate facebookLoginCanceled];
        } else if (result.isCancelled) {
            NSLog(@"Cancelled");
            [UIHelper hideProgress];
            [_delegate facebookLoginCanceled];
        } else {
    
            [self loginApi:[FBSDKAccessToken currentAccessToken].userID];
        }
        
    }];
    
}


-(void)loginApi:(NSString* )facebookID{
    
    [UIHelper showProgress];
    
    LoginRequest *request = [[LoginRequest alloc]initWithFacebookID:facebookID];
    
    [[APIClient sharedManager] login:request completion:^(CadastroPerfilResponse *response, NSString *errorMessage) {
       
        
        if(response.Sucesso)
        {
            [UIHelper hideProgress];
            
            UIViewController *vc = [[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateInitialViewController];
            [Delegate changeRootViewController:vc];
        }
        else
        {
            [self obterDadosFacebook];
        }
        
    }];

    
}

-(void)obterDadosFacebook{
    
    NSDictionary*dictironary = @{@"fields":@"id,name,link,email,gender"};
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:dictironary]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         
         [UIHelper hideProgress];
         
         if (!error) {
             NSLog(@"fetched user:%@", result);
             
             NSError *erroFacebookUser;
             
             FacebookUser *usuarioFacebook = [[FacebookUser alloc]initWithDictionary:result error:&erroFacebookUser];
             
             usuarioFacebook.imageUrl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",usuarioFacebook.idFacebook];
             
             if(erroFacebookUser)
                 [_delegate facebookLoginCanceled];
             else
                 [_delegate facebookLoginSuccess:usuarioFacebook];
             
         }
         else
             [_delegate facebookLoginCanceled];
     }];
    
}


@end
