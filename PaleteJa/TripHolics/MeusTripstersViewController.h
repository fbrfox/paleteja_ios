//
//  MeusTripstersViewController.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Viagem.h"

@interface MeusTripstersViewController : BaseViewController

@property(nonatomic,strong) Viagem *viagem;

@end
