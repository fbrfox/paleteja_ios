//
//  CadastroUsuarioViewController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 24/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "CadastroUsuarioViewController.h"
#import "BaseController.h"
#import "CadastroEnderecoViewController.h"
#import "SexoHelper.h"

@interface CadastroUsuarioViewController ()<FacebookControlerDelegate,UsuarioControllerDelegate>
{
    
    __weak IBOutlet JVFloatLabeledTextField *tfNome;
    __weak IBOutlet JVFloatLabeledTextField *tfEmail;
    __weak IBOutlet JVFloatLabeledTextField *tfSenha;
    __weak IBOutlet TextFieldPickerHelper *tfSexo;
    FacebookController *facebookController;
    __weak IBOutlet UIButton *btFace;
    
    UsuarioController *usuarioController;
    
    NSArray*sexos;
}
@end

@implementation CadastroUsuarioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [super configdismissOnTap:@[tfNome,tfEmail,tfSexo,tfSenha]];
    
    sexos = [SexoHelper retornaSexos];
    
    if(_usuarioFacebook)
    {
        btFace.alpha = 0;
        btFace.enabled = NO;
        [self preencheCamposFacebook];
    }
    else
        facebookController = [[FacebookController alloc] initWithView:self.view delegate:self];
    
    
    usuarioController = [[UsuarioController alloc]initWithController:self delegate:self];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    
    [super viewWillAppear:animated];
    
}


-(void)preencheCamposFacebook{
    
    tfNome.text = _usuarioFacebook.name;
    tfEmail.text = _usuarioFacebook.email;
    tfSexo.text = [_usuarioFacebook.gender isEqualToString:@"male"] ? @"Masculino" : @"Feminino";
    
}



- (IBAction)onProximoTap:(id)sender {
    
    CadastroPerfilRequest *request = [CadastroPerfilRequest new];
    
    request.Nome = tfNome.text;
    request.Sexo = tfSexo.text;
    request.Senha = tfSenha.text;
    request.Email = tfEmail.text;
    
    if(_usuarioFacebook){
        request.Foto = [UIHelper imageUrlToBase64Image:_usuarioFacebook.imageUrl];
        request.FacebookID = _usuarioFacebook.idFacebook;
    
    }
    [usuarioController validateCadastroUsuario:request];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}
- (IBAction)onFacebookClick:(id)sender {
    
    [facebookController loginWithFacebook:self];
}

-(void)facebookLoginCanceled{
    
    
}


-(void)facebookLoginSuccess:(FacebookUser *)facebookUser{
    
    _usuarioFacebook = facebookUser;
    [self preencheCamposFacebook];
}

-(void)toCadastroEndereco:(CadastroPerfilRequest *)request{
    
    
    CadastroEnderecoViewController *tempVc=  [self.storyboard instantiateViewControllerWithIdentifier:@"CadastroEnderecoViewController"];
    
    tempVc.cadastroPerfilRequest = request;
    
    [self.navigationController pushViewController:tempVc animated:YES];
    
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView withTextField:(TextFieldPickerHelper *)sender{
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender{
    
    return sexos.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender{
    
    SexoHelper *sexo = sexos[row];
    return sexo.descricao;
    
}

- (void)doneClicked:(TextFieldPickerHelper *)sender withIndex:(NSInteger)index{
    
    [super dismissKeyboard];
    SexoHelper *sexo = sexos[index];
    tfSexo.text = sexo.descricao;
    
    
}


@end
