//
//  LoginRequest.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ModelBase.h"

@interface LoginRequest : ModelBase

@property(nonatomic,strong) NSString *Email;
@property(nonatomic,strong) NSString *Senha;
@property(nonatomic,strong) NSString *FacebookID;
@property(nonatomic,strong) NSString *DigitalID;


-(id)initWithEmail:(NSString *)email senha:(NSString *)senha;

-(id)initWithFacebookID:(NSString *)facebookID;

@end
