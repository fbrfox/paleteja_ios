//
//  CadastroTelefoneViewController.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 24/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "CadastroTelefoneViewController.h"

@interface CadastroTelefoneViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate,UsuarioControllerDelegate,UIHelperDelegate>
{
    NSString *imageSelected;
    __weak IBOutlet JVFloatLabeledTextField *tfTelefone;
    __weak IBOutlet JVFloatLabeledTextField *tfPais;
    
    UsuarioController *controller;
}

@end

@implementation CadastroTelefoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tfPais.text = _request.TelefonePais;
    controller = [[UsuarioController alloc]initWithController:self delegate:self];
    
    imageSelected = _request.Foto;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    
    [super viewWillAppear:animated];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onPhotoTap:(id)sender {
    
    
    if(_request.FacebookID && _request.Foto)
    {
        [UIHelper mostrarAlertaInfo:@"Já estamos utilizando a sua foto do perfil do facebook" viewController:self];
        return;
    }
    
    UIAlertController *alertaPhotoController = [UIAlertController alertControllerWithTitle:@"Selecione uma opção" message:@"Selecione se deseja capturar o selecionar uma foto da galeria" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [alertaPhotoController addAction:[UIAlertAction actionWithTitle:@"Galeria" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self showGaleria];
        
    }]];
    
    
    [alertaPhotoController addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self showCamera];
        
    }]];
    
    
    [alertaPhotoController addAction:[UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    [self presentViewController:alertaPhotoController animated:YES completion:nil];
    
}


-(void)showGaleria{
    
    [self showDevicePhoto:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    
}

-(void)showCamera{
    
    [self showDevicePhoto:UIImagePickerControllerSourceTypeCamera];
}


-(void)showDevicePhoto:(UIImagePickerControllerSourceType)type{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = type;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:Nil];

    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *, id> *)info {
    
    UIImage *pickedImage = info[UIImagePickerControllerOriginalImage];
    imageSelected = [UIHelper imageToNSString:pickedImage];
    [picker dismissViewControllerAnimated:NO completion:nil];
    [UIHelper mostrarAlerta:@"Foto capturada com sucesso!" viewController:self];
    
}
- (IBAction)onSalvarTap:(id)sender {
    
    
    _request.TelefoneCodigo =  @"0";
    _request.TelefoneNumero = tfTelefone.text;
    _request.Foto = imageSelected;
    [controller validateCadastroTelefone:_request];
    
}



-(void)cadastroEfetuadoComSucesso:(NSString *)msg{
    
    [UIHelper mostrarAlertaSucesso:msg delegate:self viewController:self];
}

-(void)erroAoCadastrar:(NSString *)error{
    
    [UIHelper mostrarAlertaErro:error viewController:self];
    
}

-(void)alertControllerOkClicked{
    
    UIViewController *initialVC =  [[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateInitialViewController];    
    [Delegate changeRootViewController:initialVC];

}

@end
