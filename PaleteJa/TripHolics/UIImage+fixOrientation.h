//
//  UIImage+fixOrientation.h
//  Movida Condominio
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 12/05/17.
//  Copyright © 2017 Movida Locação de Veículos Ltda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

 - (UIImage *)fixOrientation;

@end
