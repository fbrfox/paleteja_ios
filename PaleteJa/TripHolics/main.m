//
//  main.m
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 24/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
