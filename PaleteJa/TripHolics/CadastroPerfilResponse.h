//
//  CadastroPerfilResponse.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ModelBase.h"
#import "Usuario.h"

@interface CadastroPerfilResponse : ResponseBase

@property(nonatomic,strong) NSString *Token;

@property(nonatomic,strong) Usuario *Usuario;

@end
