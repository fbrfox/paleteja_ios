//
//  TripstersController.h
//  TripHolics
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 27/07/17.
//  Copyright © 2017 MPM. All rights reserved.
//

#import "ResponseBase.h"
#import "Usuario.h"

@protocol TripstersControllerDelegate <NSObject>

@optional

-(void)perfilDenunciadoSuccess:(NSString*)msg;
-(void)loadTripstersErro:(NSString *)error;
-(void)loadTripstersSuccess:(NSArray<Usuario> *)usuarios;
-(void)matchAdicionadoComSucesso:(NSString *)msg;

@end

@interface TripstersController : ResponseBase

@property(nonatomic,strong) UIViewController *vc;
@property(nonatomic,weak) id<TripstersControllerDelegate> delegate;


-(id)initWithController:(UIViewController *)vc delegate:(id<TripstersControllerDelegate>)delegate;

-(void)loadTripstersViagem:(NSNumber *)idViagem;

-(void)loadMeusTripsters;

-(void)adicionarMatchIdUsuario:(NSNumber *)idUsuario viagemId:(NSNumber *)idViagem;

-(void)denunciarPerfil:(NSNumber *)idPerfil;

@end
