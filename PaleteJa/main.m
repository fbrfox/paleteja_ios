//
//  main.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/11/17.
//  Copyright © 2017 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
