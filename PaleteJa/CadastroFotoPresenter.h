//
//  CadastroFotoPresenter.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 23/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CadastroFotoView.h"
#import "Autenticacao.h"

@protocol CadastroFotoView;

@protocol CadastroFotoPresenter <NSObject>

-(void)initWithView:(id< CadastroFotoView > )theView;

@property id <CadastroFotoView> view;

-(void)nextStepClick;

@end

@interface CadastroFotoPresenter : NSObject

@end
