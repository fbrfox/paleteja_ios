//
// Created by Movida on 09/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Palete;


@interface OfertaSegue : UIStoryboardSegue

@property Palete *palete;

@end