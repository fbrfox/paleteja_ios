//
//  CadastrarDadosVendedorRequest.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//





#import "ModelBase.h"

@interface CadastrarDadosVendedorRequest : ModelBase

@property(nonatomic,strong) NSString *Documento;
@property(nonatomic,strong) NSString *Foto;
@property(nonatomic,strong) NSString *TelefoneFixo;
@property(nonatomic,strong) NSString *Celular;
@property(nonatomic,strong) NSString *Endereco;
@property(nonatomic) BOOL CelularWhatsapp;
@property(nonatomic,strong) NSString *Site;
@property(nonatomic,strong) NSNumber *Latitude;
@property(nonatomic,strong) NSNumber *Longitude;
@property(nonatomic,strong) NSString *Cidade;
@property(nonatomic,strong) NSString *Estado;
@property(nonatomic,strong) NSNumber *RaioEntrega;
@property(nonatomic,strong) NSNumber *TipoEntrega;
@property(nonatomic,strong) NSNumber *QuantidadeMinimaEntrega;
@property(nonatomic,strong) NSNumber *TipoProduto;

@property(nonatomic, strong)NSNumber *TipoUsuario;

@end
