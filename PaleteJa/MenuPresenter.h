//
// Created by Movida on 02/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MenuView;

@protocol MenuPresenter <NSObject>


-(void)initWithView:(id <MenuView> )theView;

@property (nonatomic, strong) id<MenuView> view;



@end

