//
//  HomeViewController.m
//  PaleteJa
//
//  Created by Movida on 01/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "HomeViewController.h"
#import "HomePresenterImpl.h"
#import "DetalheVendedorSegue.h"

#define kSegueDetalheVendedor @"detalheVendedorSegue"
#define kSegueOferta  @"ofertaSegue"

@interface HomeViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewTopConstraint;
@property CGFloat originalViewTopConstraint;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toMinhasOfertas) name:@"MinhasOfertas" object:nil];


    _tfEstado.delegate = self;
    _originalViewTopConstraint = _viewTopConstraint.constant;
    _tbVendedores.delegate =self;
    _presenter = [[HomePresenterImpl alloc] init];
    [_presenter initWithView:self];

}

- (void)toMinhasOfertas {

    [self performSegueWithIdentifier:@"minhasOfertasSegue" sender:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if([segue.identifier isEqualToString:kSegueDetalheVendedor]){
        DetalheVendedorSegue* ns = (DetalheVendedorSegue*) segue;
        ns.usuario = _usuarios[(NSUInteger) _indexSelected];
    }

}

- (IBAction)onFilterClick:(id)sender {

    [self animateViewFilter];
    
}

-(void)animateViewFilter{
    
    if(_viewTopConstraint.constant != 0)
        _viewTopConstraint.constant = 0;
    else
        _viewTopConstraint.constant = _originalViewTopConstraint;
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}
- (IBAction)onCancelarClick:(id)sender {
    
    [self animateViewFilter];
    [_presenter onCancelFiltro];

}
- (IBAction)onFIltrarClick:(id)sender {

    [self animateViewFilter];
    [_presenter onFilterClick];
    
}

#pragma mark - HomeView
-(int)getRaio{
    
    return (int) _slKm.value;
    
}

-(NSString *)getCidade{
    
    return _tfEstado.text;
}


- (NSString *)getEstado {
    return _tfEstado.text;
}

-(double)getLatitude{
    
    return self.currentLocation.coordinate.latitude;
}

-(double)getLongitude{
    
    return self.currentLocation.coordinate.longitude;
    
}

-(void)onListUsuariosReceived:(NSArray<Usuario> *)usuarios{

    _usuarios = usuarios;
    _dataSource = [[UsuariosPaletesDataSource alloc] initWithUsuarios:_usuarios andViewController:self];
    _tbVendedores.dataSource = _dataSource;
    [_tbVendedores reloadData];


}
-(void)navigateToDetails:(int)index{


    _indexSelected = index;
    [self performSegueWithIdentifier:kSegueDetalheVendedor sender:nil];


}
-(void) showLoading{

    [UIHelper showProgress];
}
-(void) hideLoading{

    [UIHelper hideProgress];
}
-(void) showAlertErro:(NSString *)error{

    [UIHelper mostrarAlertaErro:error viewController:self];
}

-(void)setStates:(NSArray *)estados{
    
    _estados = estados;
}

- (void)setPrecoMedio:(NSString *)precoMedio {

    _tfPrecoMedio2.text = _tfPrecoMedio.text = precoMedio;
}


#pragma mark TextFieldPickerHelperDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView withTextField:(TextFieldPickerHelper *)sender{
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender{
    
    
    return _estados.count;
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender{
    
    
    return _estados[row];
}

- (void)doneClicked:(TextFieldPickerHelper *)sender withIndex:(NSInteger)index{

    [sender resignFirstResponder];
    _tfEstado.text = _estados[index];
}


- (void)navigateToOrder {

    [self performSegueWithIdentifier:kSegueOferta sender:nil];

}

- (IBAction)onOfertaTap:(id)sender {
    
    [_presenter onNavigateToOrder];
}




#pragma TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [_presenter onNavigateToDetail:indexPath.row];

}


- (void)viewWillDisappear:(BOOL)animated {

}
- (IBAction)onMinhaLocalizacaoClick:(id)sender {
    
    [self animateViewFilter];
    [_presenter onFilterClick];
    
}

@end
