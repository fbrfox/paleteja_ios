//
// Created by Movida on 05/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import "DetalheVendedorSegue.h"
#import "Usuario.h"
#import "DetalheVendedorViewController.h"


@implementation DetalheVendedorSegue {

}

- (void)perform {


    UIViewController* src = self.sourceViewController;
    UIViewController* dst = self.destinationViewController;

    if([dst isKindOfClass:[DetalheVendedorViewController class]]){
        DetalheVendedorViewController* dvc = (DetalheVendedorViewController*) dst;
        dvc.usuario = _usuario;
    }

    [src.navigationController pushViewController:dst animated:YES];

}


@end