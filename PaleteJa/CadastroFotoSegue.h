//
//  CadastroFotoSegue.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 23/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CadastroFotoSegue : UIStoryboardSegue

@property NSString* foto;

@end
