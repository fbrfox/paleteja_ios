//
//  ObterUsuariosPaletesResponse.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 18/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "ResponseBase.h"


@protocol Usuario;

@interface ObterUsuariosPaletesResponse : ResponseBase

@property(nonatomic,strong) NSArray<Usuario>* Usuarios;


@end
