//
//  MinhasOfertasSegue.m
//  PaleteJa
//
//  Created by Movida on 22/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "MinhasOfertasSegue.h"
#import "MinhasOfertasViewController.h"

@implementation MinhasOfertasSegue


- (void)perform {
    UIViewController* src = self.sourceViewController;
    UIViewController* dst = self.destinationViewController;
    
    if([dst isKindOfClass:[MinhasOfertasViewController class]]){
        MinhasOfertasViewController* dvc = (MinhasOfertasViewController*) dst;
        dvc.origem= _origem;
    }
    
    [src.navigationController pushViewController:dst animated:YES];
}


@end
