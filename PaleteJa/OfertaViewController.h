//
//  OfertaViewController.h
//  PaleteJa
//
//  Created by Movida on 05/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OfertaView.h"

static NSString *const kMinhasOfertasSegue = @"minhasOfertasSegue";
@class Palete;
@class OfertaPresenterImpl;

@interface OfertaViewController : BaseViewController<OfertaView, UIHelperDelegate, TextFieldPickerHelperDelegate>

@property OfertaPresenterImpl *presenter;

@property Palete *palete;

@end
