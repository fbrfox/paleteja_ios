//
//  LoginViewController.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 29/11/17.
//  Copyright © 2017 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginView.h"
#import "LoginPresenterImpl.h"
#import "Autenticacao.h"

@interface LoginViewController : BaseViewController <LoginView>

@property LoginPresenterImpl* presenter;
@property Autenticacao* autenticacao;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfEmail;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfSenha;



@end
