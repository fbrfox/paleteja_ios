//
//  SharedPreferences.m
//  Movida Condominio
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/04/17.
//  Copyright © 2017 Movida Locação de Veículos Ltda. All rights reserved.
//

#import "SharedPreferences.h"

@implementation SharedPreferences


+(void)setUserToken:(NSString *)token{
    
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"KeyUserToken"];
}

+(void)setUserId:(NSNumber *)userId{
    if(userId)
        [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"KeyUserID"];
    else
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"KeyUserID"];
    
}

+(void)setUserName:(NSString *)name{
    
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"Nome"];
    
}

+(void)setUserTipo:(NSNumber *)local{
    
    [[NSUserDefaults standardUserDefaults] setObject:local forKey:@"Tipo"];
    
}

+(NSNumber *)userTipo{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"Tipo"];
}

+(NSString *)userName{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"Nome"];
}

+(NSString *)userToken{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"KeyUserToken"];
}

+(NSNumber *)userId{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"KeyUserID"];
}


+(void)setCadastroCompleto:(NSNumber *)usuarioNovo{
    
    [[NSUserDefaults standardUserDefaults] setObject:usuarioNovo forKey:@"CadastroCompleto"];
    
}

+(BOOL)getCadastroCompleto{
    
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"CadastroCompleto"] boolValue];
}


+(void)setDeviceToken:(NSString *)token{
    
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"DeviceToken"];
    
}

+(NSString *)deviceToken{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"];
}

@end
