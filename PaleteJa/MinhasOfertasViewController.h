//
//  MinhasOfertasViewController.h
//  PaleteJa
//
//  Created by Movida on 22/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MinhasOfertasView.h"
#import "MInhasOfertasPresenterImpl.h"

@class MinhasOfertasDataSource;


@interface MinhasOfertasViewController : UIViewController<MinhasOfertasView, UITableViewDelegate, UIHelperDelegate>

@property int origem;
@property MinhasOfertasDataSource *dataSource;
@property MInhasOfertasPresenterImpl *presenter;
@property NSArray <Oferta> *ofertas;


@end
