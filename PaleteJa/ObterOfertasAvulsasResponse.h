//
//  ObterOfertasAvulsasResponse.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 18/01/18.
//  Copyright © 2018 phep. All rights reserved.
//


#import "ResponseBase.h"

@protocol Oferta;


@interface ObterOfertasAvulsasResponse : ResponseBase

@property(nonatomic,strong) NSArray<Oferta>* Ofertas;

@end
