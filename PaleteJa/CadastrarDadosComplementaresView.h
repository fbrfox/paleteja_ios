//
//  CadastrarDadosComplementaresView.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 23/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CadastrarDadosComplementaresView <NSObject>

-(NSString *)getDocumento;
-(NSString *)getTelefone;
-(NSString *)getCelular;
-(NSString *)getEndereco;
-(GMSPlace *)getPlace;
-(void)setEndereco:(GMSPlace *)place;
-(BOOL)getIsWhatsapp;



-(void)showAlertError:(NSString *)error;
-(void)registerWithSuccessToHome;
-(void)registerWithSucessToVendor:(CadastrarDadosVendedorRequest *)request;
-(void)showProgress;
-(void)hideProgress;


@end

