//
//  SelecionarTipoUsuarioViewController.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 19/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "SelecionarTipoUsuarioViewController.h"
#import "SelecionarTipoUsuarioSegue.h"

#define kSegueNavigateToCadastro @"tipoUsuarioSegue"

@interface SelecionarTipoUsuarioViewController ()

@end

@implementation SelecionarTipoUsuarioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _presenter = [[SelecionarTipoUsuarioPresenterImpl alloc]init];
    [_presenter initWithView:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:kSegueNavigateToCadastro]){
        SelecionarTipoUsuarioSegue* ns = (SelecionarTipoUsuarioSegue*) segue;
        ns.tipoUsuario = _typeUser;
    }
}

-(void)selectTypeRegister:(NSNumber *)type{
    
    _typeUser = type;
     [self performSegueWithIdentifier:kSegueNavigateToCadastro sender:self];
}
- (IBAction)onComprarClick:(id)sender {
    
    [_presenter buttonCompradorClicked];
}
- (IBAction)onVenderClick:(id)sender {
    
    [_presenter buttonVendedorClicked];
}


-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = NO;

}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

@end
