//
//  SelecionarTipoUsuarioViewController.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 19/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelecionarTipoUsuarioView.h"
#import "SelecionarTipoUsuarioPresenterImpl.h"

@interface SelecionarTipoUsuarioViewController : BaseViewController <SelecionarTipoUsuarioView>

@property NSNumber* typeUser;

@property SelecionarTipoUsuarioPresenterImpl *presenter;

@end
