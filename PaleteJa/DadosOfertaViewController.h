//
//  DadosOfertaViewController.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Oferta;

@interface DadosOfertaViewController : BaseViewController

@property Oferta* oferta;


@end
