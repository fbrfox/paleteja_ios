//
//  MeuPerfilViewController.h
//  PaleteJa
//
//  Created by Pedro  on 02/04/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TipoEntrega;

@protocol TipoProduto;

@interface MeuPerfilViewController : BaseViewController <TextFieldPickerHelperDelegate, GMSAutocompleteViewControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property(nonatomic, strong) NSArray <TipoProduto> *tiposProduto;
@property(nonatomic, strong) NSArray <TipoEntrega> *tiposEntrega;

@end
