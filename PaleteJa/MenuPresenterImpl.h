//
// Created by Movida on 02/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MenuPresenter.h"
#import "MenuView.h"


@interface MenuPresenterImpl : NSObject <MenuPresenter>
@end