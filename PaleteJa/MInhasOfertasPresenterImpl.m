//
// Created by Movida on 22/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import "MInhasOfertasPresenterImpl.h"


@implementation MInhasOfertasPresenterImpl {

}
@synthesize minhasOfertasView;

- (void)initWithView:(id <MinhasOfertasView>)view {

    self.minhasOfertasView = view;
    [self obterMinhasOfertas];

}

- (void)obterMinhasOfertas {

    [minhasOfertasView showLoading];

    [[APIClient sharedManager] obterMinhasOfertasCompradorCompletion:^(ObterOfertasAvulsasResponse *response, NSString *errorMessage) {

        [minhasOfertasView hideLoading];

        if(errorMessage)
            [minhasOfertasView showAlertErro:errorMessage];
        else if(!response.Sucesso)
            [minhasOfertasView showAlertErro:response.Mensagem];
        else
            [minhasOfertasView onListOfertasReceived:response.Ofertas];

    }];


}

@end