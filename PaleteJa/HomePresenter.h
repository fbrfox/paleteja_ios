//
// Created by Movida on 02/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol HomeView;

@protocol HomePresenter <NSObject>

- (void)initWithView:(id <HomeView>)theView;

- (void)onFilterClick;

- (void)onNavigateToDetail:(int)index;

- (void)onNavigateToOrder;

- (void)onCancelFiltro;

@property(nonatomic, strong) id <HomeView> view;


@end

