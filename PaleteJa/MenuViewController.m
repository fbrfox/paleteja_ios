//
//  MenuViewController.m
//  PaleteJa
//
//  Created by Movida on 02/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuPresenterImpl.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _presenter = [[MenuPresenterImpl alloc] init];
    [_presenter initWithView:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onCloseTap:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onSairClick:(id)sender {

    [AutenticacaoHelper logout];
    [Delegate changeRootViewController:[[UIStoryboard storyboardWithName:@"Entrar" bundle:nil] instantiateInitialViewController]];

}
- (IBAction)onMinhasOfertasClick:(id)sender {


    [self dismissViewControllerAnimated:YES completion:^{

        [[NSNotificationCenter defaultCenter] postNotificationName:@"MinhasOfertas" object:nil];

    }];



}

- (void)setName:(NSString *)name andPhoto:(NSURL *)photo {

    _lbNome.text = name;
    [_iv_perfil sd_setImageWithURL:photo];
    [_iv_perfil setClipsToBounds:YES];

}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (IBAction)onMeusPaletesClick:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MeusPaletes" object:nil];
        
    }];
    
}

- (IBAction)onMeuPerfilClicked:(id)sender {

    __kindof UIViewController *controller;

    @try {
        controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MeuPerfilViewController"];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception occurred: %@, %@", exception, [exception userInfo]);
    }

    if(!controller){

        controller = [[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateViewControllerWithIdentifier:@"MeuPerfilViewController"];
    }
    
    
    [self presentViewController:controller animated:YES completion:nil];
    
}


@end
