//
//  OfertaViewController.m
//  PaleteJa
//
//  Created by Movida on 05/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "OfertaViewController.h"
#import "Palete.h"
#import "OfertaPresenterImpl.h"
#import "TipoEntrega.h"
#import "TipoProduto.h"
#import "MinhasOfertasSegue.h"

@interface OfertaViewController ()
{
    __weak IBOutlet JVFloatLabeledTextField *tfQuantidade;
    __weak IBOutlet JVFloatLabeledTextField *tfValor;
    __weak IBOutlet TextFieldPickerHelper *tfTipoEntrega;
    __weak IBOutlet JVFloatLabeledTextField *tfEndereco;
    __weak IBOutlet TextFieldPickerHelper *tfTipoProduto;

    NSArray *tiposEntrega;
    NSArray *tiposProduto;
    GMSPlace *localSelecionado;

    int tipoEntregaSelecionado;
    int tipoProdutoSelecionado;
    
}

@end

@implementation OfertaViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {

    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _presenter = [[OfertaPresenterImpl alloc] init];
    [_presenter initWithOfertaView:self viewController:self andPalete:_palete];
    [super configdismissOnTap:@[tfTipoEntrega,tfValor,tfQuantidade,tfEndereco,tfTipoProduto]];
    
    tfTipoEntrega.delegate = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:kMinhasOfertasSegue]){
        MinhasOfertasSegue *kSegue = (MinhasOfertasSegue *)segue;
        kSegue.origem = 1;
    }

}

- (IBAction)btRegistrarClick:(id)sender {

    [_presenter onRegisterOferta];
}


- (void)setEndereco:(GMSPlace *)place {

    localSelecionado = place;
    tfEndereco.text = localSelecionado.name;
}


- (int)getQuantidade {
    return [tfQuantidade.text intValue];
}

- (double)getValor {
    
    double valor = [[[tfValor.text stringByReplacingOccurrencesOfString:@"R$" withString:@""] stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    
    return  valor;
}

- (int)getTipoEntrega {
    return tipoEntregaSelecionado;
}

- (int)getTipoProduto {
    return tipoProdutoSelecionado;
}

- (GMSPlace *)getEndereco {
    return localSelecionado;
}

- (void)fillTipoEntrega:(NSArray *)tipos {

    tiposEntrega = tipos;
    

}

- (void)fillTipoProduto:(NSArray *)tipos {

    tiposProduto = tipos;
    

}

- (void)showProgress {

    [UIHelper showProgress];

}

- (void)hideProgress {

    [UIHelper hideProgress];
}

- (void)showAlertError:(NSString *)alertError {

    [UIHelper mostrarAlertaErro:alertError viewController:self];
}

- (void)showSucess:(NSString *)mensagem {

    [UIHelper mostrarAlertaSucesso:mensagem viewController:self delegate:self];

}

- (void)toMinhasOfertasView {

    [self performSegueWithIdentifier:kMinhasOfertasSegue sender:nil];


}

- (void)limpaCampos {

    tfEndereco.text = @"";
    tipoProdutoSelecionado = 0;
    tipoEntregaSelecionado = 0;
    tfQuantidade.text = @"";
    tfValor.text = @"";
    localSelecionado = nil;


}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{



    if([textField isEqual:tfEndereco])
    {
        [_presenter onTextEnderecoBeginEditing];
        return NO;
    }

    return YES;

}

- (void)alertControllerOkClicked {

    [_presenter onAlertClickOk];

}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView withTextField:(TextFieldPickerHelper *)sender {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender {
    if([sender isEqual:tfTipoProduto])
        return tiposProduto.count;

    return tiposEntrega.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component withTextField:(TextFieldPickerHelper *)sender {

    if([sender isEqual:tfTipoProduto])
    {
        TipoProduto *tipo = tiposProduto[row];
        return tipo.descricao;
    }
    else{

        TipoEntrega *tipo = tiposEntrega[row];
        return tipo.descricao;
    }



}

- (void)doneClicked:(TextFieldPickerHelper *)sender withIndex:(NSInteger)index {


    [sender resignFirstResponder];

    if([sender isEqual:tfTipoProduto])
    {
        TipoProduto *tipo = tiposProduto[index];
        tipoProdutoSelecionado = index + 1;
        tfTipoProduto.text = tipo.descricao;
    }
    else{

        TipoEntrega *tipo = tiposEntrega[index];
        tipoEntregaSelecionado = index + 1;
        tfTipoEntrega.text = tipo.descricao;
    }


}
- (IBAction)onBackClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)setPalete {

    tfTipoProduto.text = _palete.TipoProdutoDesc;
    tipoProdutoSelecionado = _palete.TipoProduto;
    tfValor.text = [UIHelper formatarPreco:_palete.PrecoPromocao];
    tfQuantidade.text = [NSString stringWithFormat:@"%d",_palete.Saldo];

}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    if([textField isEqual:tfValor]) {

        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];

        NSString *soNumero = [textField.text stringByReplacingOccurrencesOfString:@"R$" withString:@""];

        textField.text = [formatter stringFromNumber:@([soNumero doubleValue])];
    }

    return YES;
}

@end
