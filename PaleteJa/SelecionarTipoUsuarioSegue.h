//
//  SelecionarTipoUsuarioSegue.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 19/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelecionarTipoUsuarioSegue : UIStoryboardSegue

@property NSNumber* tipoUsuario;

@end
