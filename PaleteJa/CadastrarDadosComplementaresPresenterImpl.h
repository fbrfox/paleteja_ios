//
//  CadastrarDadosComplementaresPresenterImpl.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 23/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CadastrarDadosComplementaresPresenter.h"


@interface CadastrarDadosComplementaresPresenterImpl : NSObject<CadastrarDadosComplementaresPresenter,GMSAutocompleteViewControllerDelegate>



@end
