//
// Created by Movida on 06/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import "OfertaPresenterImpl.h"
#import "TipoProdutoRepository.h"
#import "TipoEntrega.h"
#import "TipoEntregaRepository.h"
#import "Palete.h"


@implementation OfertaPresenterImpl {

}
@synthesize ofertaView, palete,viewController;







-(void)initWithOfertaView:(id<OfertaView> )view viewController:(UIViewController *)controller andPalete:(Palete *)oPalete {

    self.ofertaView = view;
    self.palete = oPalete;
    self.viewController = controller;

    [self fillTiposEntrega];
    [self fillTiposProduto];


    if(self.palete){
        [self.ofertaView setPalete];
    }


}

- (void)onRegisterOferta {


    if(![self validateFields])
        return;

    [ofertaView showProgress];

    RealizarOfertaAvulsaRequest *request = [self createRequest];
    [[APIClient sharedManager] realizarOferta:request completion:^(ResponseBase *response, NSString *errorMessage) {

        [ofertaView hideProgress];
        if(errorMessage)
            [ofertaView showAlertError:errorMessage];
        else if(response.Sucesso)
        {
            [ofertaView showSucess:response.Mensagem];
            [ofertaView limpaCampos];
        }
        else
            [ofertaView showAlertError:response.Mensagem];
    }];

}

- (RealizarOfertaAvulsaRequest *)createRequest {
    RealizarOfertaAvulsaRequest *request;
    if(palete)
        request = [[RealizarOfertaAvulsaRequest alloc] initWithPaleteId:palete.PaleteId quantidadePaletes:@(ofertaView.getQuantidade)
                                                              valorPago:@(ofertaView.getValor)
                                                            tipoEntrega:@(ofertaView.getTipoEntrega)
                                                               endereco:ofertaView.getEndereco.name
                                                               latitude:@(ofertaView.getEndereco.coordinate.latitude)
                                                              longitude:@(ofertaView.getEndereco.coordinate.longitude)
                                                         andTipoProduto:@(ofertaView.getTipoProduto)];
    else
        request = [[RealizarOfertaAvulsaRequest alloc] initWithQuantidadePaletes:@(ofertaView.getQuantidade)
                                                                       valorPago:@(ofertaView.getValor)
                                                                     tipoEntrega:@(ofertaView.getTipoEntrega)
                                                                        endereco:ofertaView.getEndereco.name
                                                                        latitude:@(ofertaView.getEndereco.coordinate.latitude)
                                                                       longitude:@(ofertaView.getEndereco.coordinate.longitude)
                                                                  andTipoProduto:@(ofertaView.getTipoProduto)];
    return request;
}

- (void)fillTiposProduto {

    [self.ofertaView fillTipoProduto:[TipoProdutoRepository getTiposProduto]];
}

- (void)fillTiposEntrega {

    [self.ofertaView fillTipoEntrega:[TipoEntregaRepository getTiposEntrega]];

}


-(BOOL)validateFields{

    NSMutableString* error = [NSMutableString new];


    if(ofertaView.getQuantidade == 0)
        [error appendString:@"O campo quantidade é de preenchimento obrigatório!\n"];

    if(ofertaView.getValor == 0)
        [error appendString:@"O Campo Valor é obrigatório!\n"];

    if(!ofertaView.getEndereco)
        [error appendString:@"O Campo Endereço é obrigatório, utilizamos o seu endereço, para que o comprador/vendedor saiba aonde buscar ou entregar os paletes\n"];

    if(![error isEqualToString:@""])
    {
        [ofertaView showAlertError:error];
        return NO;
    }

    return YES;

}



-(void)onTextEnderecoBeginEditing{

    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
    filter.type = kGMSPlacesAutocompleteTypeFilterAddress;
    acController.autocompleteFilter = filter;
    acController.delegate = self;
    [self.viewController presentViewController:acController animated:YES completion:nil];
}


-(void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place{

    [self.viewController dismissViewControllerAnimated:YES completion:nil];
    [self.ofertaView setEndereco:place];

}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error{

    [self.viewController dismissViewControllerAnimated:YES completion:nil];

}


-(void)wasCancelled:(GMSAutocompleteViewController *)viewController{
    
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)onAlertClickOk {

    [self.ofertaView toMinhasOfertasView];
}
@end
