//
//  CadastroDadosVendedorViewController.h
//  PaleteJa
//
//  Created by Pedro  on 21/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TipoProduto;
@protocol TipoEntrega;


@interface CadastroDadosVendedorViewController : BaseViewController <TextFieldPickerHelperDelegate, UIHelperDelegate>

@property CadastrarDadosVendedorRequest *request;

@property(nonatomic, strong) NSArray <TipoProduto> *tiposProduto;
@property(nonatomic, strong) NSArray <TipoEntrega> *tiposEntrega;
@end
