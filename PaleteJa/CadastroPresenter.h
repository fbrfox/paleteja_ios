//
//  CadastroPresenter.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 19/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "CadastroVIew.h"



@protocol CadastroPresenter <NSObject>

-(void)onRegisterClicked;
-(void) initWithView: (id < CadastroView >) theView typeUser:(NSNumber*) type;

@property(nonatomic,strong) id< CadastroView > view;
@property(nonatomic,strong) NSNumber* typeUser;




@end
