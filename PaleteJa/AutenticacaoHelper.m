//
//  AutenticacaoHelper.m
//  Movida Condominio
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 09/03/17.
//  Copyright © 2017 Movida Locação de Veículos Ltda. All rights reserved.
//

#import "AutenticacaoHelper.h"

@implementation AutenticacaoHelper

+(void)logout{
    
    [SharedPreferences setUserToken:nil];
    [SharedPreferences setUserId:nil];
    [SharedPreferences setUserName:nil];
    [SharedPreferences setUserTipo:nil];
    [SharedPreferences setCadastroCompleto:nil];
    
    
}

+(void)loginAutenticacao:(AutenticarResponse *)autenticacao{

    [SharedPreferences setUserToken:autenticacao.Autenticacao.Token];
    [SharedPreferences setUserName:autenticacao.Autenticacao.Nome];
    [SharedPreferences setUserId:autenticacao.Autenticacao.Id];
    [SharedPreferences setUserTipo:autenticacao.Autenticacao.TipoUsuario];
    [SharedPreferences setCadastroCompleto:@(autenticacao.CadastroCompleto)];
    
}


+(BOOL)isAutenticado{
    
    return [SharedPreferences userId];
    
}

@end
