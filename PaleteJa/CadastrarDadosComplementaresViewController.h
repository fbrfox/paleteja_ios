//
//  CadastrarDadosComplementaresViewController.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 23/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CadastrarDadosComplementaresView.h"
#import "CadastrarDadosComplementaresPresenterImpl.h"

static NSString *const segueIdentifier = @"Vendedor";

@interface CadastrarDadosComplementaresViewController : BaseViewController<CadastrarDadosComplementaresView,UIHelperDelegate>

@property NSString* foto;
@property GMSPlace *place;

@property CadastrarDadosComplementaresPresenterImpl* presenter;


@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfDocumento;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfTelefoneFixo;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfCelular;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfEndereco;
@property (weak, nonatomic) IBOutlet UISwitch *swWhatsapp;


@property(nonatomic, strong) CadastrarDadosVendedorRequest *request;
@end
