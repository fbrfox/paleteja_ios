//
//  Filtro.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//


/*
 
 { // ObterPaletesRequest
 "Estado": "SP",
 "Cidade": null,
 "Raio": 10,
 "Latitude": null,
 "Longitude": null
 }
 
 */

#import "ModelBase.h"


@interface Filtro : ModelBase


@property(nonatomic,strong) NSString* Estado;
@property(nonatomic,strong) NSString* Cidade;
@property(nonatomic) int Raio;
@property(nonatomic,strong) NSNumber* Latitude;
@property(nonatomic,strong) NSNumber* Longitude;


- (instancetype)initWithEstado:(NSString *)Estado Raio:(int)Raio;

+ (instancetype)filtroWithEstado:(NSString *)Estado Raio:(int)Raio;


@end
