//
//  SelecionarTipoUsuarioSegue.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 19/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "SelecionarTipoUsuarioSegue.h"
#import "CadastroViewController.h"

@implementation SelecionarTipoUsuarioSegue


-(void)perform{
    
    
    UIViewController* src = self.sourceViewController;
    UIViewController* dst = self.destinationViewController;
    
    if([dst isKindOfClass:[CadastroViewController class]]){
        CadastroViewController* dvc = (CadastroViewController*) dst;
        dvc.tipoUsuario = self.tipoUsuario;
    }
    
    [src.navigationController pushViewController:dst animated:YES];
    
}



@end
