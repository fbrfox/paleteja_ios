//
// Created by Movida on 02/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import "HomePresenterImpl.h"
#import "Filtro.h"
#import "HomeView.h"
#import "EstadoRepository.h"
#import "PrecoMedioHoje.h"


@implementation HomePresenterImpl {

}

@synthesize view;


- (void)initWithView:(id <HomeView>)theView {

    self.view = theView;

    [self onCancelFiltro];
    [self fillStates];
    [self getPrecoMedio];


}

- (void)getPrecoMedio {

    [[APIClient sharedManager] obterPrecoMedioCompletion:^(PrecoMedioHoje *response, NSString *errorMessage) {

        if(errorMessage)
            [view showAlertErro:errorMessage];
        else if(!response.Sucesso)
            [view showAlertErro:response.Mensagem];
        else
            [view setPrecoMedio:response.PrecoMedio];
    }];

}

- (void)onCancelFiltro {
    Filtro *filtro = [[Filtro alloc] initWithEstado:@"SP" Raio:100];
    [self callServerApi:filtro];
}

- (void)fillStates {

    [self.view setStates:[EstadoRepository getEstados]];

}

- (void)onFilterClick {

    Filtro *filtro = [self createFiltro];
    [self callServerApi:filtro];
}

- (Filtro *)createFiltro {

    Filtro *filtro = [[Filtro alloc] init];
    filtro.Estado = [view getEstado];
    filtro.Raio = [view getRaio];
    filtro.Latitude = @([view getLatitude]);
    filtro.Longitude = @([view getLongitude]);

    return filtro;
}

- (void)callServerApi:(Filtro *)filtro {

    [view showLoading];

    [[APIClient sharedManager] obterUsuariosPaletes:filtro completion:^(ObterUsuariosPaletesResponse *response, NSString *errorMessage) {

        [view hideLoading];

        if(errorMessage)
            [view showAlertErro:errorMessage];
        else if(!response.Sucesso)
            [view showAlertErro:response.Mensagem];
        else
            [view onListUsuariosReceived:response.Usuarios];

    }];

}

- (void)onNavigateToDetail:(int)index {

    [self.view navigateToDetails:index];

}

- (void)onNavigateToOrder {

    [self.view navigateToOrder];
}


@end
