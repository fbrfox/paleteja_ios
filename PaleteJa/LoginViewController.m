//
//  LoginViewController.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 29/11/17.
//  Copyright © 2017 phep. All rights reserved.
//

#import "LoginViewController.h"
#import "CadastroSegue.h"

#define kSegueLoginToCadastroFoto @"cadastroFotoFromLoginSegue"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _presenter = [LoginPresenterImpl new];
    [_presenter initWithView:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

- (IBAction)onLoginClick:(id)sender {
    
    [_presenter onLoginClicked];
}



-(NSString *)getEmail{
    
    return _tfEmail.text;
}

-(NSString *)getSenha{
    
    return _tfSenha.text;
}

-(void)hideProgress{
    
    [UIHelper hideProgress];
}


-(void)showProgress{
    
    [UIHelper showProgress];
    
}

-(void)registerWithSuccess:(Autenticacao *)autenticacao{
    
    self.autenticacao = autenticacao;
    [self performSegueWithIdentifier:kSegueLoginToCadastroFoto sender:self];
}


- (void)toHome {

    if([[SharedPreferences userTipo] isEqualToNumber:@2])
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
        [Delegate changeRootViewController:[storyBoard instantiateInitialViewController]];
    }
    else
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        [Delegate changeRootViewController:[storyBoard instantiateInitialViewController]];
    }

}


-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:kSegueLoginToCadastroFoto]){
        CadastroSegue* ns = (CadastroSegue*) segue;
        ns.autenticacao = _autenticacao;
    }
}


-(void)showAlertError:(NSString *)error{
    
    [UIHelper mostrarAlertaErro:error viewController:self];
}
- (IBAction)onEsqueciASenhaClick:(id)sender {

    [_presenter onEsqueciSenhaClicked];
}

- (void)showAlertSucess:(NSString *)success {

    [UIHelper mostrarAlertaSucesso:success viewController:self];


}


@end
