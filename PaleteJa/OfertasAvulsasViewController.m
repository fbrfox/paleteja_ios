//
//  OfertasAvulsasViewController.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 25/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "OfertasAvulsasViewController.h"
#import "Oferta.h"


@interface OfertasAvulsasViewController ()<UITableViewDataSource,UITableViewDelegate,UIHelperDelegate>
{
    __weak IBOutlet UITableView *tbOfertas;
    
}
@end

@implementation OfertasAvulsasViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
  
    return _ofertas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    Oferta *oferta = _ofertas[(NSUInteger) indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellOferta" forIndexPath:indexPath];
    

    UILabel *lbNome = [cell viewWithTag:1000];
    UILabel *lbQuantidade = [cell viewWithTag:1001];
    UILabel *lbPreco = [cell viewWithTag:1003];
    
    
    lbNome.text = [oferta.TipoProduto isEqualToNumber:@1] ? @"Pedido de Palete Novo" : @"Pedido de Palete Usado";
    lbQuantidade.text = [NSString stringWithFormat:@"%d unidade(s) solicitadas", oferta.Quantidade];

    lbPreco.text =[NSString stringWithFormat:@"Preço ofertado:%@",[UIHelper formatarPreco:oferta.PrecoPago]];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    Oferta *oferta = _ofertas[(NSUInteger) indexPath.row];
    
    _meusPaletesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MeusPaletesViewController"];
    _meusPaletesVC.idOferta = oferta.IdOferta;
    
    [UIHelper mostrarAlerta:@"Seleção de Palete" mensagem:@"Selecione o palete que deseja vender para esta oferta" delegate:self viewController:self];
    
}

-(void)alertControllerOkClicked{
    
    [self.navigationController pushViewController:_meusPaletesVC animated:YES];
}

@end
