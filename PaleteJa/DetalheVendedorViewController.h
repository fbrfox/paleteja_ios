//
//  DetalheVendedorViewController.h
//  PaleteJa
//
//  Created by Movida on 05/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetalheVendedorView.h"
#import "PaletesDataSource.h"

static NSString *const identifier = @"ofertaSegue";
@class Usuario;
@class DetalheVendedorPresenterImpl;

@interface DetalheVendedorViewController : BaseViewController <DetalheVendedorView, UITableViewDelegate>

@property PaletesDataSource *dataSource;
@property Usuario *usuario;
@property NSArray<Palete> *paletes;
@property DetalheVendedorPresenterImpl *presenter;
@property int indexPalete;



@end
