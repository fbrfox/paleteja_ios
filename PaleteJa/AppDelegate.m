//
//  AppDelegate.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/11/17.
//  Copyright © 2017 phep. All rights reserved.
//

#import "AppDelegate.h"
@import GooglePlaces;

@interface AppDelegate ()

@end

@implementation AppDelegate


-(void)changeRootViewController:(UIViewController *)viewController{

    self.window.rootViewController = viewController;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [GMSPlacesClient provideAPIKey:NSLocalizedString(@"PlacesAPI", nil)];

    if([AutenticacaoHelper isAutenticado] && [SharedPreferences getCadastroCompleto] && [[SharedPreferences userTipo] isEqualToNumber:@2])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Home" bundle:nil];

        self.window.rootViewController = [storyboard instantiateInitialViewController];

    }
    else if([AutenticacaoHelper isAutenticado] && [SharedPreferences getCadastroCompleto] && [[SharedPreferences userTipo] isEqualToNumber:@1]){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        self.window.rootViewController = [storyboard instantiateInitialViewController];
    }



    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
