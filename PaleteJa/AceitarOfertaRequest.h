//
//  AceitarOfertaRequest.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 18/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "ModelBase.h"

@interface AceitarOfertaRequest : ModelBase

@property(nonatomic,strong) NSNumber* IdPedido;
@property(nonatomic,strong) NSNumber* IdPalete;

@end
