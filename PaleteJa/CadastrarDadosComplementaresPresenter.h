//
//  CadastrarDadosComplementaresPresenter.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 23/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CadastrarDadosComplementaresView.h"


@protocol CadastrarDadosComplementaresView;

@protocol CadastrarDadosComplementaresPresenter <NSObject>

-(void)initWithView:( id <CadastrarDadosComplementaresView> )theView viewController:(UIViewController *)theViewController andFoto:(NSString *)fotoPerfil;

@property id<CadastrarDadosComplementaresView> view;
@property NSString* foto;
@property UIViewController* viewController;


-(void)onSalvarClicked;

-(void)onTextEnderecoBeginEditing;


@end

@interface CadastrarDadosComplementaresPresenter : NSObject

@end
