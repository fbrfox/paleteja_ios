//
//  CadastroViewController.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 29/11/17.
//  Copyright © 2017 phep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CadastroVIew.h"
#import "CadastroPresenterImpl.h"

@interface CadastroViewController : BaseViewController<CadastroView>


@property CadastroPresenterImpl* presenter;

@property NSNumber* tipoUsuario;
@property Autenticacao* autenticacao;



@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *lbNome;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *lbEmail;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *lbSenha;

@end
