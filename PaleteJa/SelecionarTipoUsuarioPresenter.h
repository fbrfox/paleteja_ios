//
//  EntrarPresenter.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 19/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "SelecionarTipoUsuarioView.h"

@protocol SelecionarTipoUsuarioPresenter <NSObject>


@property(nonatomic,strong) id< SelecionarTipoUsuarioView > view;

-(void)buttonCompradorClicked;
-(void)buttonVendedorClicked;

-(void) initWithView: (id < SelecionarTipoUsuarioView >) theView;

@end
