//
//  CadastrarDadosComplementaresViewController.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 23/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "CadastrarDadosComplementaresViewController.h"
#import "VendedorSegue.h"

@interface CadastrarDadosComplementaresViewController ()

@end

@implementation CadastrarDadosComplementaresViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _presenter = [CadastrarDadosComplementaresPresenterImpl new];
    [_presenter initWithView:self viewController:self andFoto:_foto];
    
    [self configdismissOnTap:@[_tfEndereco,_tfDocumento,_tfTelefoneFixo,_tfCelular]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:segueIdentifier]) {
        VendedorSegue *ns = (VendedorSegue *) segue;
        ns.request = _request;
    }

}

- (IBAction)onNextClicked:(id)sender {
    
    [_presenter onSalvarClicked];
}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    
    
    if([textField isEqual:_tfEndereco])
    {
        [_presenter onTextEnderecoBeginEditing];
        return NO;
    }
    
    return YES;
    
}


 #pragma mark - CadastrarDadosComplementaresView


- (GMSPlace *)getPlace {
    return _place;
}

-(void)setEndereco:(GMSPlace *)place{
    
    _tfEndereco.text = place.name;
    _place = place;
    
}


-(NSString *)getCelular{
    
    return _tfCelular.text;
}

-(NSString *)getEndereco{
    
    return _place.name;
}

-(NSString *)getTelefone{
    
    return _tfTelefoneFixo.text;
}


-(NSString *)getDocumento{
    
    return _tfDocumento.text;
}


-(void)showProgress{

    [UIHelper showProgress];
}

-(void)hideProgress{
    
    [UIHelper hideProgress];
}

-(void)showAlertError:(NSString *)error{
    
    [UIHelper mostrarAlertaErro:error viewController:self];
}


-(BOOL)getIsWhatsapp{
    
    return _swWhatsapp.isOn;
}

-(void)registerWithSuccessToHome{
    
    
    [UIHelper mostrarAlertaSucesso:@"Cadastro efetuado com sucesso!" viewController:self delegate:self];
    
    
    
}

-(void)registerWithSucessToVendor:(CadastrarDadosVendedorRequest *)request{

    self.request = request;
    [self performSegueWithIdentifier:segueIdentifier sender:nil];
    
}


-(void)alertControllerOkClicked{
    
    [Delegate changeRootViewController:[[UIStoryboard storyboardWithName:@"Home" bundle:nil] instantiateInitialViewController]];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(![textField isEqual:_tfTelefoneFixo] && ![textField isEqual:_tfCelular])
        return YES;
    
    int length = (int)[self getLength:textField.text];
    //NSLog(@"Length  =  %d ",length);
    
    if(length == 12)
    {
        if(range.length == 0)
            return NO;
    }
    
    if(length == 3)
    {
        NSString *num = [self formatNumber:textField.text];
        textField.text = [NSString stringWithFormat:@"(%@) ",num];
        
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
    }
    else if(length == 7 && [textField isEqual:_tfTelefoneFixo])
    {
        NSString *num = [self formatNumber:textField.text];
        //NSLog(@"%@",[num  substringToIndex:3]);
        //NSLog(@"%@",[num substringFromIndex:3]);
        
            textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
        
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
        
    }
    else if(length == 8 && [textField isEqual:_tfCelular])
    {
         NSString *num = [self formatNumber:textField.text];
        
        textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
        
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
    }
    
    return YES;
}

- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 12)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-12];
        NSLog(@"%@", mobileNumber);
        
    }
    
    return mobileNumber;
}

- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
}

@end
