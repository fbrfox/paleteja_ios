//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import "APIClient.h"
#import "JSONResponseSerializerWithData.h"
#import "AutenticacaoHelper.h"
#import "Filtro.h"
#import "PrecoMedioHoje.h"
#import "ObterUsuarioResponse.h"
#import "ObterPaletesVendidosResponse.h"

//EndPoints Urls


NSString *const APIBaseURL = @"http://www.paleteja.com.br/api/";

//NSString *const APIBaseURL = @"http://192.168.43.44/tripholics/api/";

//Producao
NSString *const CadastrarUsuarioUrl = @"Usuario/Cadastrar";
NSString *const AutenticarUsuarioUrl = @"Usuario/Autenticar";
NSString *const EsqueciSenhaUsuarioUrl = @"Usuario/EsqueciSenha";
NSString *const CadastrarDadosCompradorUrl = @"Usuario/CadastrarDadosComprador";
NSString *const CadastrarDadosVendedorUrl = @"Usuario/CadastrarDadosVendedor";
NSString *const SalvarPaleteUrl = @"Palete/Salvar";
NSString *const MeusPaletesUrl = @"Palete/MeusPaletes";
NSString *const PrecoMedioHojeUrl = @"Palete/PrecoMedioHoje";
NSString *const ObterUsuariosPaletesUrl = @"Palete/ObterUsuariosPaletes";
NSString *const RealizarOfertaAvulsaUrl = @"Palete/RealizarOfertaAvulsa";
NSString *const RealizarOfertaUrl = @"Palete/RealizarOferta";
NSString *const ObterOfertasAvulsasUrl = @"Palete/ObterOfertasAvulsas";
NSString *const ObterMinhasOfertasUrl = @"Palete/ObterMinhasOfertas";
NSString *const ObterOfertasCompradorUrl = @"Palete/ObterOfertasComprador";
NSString *const AceitarOfertaUrl = @"Palete/AceitarOferta";
NSString *const ExcluirOfertaUrl = @"Palete/ExcluirOferta?idOferta=%@";
NSString *const ObterPaletesVendidosUrl = @"Palete/ObterPaletesVendidos";
NSString *const UsuarioObterUrl = @"Usuario/Obter";
NSString *const UsuarioAtualizarUrl = @"Usuario/Atualizar";
















//Errors
int const kErrorUnauthorized = 401;


//Localized Strings
NSString *const serverDown = @"ServerDown";
NSString *const badCredentials = @"BadCredentials";

@implementation APIClient

#pragma mark Configurações Header



+ (APIClient *)sharedManager {
    static APIClient *_sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

        NSLog(APIBaseURL);
        
        _sharedManager = [[APIClient alloc] initWithBaseURL:[NSURL URLWithString:APIBaseURL] sessionConfiguration:configuration];
        [self configClient:_sharedManager];
    });

    return _sharedManager;
}

+ (void)configClient:(APIClient *)_sharedManager {
    [_sharedManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [_sharedManager setResponseSerializer:[JSONResponseSerializerWithData serializer]];
    [_sharedManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [_sharedManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
}

- (void)setUserTokenHeader {
    
    if ([SharedPreferences userToken]) {
        [self.requestSerializer setValue:[SharedPreferences userToken] forHTTPHeaderField:@"UserToken"];
    }
}

- (NSURLSessionDataTask *)obterUsuarioWithCompletion:(void (^)(ObterUsuarioResponse *response, NSString *errorMessage))completion {

    NSURLSessionDataTask *task = [self GETWithLogParams:UsuarioObterUrl parameters:[NSDictionary new]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {

                                                    NSError *error;

                                                    ObterUsuarioResponse *baseResponse = [[ObterUsuarioResponse alloc] initWithDictionary:responseObject error:&error];
                                                    if (error) {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    } else {
                                                        completion(baseResponse, nil);
                                                    }

                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;

}


- (NSURLSessionDataTask *)atualizarUsuario:(CadastrarDadosVendedorRequest *)loginRequest completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion {

    NSURLSessionDataTask *task = [self POSTWithLogParams:UsuarioAtualizarUrl parameters:[loginRequest toDictionary]
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {

                                                     NSError *error;

                                                     ResponseBase *loginResponse = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];


                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {

                                                         completion(loginResponse, nil);
                                                     }

                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;

}

- (NSURLSessionDataTask *)esqueciSenha:(AutenticarRequest *)loginRequest completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion {

    NSURLSessionDataTask *task = [self POSTWithLogParams:EsqueciSenhaUsuarioUrl parameters:[loginRequest toDictionary]
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {

                                                     NSError *error;

                                                     ResponseBase *loginResponse = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];


                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {

                                                         completion(loginResponse, nil);
                                                     }

                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;

}

-(NSURLSessionDataTask *)autenticar:(AutenticarRequest *)loginRequest completion:(void (^)(AutenticarResponse *response, NSString *errorMessage))completion{
    
    
    
        NSURLSessionDataTask *task = [self POSTWithLogParams:AutenticarUsuarioUrl parameters:[loginRequest toDictionary]
                                                    success:^(NSURLSessionDataTask *task, id responseObject) {
    
                                                        NSError *error;
    
                                                        AutenticarResponse *loginResponse = [[AutenticarResponse alloc] initWithDictionary:responseObject error:&error];
    
                                                        
                                                        if (error) {
                                                            completion(nil, NSLocalizedString(serverDown, nil));
                                                        } else {
    
                                                            [AutenticacaoHelper loginAutenticacao:loginResponse];
                                                            completion(loginResponse, nil);
                                                        }
    
                                                    } failure:^(NSURLSessionDataTask *task, NSError *error) {
    
                                                        if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                            completion(nil, NSLocalizedString(badCredentials, nil));
                                                        } else {
                                                            completion(nil, NSLocalizedString(serverDown, nil));
                                                        }
                                                    }];
        
        return task;

}
-(NSURLSessionDataTask *)cadastrarUsuario:(CadastrarUsuarioRequest *)cadastroRequest completion:(void (^)(AutenticarResponse *response, NSString *errorMessage))completion{
    
    
    
    NSURLSessionDataTask *task = [self POSTWithLogParams:CadastrarUsuarioUrl parameters:[cadastroRequest toDictionary]
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {
                                                     
                                                     NSError *error;
                                                     
                                                     AutenticarResponse *loginResponse = [[AutenticarResponse alloc] initWithDictionary:responseObject error:&error];
                                                     
                                                     
                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {
                                                         [AutenticacaoHelper loginAutenticacao:loginResponse];
                                                         [SharedPreferences setUserTipo:[cadastroRequest TipoUsuario]];
                                                         completion(loginResponse, nil);
                                                     }
                                                     
                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                     
                                                     if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                         completion(nil, NSLocalizedString(badCredentials, nil));
                                                     } else {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     }
                                                 }];
    
    return task;
    
}

- (NSURLSessionDataTask *)cadastrarDadosVendedor:(CadastrarDadosVendedorRequest *)cadastroRequest completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion {


    NSURLSessionDataTask *task = [self POSTWithLogParams:CadastrarDadosVendedorUrl parameters:[cadastroRequest toDictionary]
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {

                                                     NSError *error;

                                                     ResponseBase *baseResponse = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];


                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {
                                                         completion(baseResponse, nil);
                                                     }

                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;
}


-(NSURLSessionDataTask *)cadastrarDadosComprador:(CadastrarDadosCompradorRequest *)cadastroRequest completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion{
    
    
    
    NSURLSessionDataTask *task = [self POSTWithLogParams:CadastrarDadosCompradorUrl parameters:[cadastroRequest toDictionary]
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {
                                                     
                                                     NSError *error;
                                                     
                                                     ResponseBase *baseResponse = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];
                                                     
                                                     
                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {
                                                         completion(baseResponse, nil);
                                                     }
                                                     
                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                     
                                                     if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                         completion(nil, NSLocalizedString(badCredentials, nil));
                                                     } else {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     }
                                                 }];
    
    return task;
    
}

- (NSURLSessionDataTask *)obterUsuariosPaletes:(Filtro *)request completion:(void (^)(ObterUsuariosPaletesResponse *response, NSString *errorMessage))completion {

    NSURLSessionDataTask *task = [self POSTWithLogParams:ObterUsuariosPaletesUrl parameters:[request toDictionary]
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {

                                                     NSError *error;

                                                     ObterUsuariosPaletesResponse *baseResponse = [[ObterUsuariosPaletesResponse alloc] initWithDictionary:responseObject error:&error];


                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {
                                                         completion(baseResponse, nil);
                                                     }

                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;

}


- (NSURLSessionDataTask *)obterPrecoMedioCompletion:(void (^)(PrecoMedioHoje *response, NSString *errorMessage))completion {


    NSURLSessionDataTask *task = [self GETWithLogParams:PrecoMedioHojeUrl parameters:[NSDictionary new]
            success:^(NSURLSessionDataTask *task, id responseObject) {

                NSError *error;

                PrecoMedioHoje *baseResponse = [[PrecoMedioHoje alloc] initWithDictionary:responseObject error:&error];


                if (error) {
                    completion(nil, NSLocalizedString(serverDown, nil));
                } else {
                    completion(baseResponse, nil);
                }

            } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;

}

- (NSURLSessionDataTask *)realizarOferta:(RealizarOfertaAvulsaRequest *)request completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion {

    NSURLSessionDataTask *task = [self POSTWithLogParams:RealizarOfertaAvulsaUrl parameters:[request toDictionary]
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {

                                                     NSError *error;

                                                     ResponseBase *baseResponse = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];


                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {
                                                         completion(baseResponse, nil);
                                                     }

                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;
}


-(NSURLSessionDataTask *)obterMinhasOfertasCompradorCompletion:(void (^)(ObterOfertasAvulsasResponse *response, NSString *errorMessage))completion{

    NSURLSessionDataTask *task = [self GETWithLogParams:ObterOfertasCompradorUrl parameters:[NSDictionary new]
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {

                                                     NSError *error;

                                                     ObterOfertasAvulsasResponse *baseResponse = [[ObterOfertasAvulsasResponse alloc] initWithDictionary:responseObject error:&error];
                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {
                                                         completion(baseResponse, nil);
                                                     }

                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;
}


-(NSURLSessionDataTask *)obterMinhasOfertasCompletion:(void (^)(ObterOfertasAvulsasResponse *response, NSString *errorMessage))completion{

    NSURLSessionDataTask *task = [self GETWithLogParams:ObterMinhasOfertasUrl parameters:[NSDictionary new]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {

                                                    NSError *error;

                                                    ObterOfertasAvulsasResponse *baseResponse = [[ObterOfertasAvulsasResponse alloc] initWithDictionary:responseObject error:&error];
                                                    if (error) {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    } else {
                                                        completion(baseResponse, nil);
                                                    }

                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;
}

-(NSURLSessionDataTask *)obterPaletesVendidos:(void (^)(ObterPaletesVendidosResponse *response, NSString *errorMessage))completion{

    NSURLSessionDataTask *task = [self GETWithLogParams:ObterPaletesVendidosUrl parameters:[NSDictionary new]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {

                                                    NSError *error;

                                                    ObterPaletesVendidosResponse *baseResponse = [[ObterPaletesVendidosResponse alloc] initWithDictionary:responseObject error:&error];
                                                    if (error) {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    } else {
                                                        completion(baseResponse, nil);
                                                    }

                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;
}


- (NSURLSessionDataTask *)meusPaletescompletion:(void (^)(MeusPaletesResponse *response, NSString *errorMessage))completion {


    NSURLSessionDataTask *task = [self GETWithLogParams:MeusPaletesUrl parameters:[NSDictionary new]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {

                                                    NSError *error;

                                                    MeusPaletesResponse *baseResponse = [[MeusPaletesResponse alloc] initWithDictionary:responseObject error:&error];
                                                    if (error) {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    } else {
                                                        completion(baseResponse, nil);
                                                    }

                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;
}


- (NSURLSessionDataTask *)ofertasAvulsasCompletion:(void (^)(ObterOfertasAvulsasResponse *response, NSString *errorMessage))completion {


    NSURLSessionDataTask *task = [self GETWithLogParams:ObterOfertasAvulsasUrl parameters:[NSDictionary new]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {

                                                    NSError *error;

                                                    ObterOfertasAvulsasResponse *baseResponse = [[ObterOfertasAvulsasResponse alloc] initWithDictionary:responseObject error:&error];
                                                    if (error) {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    } else {
                                                        completion(baseResponse, nil);
                                                    }

                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;
}


-(NSURLSessionDataTask *)aceitarOferta:(AceitarOfertaRequest *)request completion:(void (^)(ResponseBase *, NSString *))completion{
    
    NSURLSessionDataTask *task = [self PUTWithLogParams:AceitarOfertaUrl parameters:[request toDictionary]
                                                 success:^(NSURLSessionDataTask *task, id responseObject) {
                                                     
                                                     NSError *error;
                                                     
                                                     ResponseBase *baseResponse = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];
                                                     
                                                     if (error) {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     } else {
                                                         completion(baseResponse, nil);
                                                     }
                                                     
                                                 } failure:^(NSURLSessionDataTask *task, NSError *error) {
                                                     
                                                     if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                                                         completion(nil, NSLocalizedString(badCredentials, nil));
                                                     } else {
                                                         completion(nil, NSLocalizedString(serverDown, nil));
                                                     }
                                                 }];
    
    return task;
}

- (NSURLSessionDataTask *)salvarPalete:(SalvarPaleteRequest *)request completion:(void (^)(ResponseBase *, NSString *))completion {

    NSURLSessionDataTask *task = [self POSTWithLogParams:SalvarPaleteUrl parameters:[request toDictionary]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {

                                                    NSError *error;

                                                    ResponseBase *baseResponse = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];

                                                    if (error) {
                                                        completion(nil, NSLocalizedString(serverDown, nil));
                                                    } else {
                                                        completion(baseResponse, nil);
                                                    }

                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
                    completion(nil, NSLocalizedString(badCredentials, nil));
                } else {
                    completion(nil, NSLocalizedString(serverDown, nil));
                }
            }];

    return task;
}




//- (NSURLSessionDataTask *)denunciarPerfil:(NSNumber *)idPerfil completion:(void (^)(ResponseBase *response, NSString *errorMessage))completion{
//    
//    
//    NSString *url = [NSString stringWithFormat:APIDenunciarPerfil,idPerfil];
//    
//    NSURLSessionDataTask *task = [self PUTWithLogParams:url parameters:[NSDictionary new]
//                                                success:^(NSURLSessionDataTask *task, id responseObject) {
//                                                    
//                                                    NSError *error;
//                                                    
//                                                    ResponseBase *loginResponse = [[ResponseBase alloc] initWithDictionary:responseObject error:&error];
//                                                    
//                                                    
//                                                    if (error) {
//                                                        completion(nil, NSLocalizedString(serverDown, nil));
//                                                    } else {
//                                                        
//                                                        completion(loginResponse, nil);
//                                                    }
//                                                    
//                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {
//                                                    
//                                                    if (((NSHTTPURLResponse *) [task response]).statusCode == kErrorUnauthorized) {
//                                                        completion(nil, NSLocalizedString(badCredentials, nil));
//                                                    } else {
//                                                        completion(nil, NSLocalizedString(serverDown, nil));
//                                                    }
//                                                }];
//    
//    return task;
//}
//
//


//Metodo que encapsula o método de POST somente para logar os parametros em desenvolvimento
- (NSURLSessionDataTask *)POSTWithLogParams:(NSString *)URLString
                                 parameters:(NSDictionary *)parameters
                                    success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                    failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {

#if DEBUG
    NSError *errorAutentication;
    NSData *jsonDataAutentication = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&errorAutentication];
    if (!errorAutentication) {
        NSLog(@"POST URL :%@ >>>>>>>>>>>>> Json params:%@", URLString, [[NSString alloc] initWithData:jsonDataAutentication encoding:NSUTF8StringEncoding]);
    }
#endif
    
    
//    [self.requestSerializer setValue:[TokenHelper retornaToken]  forHTTPHeaderField:@"Token"];
    
    [self setUserTokenHeader];
    
    NSURLSessionDataTask *task = [self POST:URLString parameters:parameters progress:nil
        success:^(NSURLSessionDataTask *task, id responseObject) {
            success(task, responseObject);
        }
        failure:^(NSURLSessionDataTask *task, NSError *error) {
#if DEBUG
            NSLog(@"%@", error);
#endif
            failure(task, error);
        }
    ];

    return task;
}

//Metodo que encapsula o método de PUT somente para logar os parametros em desenvolvimento
- (NSURLSessionDataTask *)PUTWithLogParams:(NSString *)URLString
                                parameters:(NSDictionary *)parameters
                                   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {

#if DEBUG
    NSError *errorAutentication;
    NSData *jsonDataAutentication = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&errorAutentication];
    if (!errorAutentication) {
        NSLog(@"PUT URL :%@ >>>>>>>>>>>>> Json params:%@", URLString, [[NSString alloc] initWithData:jsonDataAutentication encoding:NSUTF8StringEncoding]);
    }
#endif

//    [self.requestSerializer setValue:[TokenHelper retornaToken]  forHTTPHeaderField:@"Token"];
    [self setUserTokenHeader];
    
    NSURLSessionDataTask *task = [self PUT:URLString parameters:parameters
        success:^(NSURLSessionDataTask *task, id responseObject) {
            success(task, responseObject);
        }
        failure:^(NSURLSessionDataTask *task, NSError *error) {
#if DEBUG
            NSLog(@"%@", error);
#endif
            failure(task, error);
        }
    ];

    return task;
}

- (NSURLSessionDataTask *)GETURLFormatedWithLogParams:(NSString *)URLString
                                           parameters:(NSDictionary *)parameters
                                              success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                              failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {

#if DEBUG
    NSError *errorAutentication;
    NSData *jsonDataAutentication = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&errorAutentication];
    if (!errorAutentication) {
        NSLog(@"GET URL :%@ >>>>>>>>>>>>> Json params:%@", URLString, [[NSString alloc] initWithData:jsonDataAutentication encoding:NSUTF8StringEncoding]);
    }
#endif

//    [self.requestSerializer setValue:[TokenHelper retornaToken]  forHTTPHeaderField:@"Token"];
    
    [self setUserTokenHeader];
    NSURLSessionDataTask *task = [self GET:URLString parameters:nil progress:nil
        success:^(NSURLSessionDataTask *task, id responseObject) {
            success(task, responseObject);
        }
        failure:^(NSURLSessionDataTask *task, NSError *error) {
#if DEBUG
            NSLog(@"%@", error);
#endif
            failure(task, error);
        }
    ];

    return task;
}

//Metodo que encapsula o método de Get somente para logar os parametros em desenvolvimento
- (NSURLSessionDataTask *)GETWithLogParams:(NSString *)URLString
                                parameters:(NSDictionary *)parameters
                                   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {

    for (NSString *key in parameters.allKeys) {

        URLString = [URLString stringByAppendingString:[NSString stringWithFormat:@"/%@/%@", key, parameters[key]]];
    }

#if DEBUG
    NSError *errorAutentication;
    NSData *jsonDataAutentication = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&errorAutentication];
    if (!errorAutentication) {
        NSLog(@"GET URL :%@ >>>>>>>>>>>>> Json params:%@", URLString, [[NSString alloc] initWithData:jsonDataAutentication encoding:NSUTF8StringEncoding]);
    }
#endif
    
    
//    [self.requestSerializer setValue:[TokenHelper retornaToken]  forHTTPHeaderField:@"Token"];
    

    [self setUserTokenHeader];
    
    NSURLSessionDataTask *task = [self GET:URLString parameters:nil progress:nil
        success:^(NSURLSessionDataTask *task, id responseObject) {
            success(task, responseObject);
        }
        failure:^(NSURLSessionDataTask *task, NSError *error) {
#if DEBUG
            NSLog(@"%@", error);
#endif
            failure(task, error);
        }];

    return task;
}



@end
