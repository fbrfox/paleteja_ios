//
// Created by Movida on 05/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Usuario;


@interface DetalheVendedorSegue : UIStoryboardSegue

@property Usuario* usuario;

@end