//
//  PaletesDataSource.h
//  PaleteJa
//
//  Created by Movida on 05/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Palete;

@interface PaletesDataSource : NSObject<UITableViewDataSource>

-(id)initWithPaletes:(NSArray <Palete>*) paletes;

@property NSArray<Palete> *paletes;

@end
