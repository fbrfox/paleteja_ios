//
// Created by Movida on 06/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OfertaPresenter.h"


#import "OfertaView.h"

@interface OfertaPresenterImpl : NSObject<OfertaPresenter, GMSAutocompleteViewControllerDelegate>

@end