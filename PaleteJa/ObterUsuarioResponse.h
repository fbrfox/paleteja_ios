//
// Created by Pedro  on 03/04/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Usuario;


@interface ObterUsuarioResponse : ResponseBase

@property (nonatomic, strong) Usuario *Usuario;

@end