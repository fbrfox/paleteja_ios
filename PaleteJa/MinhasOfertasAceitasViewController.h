//
//  MinhasOfertasAceitasViewController.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 26/03/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol Oferta;

@interface MinhasOfertasAceitasViewController : UIViewController

@property(nonatomic, strong) NSArray <Oferta> *minhasOfertas;

@end
