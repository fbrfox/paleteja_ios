//
//  CadastroViewController.m
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 29/11/17.
//  Copyright © 2017 phep. All rights reserved.
//

#import "CadastroViewController.h"
#import "CadastroSegue.h"

#define kSegueNavigateToCadastroFoto @"cadastroFotoSegue"


@interface CadastroViewController ()

@end

@implementation CadastroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _presenter = [CadastroPresenterImpl new];
    [_presenter initWithView:self typeUser:_tipoUsuario];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onVoltarClick:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onRegisterClick:(id)sender {
    
    [_presenter onRegisterClicked];
}

-(void)hideProgress{
    
    [UIHelper hideProgress];
}

-(void)showProgress{

    [UIHelper showProgress];
}

-(void)showAlertError:(NSString *)error{
    
    [UIHelper mostrarAlerta:error viewController:self];
}

-(NSString *)getNome{
    
    return _lbNome.text;
}

-(NSString *)getSenha{
    
    return _lbSenha.text;
}

-(NSString *)getEmail{
    
    return _lbEmail.text;
}


-(void)registerWithSuccess:(Autenticacao *)autenticacao{
    
    self.autenticacao = autenticacao;
    [self performSegueWithIdentifier:kSegueNavigateToCadastroFoto sender:self];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:kSegueNavigateToCadastroFoto]){
        CadastroSegue* ns = (CadastroSegue*) segue;
        ns.autenticacao = _autenticacao;
    }
}

@end
