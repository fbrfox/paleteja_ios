//
//  RealizarOfertaAvulsaRequest.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 18/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

/*
 
 { // RealizarOfertaAvulsaRequest
 "QuantidadePaletes": 50,
 "ValorPago": 10.90,
 "TipoEntrega": 1  - Entregar, 2 - Retirar ,
"Endereco": "Rua noêmia 115 casa 1",
"Latitude": -23.5034999,
"Longitude": -46.714765,
"TipoProduto": 1  1- novo , 2 usado
}


 */

#import "ModelBase.h"

@interface RealizarOfertaAvulsaRequest : ModelBase



@property(nonatomic,strong) NSNumber *PaleteId;
@property(nonatomic,strong) NSNumber *QuantidadePaletes;
@property(nonatomic,strong) NSNumber *ValorPago;
@property(nonatomic,strong) NSNumber *TipoEntrega;
@property(nonatomic,strong) NSString *Endereco;
@property(nonatomic,strong) NSNumber *Latitude;
@property(nonatomic,strong) NSNumber *Longitude;
@property(nonatomic,strong) NSNumber *TipoProduto;


-(id)initWithPaleteId:(NSNumber *)paleteId quantidadePaletes:(NSNumber *)quantidadePaletes valorPago:(NSNumber *)valorPago tipoEntrega:(NSNumber *)tipoEntrega
             endereco:(NSString *)endereco latitude:(NSNumber *)latitude longitude:(NSNumber *)longitude andTipoProduto:(NSNumber *)tipoProduto;



-(id)initWithQuantidadePaletes:(NSNumber *)quantidadePaletes valorPago:(NSNumber *)valorPago tipoEntrega:(NSNumber *)tipoEntrega
             endereco:(NSString *)endereco latitude:(NSNumber *)latitude longitude:(NSNumber *)longitude andTipoProduto:(NSNumber *)tipoProduto;



@end
