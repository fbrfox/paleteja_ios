//
// Created by Movida on 05/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Palete.h"

@class Usuario;
@protocol Palete;




@protocol DetalheVendedorView <NSObject>

-(void)setName:(NSString *)name foto:(NSString *)userFoto andPaletes:(NSArray<Palete> *)paletes;
-(void)toOferta:(int)indexPalete;


@end

