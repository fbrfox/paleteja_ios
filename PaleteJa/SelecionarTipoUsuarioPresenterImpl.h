//
//  SelecionarTipoUsuarioPresenterImpl.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 19/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SelecionarTipoUsuarioView.h"
#import "SelecionarTipoUsuarioPresenter.h"

@interface SelecionarTipoUsuarioPresenterImpl : NSObject <SelecionarTipoUsuarioPresenter>

@end
