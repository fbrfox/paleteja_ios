//
// Created by Movida on 05/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DetalheVendedorView;
@class Usuario;
@class Palete;


@protocol DetalheVendedorPresenter <NSObject>

@property id<DetalheVendedorView> view;
@property Usuario *usuario;


-(void)initWith:(id<DetalheVendedorView> )view andUsuario:(Usuario *)usuario;
-(void)selectPaleteToOrder:(int)paleteIndex;



@end

