//
//  HomeView.h
//  PaleteJa
//
//  Created by Movida on 02/02/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol Usuario;

@protocol HomeView <NSObject>

-(int)getRaio;
-(NSString *)getEstado;
-(double)getLatitude;
-(double)getLongitude;


-(void)setPrecoMedio:(NSString *)precoMedio;
-(void)onListUsuariosReceived:(NSArray<Usuario> *)usuarios;
-(void)navigateToDetails:(int)index;
-(void)navigateToOrder;
-(void) showLoading;
-(void) hideLoading;
-(void) showAlertErro:(NSString *)error;
-(void)setStates:(NSArray *)estados;

@end


