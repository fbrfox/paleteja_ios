//
//  PrecoMedioHoje.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "ResponseBase.h"

@interface PrecoMedioHoje : ResponseBase

@property(nonatomic,strong) NSString* PrecoMedio;

@end
