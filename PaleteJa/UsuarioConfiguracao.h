//
// Created by Pedro  on 03/04/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UsuarioConfiguracao : ModelBase

@property(nonatomic,strong) NSNumber *RaioEntrega;
@property(nonatomic,strong) NSNumber *TipoEntrega;
@property(nonatomic,strong) NSNumber *QuantidadeMinimaEntrega;
@property(nonatomic,strong) NSNumber *TipoProduto;

@end