//
// Created by Movida on 09/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import "OfertaSegue.h"
#import "Palete.h"
#import "OfertaViewController.h"


@implementation OfertaSegue {

}


- (void)perform {
    UIViewController* src = self.sourceViewController;
    UIViewController* dst = self.destinationViewController;

    if([dst isKindOfClass:[OfertaViewController class]]){
        OfertaViewController* dvc = (OfertaViewController*) dst;
        dvc.palete= _palete;
    }

    [src.navigationController pushViewController:dst animated:YES];
}

@end