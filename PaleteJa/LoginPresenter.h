//
//  LoginPresenter.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 22/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

#import "LoginView.h"


@protocol LoginPresenter <NSObject>

-(void)initWithView:(id<LoginView> )theView;

-(void)onLoginClicked;
-(void)onEsqueciSenhaClicked;

@property(nonatomic,strong) id< LoginView > view;

@end
