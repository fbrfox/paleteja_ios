//
//  EntrarView.h
//  PaleteJa
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 19/01/18.
//  Copyright © 2018 phep. All rights reserved.
//

@protocol SelecionarTipoUsuarioView < NSObject >

-(void)selectTypeRegister:(NSNumber *)tipo;

@end
