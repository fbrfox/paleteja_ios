//
// Created by Movida on 06/02/18.
// Copyright (c) 2018 phep. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol TipoEntrega;

@interface TipoEntrega : NSObject

@property int idTipoEntrega;
@property NSString *descricao;

- (instancetype)initWithIdTipoEntrega:(int)idTipoEntrega descricao:(NSString *)descricao;

+ (instancetype)entregaWithIdTipoEntrega:(int)idTipoEntrega descricao:(NSString *)descricao;


@end